<?php

return [
    'currencies' => [
        'GBP' => 'GBP',
        'EUR' => 'EUR',
        'USD' => 'USD',
        'CNY' => 'CNY',
        'RUB' => 'RUB',
        'CHF' => 'CHF'
    ]
];