<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\GeneralSetting;
use App\Models\ImageTool;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{


    /**
     * @param $url
     * @param $assoc
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendGet($url, $assoc = true)
    {
        $client  = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, []);
        $answer = json_decode($response->getBody()->getContents(), $assoc);
        return $answer;
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/admin-lte.php' => config_path('admin-lte.php'),
        ], 'config');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'admin-lte');
        $this->publishes([
            __DIR__ . '/../../resources/views' => resource_path('views/vendor/admin-lte')
        ], 'views');

        Schema::defaultStringLength(191);

        try{

            $result = $this->sendGet(env('API_CURRENCY'));
       
        }catch (\Exception $exception){
            $result = ['rates' => [
                    'MXN' => 21.7827,
                    'AUD' => 1.5988,
                    'HKD' => 8.8766,
                    'RON' => 4.7565,
                    'HRK' => 7.419,
                    'CHF' => 1.136,
                    'IDR' => 16143.3,
                    'CAD' => 1.5118,
                    'USD' => 1.1308,
                    'ZAR' => 16.3782,
                    'JPY' => 126.16,
                    'BRL' => 4.3576,
                    'HUF' => 314.35,
                    'CZK' => 25.668,
                    'NOK' => 9.688,
                    'INR' => 78.074,
                    'PLN' => 4.3049,
                    'ISK' => 132.9,
                    'PHP' => 59.491,
                    'SEK' => 10.4964,
                    'ILS' => 4.0708,
                    'GBP' => 0.85415,
                    'SGD' => 1.5313,
                    'CNY' => 7.5922,
                    'TRY' => 6.1858,
                    'MYR' => 4.6171,
                    'RUB' => 73.9233,
                    'NZD' => 1.6532,
                    'KRW' => 1284.65,
                    'THB' => 35.846,
                    'BGN' => 1.9558,
                    'DKK' => 7.4634
                ],
                'base' => 'EUR',
            'date' => '2019-03-15'];
        }
        \Config::set('currency_rates', $result);

        View::composer('*', function($view)
        {
            $general_settings = GeneralSetting::get();
            $top_logo ='';
            $top_menu_items = [];
            $home_locales =[];
            $top_logo_adaptive="";
            $footer_data = [];
            foreach($general_settings as $setting){
                if($setting['key'] == 'top_menu_items'){
                    $top_menu_items = json_decode($setting['value'],true);
                }
                if($setting['key'] == 'home_locales'){
                    $home_locales = json_decode($setting['value'],true);
                }
                if($setting['key'] == 'top_logo'){
                    $top_logo = $setting['value'];
                }
                if($setting['key'] == 'top_logo_adaptive'){
                    $top_logo_adaptive = $setting['value'];
                }
                if($setting['key'] == 'general_address'){
                    $footer_data['address'] = $setting['value'];
                }
                if($setting['key'] == 'general_phone_1'){
                    $footer_data['phone1'] = $setting['value'];
                }
                if($setting['key'] == 'general_phone_2'){
                    $footer_data['phone2'] = $setting['value'];
                }
                if($setting['key'] == 'general_email'){
                    $footer_data['email'] = $setting['value'];
                }
                if($setting['key'] == 'general_site_url'){
                    $footer_data['site']= $setting['value'];
                }
            }
            $view->with('top_menu_items', $top_menu_items);
            $view->with('top_logo_thumb', ImageTool::resize($top_logo,0,0));
            $view->with('top_logo_thumb_adaptive', ImageTool::resize($top_logo_adaptive,0,0));
            $view->with('footer_articles', Article::orderBy('date','desc')->where('published',1)->get()->take(3));
            $view->with('home_locales' , $home_locales);
            $view->with('footer_data',$footer_data);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/admin-lte.php', 'admin-lte'
        );
    }
}
