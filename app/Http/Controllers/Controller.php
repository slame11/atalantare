<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $paginate = 15; 

    public function __construct()
    {
        $this->middleware(function ( $request, $next ) {
            if(session()->has('locale')) {
                App::setLocale(session('locale'));
            }
            if(! session()->has('currency')) {
                session(['currency' => env('DEFAULT_CURRENCY')]);
                session(['rate' => 1]);
            }
            return $next($request);
        });
    }

    /**
     * @param $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setLang($locale)
    {
        // Chech is exist this locale
        if(in_array($locale, Config::get('translatable.locales'))) {
            App::setLocale($locale);
            session(['locale' => $locale]);
        } else {
            App::setLocale(Config::get('app.locale'));
        }
        //return redirect()->back();
    }

}
