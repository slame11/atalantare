<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 09.01.2019
 * Time: 10:27
 */

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Support\Facades\App;
use App\Models\Feedback;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        return view('article.list', [
            'articles'=>Article::where('published',1)->paginate(6)
        ]);
    }

    public function show(Request $request)
    {
        if($request->has('article')) {
            $article = Article::findOrFail($request->input('article'));
            return view('article.article', ['article'=>$article]);
        }
    }
}