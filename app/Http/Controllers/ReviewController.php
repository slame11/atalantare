<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 18.01.2019
 * Time: 16:31
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * @param Request $request
     */
    public function review(Request $request)
    {
        $request->validate([
            'nameReview' => 'required|string|max:255',
            'emailReview' => 'required|email|max:255',
            'review' => 'required|string',
        ]);

        $review = Review::create([
            'full_name' => $request->input('nameReview'),
            'email' => $request->input('emailReview'),
            'available_translation' => App::getLocale(),
        ]);

        $review->translateOrNew(App::getLocale())->review = $request->input('review');
        if($review->save()){
            response(__('general.create_success'), 200)
                ->header('Content-Type', 'text/plain');
        }
    }
}
