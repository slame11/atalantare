<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 09.01.2019
 * Time: 10:27
 */

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\ImageTool;
use App\Models\Page;
use App\Models\RealEstate;
use App\Models\RealEstateImages;

class PageController extends Controller
{


    public function index($name){

        $Page = Page::where('seo_url',$name)->first();
        if(!$Page){
            abort(404);
        }
        return view('page.index',
            [
                'page' => $Page
            ]);
    }
}
