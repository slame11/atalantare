<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App as App;

class BasicController extends Controller
{
    public $currentLocale;

    public function __construct()
    {
        $this->middleware('auth');
        $this->currentLocale = App::getLocale();
    }
    public function getNames($model)
    {
        switch ($model) {
            case 'city':
                $items = City::get();
                break;
            case 'country':
                $items = Country::get();
                break;
        }
        $locales = [];
        if ($items) {
            foreach ($items as $item) {
                if ($item->availableTranslation($this->currentLocale) != false) {
                    foreach ($item->translations as $key => $el) {
                        if ($el->locale != $this->currentLocale) {
                            $item->translations->forget($key);
                        }
                    }
                }
            }

            if ($items) {
                foreach ($items as $item) {
                    $locales[$item->id] = $item->translations[0]->name;
                }
            }
        }
        return $locales;
    }
}
