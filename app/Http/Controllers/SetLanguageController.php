<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SetLanguageController extends Controller
{
    public function setLang( $locale)
    {
        App::setLocale($locale);
        session(['locale'=>$locale]);

        return redirect()->back();
    }
}
