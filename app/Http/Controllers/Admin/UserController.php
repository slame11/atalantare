<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

use App\Models\UserAvatar;
use Auth;

class UserController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:users');
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view( 'admin.user.index',
            [
                'users' => User::orderBy('name')->where('id','!=',Auth::id())->paginate($this->paginate)
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create',
            [
                'user' => [],
                'roles' => Role::where('name', 'Admin')->get(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        $user   = new User;
////        $roles  = $request['roles'];
////        $user->fill($request->only( 'email','name','password','phone_number','avatar_id','is_active' ))->save();
        $user = User::create($request->except('avatar_id'));
        $roles  = $request['roles'];
        if($request->has('avatar_id' )){
            $avatar = UserAvatar::where('id', $request->input('avatar_id'))->first();
            if(isset($avatar)) {
                $avatar->user_id = $user->id;
                $avatar->save();
            }
        }

        //Checking if a role was selected
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::where( 'id', '=', $role )->firstOrFail();
                $user->assignRole($role_r);
            }
        }

//        if ($request->has('reset_pass_notify') && $request->input('reset_pass_notify') == '1') {
//            $user->notify(new NewUser());
//        }
        $user->save();
        return redirect()
            ->route( 'users.index' )
            ->with( 'message.level', 'success' )
            ->with( 'message.content', 'User successfully added.' );
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return redirect()->route('users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        $roles = Role::where('name', 'Admin')->get();

        $user = User::select( [ 'id','name','email','phone_number','is_active',] )
            ->where( 'id', $user->id )
            ->firstOrFail();

        return view( 'admin.user.edit', [
            'user'      => $user,
            'roles'     => $roles,
        ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->except('avatar_id','password'));
        if($request->has('avatar_id' )){
            $avatar = UserAvatar::where('id', $request->input('avatar_id'))->first();
            if(!is_null($avatar)) {
                $avatar->user_id = $user->id;
                $avatar->save();
            }else{
                $user->avatar_id = null;
            }
        }

        if ($request['password'] != '' ) {
            $input['password'] = $request['password'];
            $user->password = $input['password'];
        }

        $user->save();

        $roles = $request['roles'];
        if (isset($roles)) {
            $user->roles()->detach();
            foreach ($roles as $role) {
                $role_r = Role::where( 'id', '=', $role )->firstOrFail();
                $user->assignRole( $role_r );
            }
        }

        return redirect()
            ->route( 'users.index' )
            ->with( 'message.level', 'success' )
            ->with( 'message.content', 'User successfully updated.' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()
            ->route( 'users.index' )
            ->with( 'message.level', 'success' )
            ->with( 'message.content', 'User successfully deleted.' );
    }
}
