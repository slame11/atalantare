<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CityValidation;
use App\Models\City;
use Illuminate\Http\Request;
use App\Models\Country;
use Illumonte\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CityController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:cities');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = City::with('country')->paginate($this->paginate);
        return view('admin.city.index', ['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city = new City();
        return view('admin.city.create',
            [
                'city' => $city,
                'locales' => $city->getLocale('create'),
                'countries' => Country::get()->pluck('name','id'),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityValidation $request)
    {
        $city = City::create($request->all());
        $city->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $city->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }
        $city->save();
        return redirect()
            ->route('cities.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return view('admin.city.edit',
            [
                'city' => $city,
                'locales' => $city->getLocale('edit'),
                'countries' => Country::get()->pluck('name','id'),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(CityValidation $request, City $city)
    {
        $city->update($request->all());
        $city->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $city->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }
        $city->save();
        return redirect()
            ->route('cities.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.update_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect()
            ->route('cities.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }
}
