<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleValidation;
use App\Models\Article;
use App\Models\ImageTool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticleController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $articles = Article::paginate($this->paginate);
        return view('admin.article.index',['articles'=>$articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new Article();
        return view('admin.article.create',
            [
                'article' => $article,
                'locales' => $article->getLocale('create'),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleValidation $request)
    {
        $article = Article::create($request->all());
        $article->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $article->translateOrNew($locale)->name = $request->input('name-'.$locale);
                $article->translate($locale)->description_short = $request->input('description_short-'.$locale);
                $article->translate($locale)->description = $request->input('description-'.$locale);
                $article->translate($locale)->meta_title = $request->input('meta_title-'.$locale);
                $article->translate($locale)->meta_description = $request->input('meta_description-'.$locale);
                $article->translate($locale)->meta_keyword = $request->input('meta_keyword-'.$locale);
            }
        }

        $article->save();

        return redirect()
            ->route('articles.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $article['thumb'] = ImageTool::resize($article['image'],100,100);
        $article['thumb_big'] = ImageTool::resize($article['image_big'],100,100);

        return view('admin.article.edit',
            [
                'article' => $article,
                'locales' => $article->getLocale('edit'),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleValidation $request, Article $article)
    {
        $article->update($request->all());
        if(! $request->has('published')) {
            $article->published = 0;
        }
        $article->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $article->translateOrNew($locale)->name = $request->input('name-'.$locale);
                $article->translate($locale)->description_short = $request->input('description_short-'.$locale);
                $article->translate($locale)->description = $request->input('description-'.$locale);
                $article->translate($locale)->meta_title = $request->input('meta_title-'.$locale);
                $article->translate($locale)->meta_description = $request->input('meta_description-'.$locale);
                $article->translate($locale)->meta_keyword = $request->input('meta_keyword-'.$locale);
            }
        }

        $article->save();

        return redirect()
            ->route('articles.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.update_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()
            ->route('articles.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }
}
