<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 18.01.2019
 * Time: 14:01
 */

namespace App\Http\Controllers\Admin;

use App\GoogleAnalytics;
use App\Http\Requests\ReviewValidation;
use App\Models\Review;
use Illuminate\Http\Request;

class GoogleAnalyticsController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:admin');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ga = GoogleAnalytics::find(1);
        if(!$ga){
            $ga = new GoogleAnalytics();
        }
        $data = $request->all();
        if($data)
        {
            $ga->head = htmlspecialchars($data['head']);
            $ga->body = htmlspecialchars($data['body']);
            $ga->save();
        }
        return view('admin.ga.index',[
            'ga' => $ga,
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $ga = new GoogleAnalytics();
        return view('admin.ga.index',[
            'ga' => $ga,
        ]);
    }

    public function store(Request $request)
    {
        $ga = GoogleAnalytics::find(1);
        if(!$ga){
            $ga = new GoogleAnalytics();
        }
        $data = $request->all();
        if($data)
        {
            $ga->head = htmlspecialchars($data['head']);
            $ga->body = htmlspecialchars($data['body']);
            $ga->save();
        }

        return redirect()
            ->route('ga.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Review  $review
    * @return \Illuminate\Http\Response
    */
    public function show(Review $review)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Review  $review
    * @return \Illuminate\Http\Response
    */
    public function edit(GoogleAnalytics $ga)
    {
        return view('admin.ga.index',[
            'ga' => $ga,
        ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Review  $review
    * @return \Illuminate\Http\Response
    */
    public function update(ReviewValidation $request, Review $review)
    {
        $review->update($request->all());
        if(! $request->has('published')) {
            $review->published = 0;
        }
        $review->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('review-'.$locale) && !is_null($request->input('review-'.$locale))) {
                $review->translateOrNew($locale)->review = $request->input('review-'.$locale);
            }
        }
        $review->save();

        return redirect()
            ->route('reviews.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.update_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $review->delete();
        return redirect()
            ->route('reviews.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }

}
