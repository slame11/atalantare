<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\UserAvatar;
use Illuminate\Http\Request;

class UserAvatarController extends AdminController
{
    public function uploadAvatar(Request $request)
    {
        if($request->has('avatar')){
            $file = $request->file('avatar');
            $name = $file->getClientOriginalName();

            $avatar = UserAvatar::where('user_id', $request->input('user_id'))->first();
            if(is_null($avatar)){
                $avatar = new UserAvatar();
                $avatar->user_id = $request->input('user_id');
                $avatar->path = Storage::putFile('public/users/avatars', $file);
                $avatar->name = $name;
            }else{
                Storage::delete($avatar->path);
                $avatar->user_id = $request->input('user_id');
                $avatar->path = Storage::putFile('public/users/avatars', $file);
                $avatar->name = $name;
            }
            $avatar->save();
            $avatar->url = Storage::url($avatar->path);
            return json_encode(['avatar' => $avatar]);
        }
    }

    public function removeAvatar(Request $request)
    {
        $avatar = UserAvatar::where('id', $request->input('avatar_id'))->first();

        if(isset($avatar)) {
            Storage::delete($avatar->path);
            if(isset($avatar->user_id)) {
                $user = User::where('avatar_id', $avatar->id)->first();
                $user->avatar_id = null;
                $user->save();
            }
            $avatar->delete();
        }

        return json_encode(['deleted', 'url' => Storage::url('public/users/avatars/user.png')]);
    }
}
