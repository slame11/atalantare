<?php

namespace App\Http\Controllers\Admin;

use App\Models\RealEstateImages;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\UserAvatar;
use Illuminate\Http\Request;

class ImportGlamourapartmentsController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:import_glamourapartments');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return view('admin.import.glamourapartments', []);
    }

    public function import_glamourapartments(Request $request)
    {
        $file = $request->file('json_file');
        $cities_ids = [
            'paris' => 1,
            'bougival' => 2,
            'orgeval' => 3,
            'deauville' => 4,
            'paris-16e-arrondissement' => 1,
            'brignac' => 5,
            'cannes' => 6,
            'thiais' => 7,
            'brtigny sur orge' => 8, //works on openserver (local)
            'saint-tienne-la-thillaye' => 9,//É почему то даже маленькая такая в strlower
            'levallois perret' => 10,
            'saint-tropez' => 11,
            'nice' => 12,
            'melun' => 13
        ];
        $countries_ids = [
            'france' => 1
        ];

        $categories = [
            'flat' => 3,
            'house' => 7,
            'hotel' => 10
        ];

        $attributes_ids = [
            'living_space' => 22,
            'rooms' => 15,
            'bedrooms' => 17,
            'bathrooms' => 19,
            'toilets' => 21
        ];

        \DB::table('real_estates')->where('import_id',\App\Models\Import::IMPORT_TYPE_Glamourapartments)->delete();
        $content = json_decode(file_get_contents($file->getRealPath()), true);
        $replace_symbols = [
            'é',
            'É'
        ];
        foreach($content as $item){
            $images_source = $item['media']['images'];
            foreach($replace_symbols as $symbol){
                $item['location']['city'] = str_replace($symbol,'',$item['location']['city']);
            }
            $realestate = \App\Models\RealEstate::create(
                [
                    'ru' => [
                        'title' =>$item['name'],
                        'keyword' => '',
                        'address' => $item['location']['country']. ', '. $item['location']['city'] .', '. $item['location']['street'].' '. $item['location']['district'],
                        'media_description' => '',
                        'description' =>$item['description'],
                    ],
                    'country_id' => $countries_ids[rtrim(strtolower(utf8_decode($item['location']['country'])))],
                    'city_id' => $cities_ids[rtrim(strtolower($item['location']['city']))] ,
                    'real_estate_type_id' => 1,
                    'show_on_home_slider' => 0,
                    'show_on_home_recommended' => 0,
                    'category_id' => $categories[$item['type']],
                    'seo_url' => $this->format_uri($this->noDiacritics($item['name'])),
                    'map_point' => $item['location']['lnglat'][1] . ' '. $item['location']['lnglat'][0],
                    'price' => $item['price'],
                    'viewed' => 1,
                    'import_id' => \App\Models\Import::IMPORT_TYPE_Glamourapartments,
                    'import_object_id' => $item['id']
                ]
            );
            foreach ($images_source as $source_image){
                RealEstateImages::create([
                    'sort' => 0,
                    'real_estate_id' => $realestate->id,
                    'path' => $source_image['detail'],
                    'ru' => ['title' => $source_image['label']]
                ]);
            }

            foreach($item['capacity'] as $attribute => $value){
                \App\Models\RealEstateAttribute::create(
                    [
                        'real_estate_id' => $realestate->id,
                        'attribute_id' => $attributes_ids[$attribute],
                        'value' => $value
                    ]
                );
            }

            \DB::table('real_estates')->where('id',$realestate->id)
                ->update(['available_translation'  => 'ru']);
        }
        return view('admin.import.glamourapartments', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeValidation $request)
    {
        $attribute = Attribute::create($request->all());
        $attribute->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $attribute->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }
        $attribute->save();
        return redirect()->route('attributes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute)
    {

        $icon = ImageTool::resize($attribute->path, 100, 100);

        return view('admin.attribute.edit',
            [
                'attribute' => $attribute,
                'icon' => $icon,
                'locales' => $attribute->getLocale('edit'),
                'attributeTypes' => Attribute::getAttributeTypes(),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeValidation $request, Attribute $attribute)
    {
        $all = $request->all();
        if(!$request->has('show_on_main')){
            $all['show_on_main'] = 0;
        }
        $attribute->update($all);
        $attribute->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $attribute->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }
        $attribute->save();
        return redirect()->route('attributes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        $attribute->delete();
        return redirect()->route('attributes.index');
    }

    public function getAttributesByName(Request $request)
    {
        if($request->has('query')) {
            $query = $request->get('query','');
            $attributeTranslations = AttributeTranslation::where('name','LIKE','%'.$query.'%')->pluck('attribute_id');

            if(! empty($attributeTranslations)) {
                $attributes = Attribute::whereIn('id',$attributeTranslations)->get();
                foreach ($attributes as $key => $attribute) {
                    $attributes[$key]['attribute_type'] = Attribute::$attributeTypes[$attribute->attribute_type_id];
                }
                return response()->json($attributes);
            }
        }
    }

    public function getAttribute(Request $request)
    {
        if($request->has('attribute_id')) {
            return Attribute::findOrFail($request->input('attribute_id'));
        }
    }
}
