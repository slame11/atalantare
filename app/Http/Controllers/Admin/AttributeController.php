<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AttributeValidation;
use App\Models\Attribute;
use App\Models\AttributeTranslation;
use App\Models\AttributeType;
use App\Models\ImageTool;
use Illuminate\Http\Request;
use Illumonte\Support\Facades\DB;


class AttributeController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:attributes');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $attributes = Attribute::paginate($this->paginate);
        return view('admin.attribute.index', ['attributes' => $attributes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attribute = new Attribute();
        return view('admin.attribute.create',
            [
                'attribute' => $attribute,
                'locales' => $attribute->getLocale('create'),
                'attributeTypes' => Attribute::getAttributeTypes(),
            ]

        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeValidation $request)
    {
        $attribute = Attribute::create($request->all());
        $attribute->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $attribute->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }
        $attribute->save();
        return redirect()->route('attributes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute)
    {

        $icon = ImageTool::resize($attribute->path, 100, 100);

        return view('admin.attribute.edit',
            [
                'attribute' => $attribute,
                'icon' => $icon,
                'locales' => $attribute->getLocale('edit'),
                'attributeTypes' => Attribute::getAttributeTypes(),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeValidation $request, Attribute $attribute)
    {
        $all = $request->all();
        if(!$request->has('show_on_main')){
            $all['show_on_main'] = 0;
        }
        $attribute->update($all);
        $attribute->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $attribute->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }
        $attribute->save();
        return redirect()->route('attributes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        $attribute->delete();
        return redirect()->route('attributes.index');
    }

    public function getAttributesByName(Request $request)
    {
        if($request->has('query')) {
            $query = $request->get('query','');
            $attributeTranslations = AttributeTranslation::where('name','LIKE','%'.$query.'%')->pluck('attribute_id');

            if(! empty($attributeTranslations)) {
                $attributes = Attribute::whereIn('id',$attributeTranslations)->get();
                foreach ($attributes as $key => $attribute) {
                    $attributes[$key]['attribute_type'] = Attribute::$attributeTypes[$attribute->attribute_type_id];
                }
                return response()->json($attributes);
            }
        }
    }

    public function getAttribute(Request $request)
    {
        if($request->has('attribute_id')) {
            return Attribute::findOrFail($request->input('attribute_id'));
        }
    }
}
