<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 18.01.2019
 * Time: 14:01
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReviewValidation;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:reviews');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reviews = Review::paginate($this->paginate);
        return view('admin.review.index',['reviews'=>$reviews]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $review = new Review();
        return view('admin.review.create',
            [
                'review' => $review,
                'locales' => $review->getLocale('create'),
            ]
        );
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(ReviewValidation $request)
    {
        $review = Review::create($request->all());
        $review->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('review-'.$locale) && !is_null($request->input('review-'.$locale))) {
                $review->translateOrNew($locale)->review = $request->input('review-'.$locale);
            }
        }
        $review->save();

        return redirect()
            ->route('reviews.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Review  $review
    * @return \Illuminate\Http\Response
    */
    public function show(Review $review)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Review  $review
    * @return \Illuminate\Http\Response
    */
    public function edit(Review $review)
    {
        return view('admin.review.edit',
            [
                'review' => $review,
                'locales' => $review->getLocale('edit'),
            ]
        );
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Review  $review
    * @return \Illuminate\Http\Response
    */
    public function update(ReviewValidation $request, Review $review)
    {
        $review->update($request->all());
        if(! $request->has('published')) {
            $review->published = 0;
        }
        $review->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('review-'.$locale) && !is_null($request->input('review-'.$locale))) {
                $review->translateOrNew($locale)->review = $request->input('review-'.$locale);
            }
        }
        $review->save();

        return redirect()
            ->route('reviews.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.update_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $review->delete();
        return redirect()
            ->route('reviews.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }

}
