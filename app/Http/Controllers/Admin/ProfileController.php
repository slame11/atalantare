<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Notifications\NewUser;
use Auth;

//Enables us to output flash messaging
use Session;

class ProfileController extends AdminController
{
    public function profileEdit() {
        return view( 'admin.user.profile', [
            'user' => Auth::user(),
        ] );
    }

    public function profileSave( ProfileEdit $request ) {

        $user = Auth::user();

        $input = $request->only([ 'name', 'email', 'avatar_id']);
        if ( ! $request['password'] == '' ) {
            $input['password'] = $request['password'];
        }
        $user->fill($input)->save();

        return redirect()
            ->route( 'profile.edit' )
            ->with( 'message.level', 'success' )
            ->with( 'message.content', 'Profile successfully saved.' );
    }
}
