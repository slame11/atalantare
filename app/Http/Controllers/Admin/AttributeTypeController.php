<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AttributeTypeValidation;
use App\Models\AttributeType;
use Illuminate\Http\Request;

class AttributeTypeController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:attributeTypes');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $attributeTypes = AttributeType::paginate($this->paginate);
        return view('admin.attributeType.index', ['attributeTypes' => $attributeTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributeType = new AttributeType();
        return view('admin.attributeType.create',
            [
                'attributeType' => $attributeType,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeTypeValidation $request)
    {
        AttributeType::create($request->all());

        return redirect()->route('attributeTypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AttributeType  $attributeType
     * @return \Illuminate\Http\Response
     */
    public function show(AttributeType $attributeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AttributeType  $attributeType
     * @return \Illuminate\Http\Response
     */
    public function edit(AttributeType $attributeType)
    {
        return view('admin.attributeType.edit',
            [
                'attributeType' => $attributeType,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AttributeType  $attributeType
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeTypeValidation $request, AttributeType $attributeType)
    {
        $attributeType->update($request->all());

        return redirect()->route('attributeTypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AttributeType  $attributeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttributeType $attributeType)
    {
        $attributeType->delete();
        return redirect()->route('attributeTypes.index');
    }
}
