<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AgentValidation;
use App\Models\Agent;
use App\Models\ImageTool;
use App\Models\RealEstate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AgentController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:attributeTypes');
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agents = Agent::paginate($this->paginate);
        return view('admin.agent.index', ['agents' => $agents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agent = new Agent();

        return view('admin.agent.create',
            [
                'agent' => $agent,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgentValidation $request)
    {
        $agent = Agent::create($request->all());
        $agent->save();

        return redirect()
            ->route('agents.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {

        $agent['thumb'] = ImageTool::resize($agent['photo'],100,100);
        return view('admin.agent.edit',
            [
                'agent' => $agent,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(AgentValidation $request, Agent $agent)
    {
        $all = $request->all();
        if(!$request->has('show_on_main')){
            $all['show_on_main'] = false;
        }
        $agent->update($all);

        $agent->save();

        return redirect()
            ->route('agents.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        $agent->delete();
        return redirect()
            ->route('agents.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }
}
