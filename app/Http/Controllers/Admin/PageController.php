<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageValidation;
use App\Models\Agent;
use App\Models\Attribute;
use App\Models\GeneralSetting;
use App\Models\ImageTool;
use App\Models\Page;
use App\Models\RealEstate;
use App\Models\Review;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class PageController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pages = Page::paginate($this->paginate);
        return view('admin.page.index',['pages'=>$pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = new Page();
        return view('admin.page.create',
            [
                'page' => $page,
                'locales' => $page->getLocale('create'),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageValidation $request)
    {
        $page = Page::create($request->all());
        $page->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('title-'.$locale))) {
                $page->translateOrNew($locale)->title = $request->input('title-'.$locale);
                $page->translate($locale)->description = $request->input('description-'.$locale);
                $page->translate($locale)->meta_title = $request->input('meta_title-'.$locale);
                $page->translate($locale)->meta_keyword = $request->input('meta_keyword-'.$locale);
                $page->translate($locale)->meta_description = $request->input('meta_description-'.$locale);
            }
        }
        $page->save();


        return redirect()
            ->route('pages.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $realEstates = [];
        if($page->id == 1) {
            $realEstates = RealEstate::get();
        }

        return view('admin.page.edit',
            [
                'page' => $page,
                'locales' => $page->getLocale('edit'),
                'realEstates' => $realEstates,
                'reviews' => Review::get(),
                'services' => Service::get(),
                'agents'   => Agent::get(),
                'attributes' => [
                    'checkboxes' => Attribute::where('attribute_type_id', Attribute::ATTRIBUTE_TYPE_CHECKBOX)->get(),
                    'number'     => Attribute::where('attribute_type_id', Attribute::ATTRIBUTE_TYPE_NUMBER)->get(),
                    'slider'     => Attribute::where('attribute_type_id', Attribute::ATTRIBUTE_TYPE_SLIDER)->get()
                ],
                'banneritems' => json_decode(GeneralSetting::where('key','banner-items')->first()['value'], true),
                'homeattributes' => json_decode(GeneralSetting::where('key','home_filter_config')->first()['value'], true),
                'top_menu_items' => json_decode(GeneralSetting::where('key','top_menu_items')->first()['value'], true),
                'all_locales' => config('translatable.locales'),
                'top_logo' =>  GeneralSetting::where('key','top_logo')->first()['value'],
                'top_logo_adaptive' => GeneralSetting::where('key','top_logo_adaptive')->first()['value'],
                'top_logo_thumb' => ImageTool::resize(GeneralSetting::where('key','top_logo')->first()['value'], 0, 0),
                'top_logo_thumb_adaptive' => ImageTool::resize(GeneralSetting::where('key','top_logo_adaptive')->first()['value'], 0, 0),
                'home_locales' =>  json_decode(GeneralSetting::where('key','home_locales')->first()['value'], true),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $page->update($request->all());
        $page->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('title-'.$locale) && !is_null($request->input('title-'.$locale))) {
                $page->translateOrNew($locale)->title = $request->input('title-'.$locale);
                $page->translate($locale)->description = $request->input('description-'.$locale);
                $page->translate($locale)->meta_title = $request->input('meta_title-'.$locale);
                $page->translate($locale)->meta_keyword = $request->input('meta_keyword-'.$locale);
                $page->translate($locale)->meta_description = $request->input('meta_description-'.$locale);
            }
        }
        $page->save();
        if($page->id == Page::PAGE_HOME){
            $this->saveHomeSettings($request);
        }
        return redirect()
            ->route('pages.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.update_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        return redirect()
            ->route('pages.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }

    private function saveHomeSettings(Request $request)
    {
        // Show RealEstate on the main page top slider
        RealEstate::where('show_on_home_slider',1)->update(['show_on_home_slider' => 0]);
        if($request->has('show_on_home_slider')){
            RealEstate::whereIn('id', $request->input('show_on_home_slider'))
                ->update(['show_on_home_slider' => 1]);
        }

        // Show recommended on the main page
        RealEstate::where('show_on_home_recommended',1)->update(['show_on_home_recommended' => 0]);
        if($request->has('show_on_home_recommended')){
            RealEstate::whereIn('id', $request->input('show_on_home_recommended'))
                ->update(['show_on_home_recommended' => 1]);
        }

        //Reviews
        Review::where('published',1)->update(['published' => 0]);
        if($request->has('reviews_on_home')){
            Review::whereIn('id', $request->input('reviews_on_home'))
                ->update(['published' => 1]);
        }

        //Services
        Service::where('status',1)->update(['status' => 0]);
        if($request->has('services_on_home')){
            Service::whereIn('id', $request->input('services_on_home'))
                ->update(['status' => 1]);
        }

        //Agents
        Agent::where('show_on_main',1)->update(['show_on_main' => 0]);
        if($request->has('agents_on_home')){
            Agent::whereIn('id', $request->input('agents_on_home'))
                ->update(['show_on_main' => 1]);
        }


        //Attributes on home
        $home_attributes = [];
        $home_attributes['checkboxes']  = [];
        if($request->has('attributes_checkboxes')){
            $home_attributes['checkboxes'] = $request->input('attributes_checkboxes');
        }
        $home_attributes['slider'] = [];
        if($request->has('attributes_slider')){
            $home_attributes['slider'] = $request->input('attributes_slider');
        }
        $home_attributes['number']  = [];
        if($request->has('attributes_number')){
            $home_attributes['number'] = $request->input('attributes_number');
        }

        $Home_filter_config = GeneralSetting::firstOrNew(array('key' => 'home_filter_config'));
        $Home_filter_config->value = json_encode($home_attributes);
        $Home_filter_config->save();

        //show on main Attributes (filter on home page)
        $show_attrs = array_merge(array_keys($home_attributes['checkboxes']),array_keys($home_attributes['slider']),array_keys($home_attributes['number']));

        Attribute::where('show_on_main',1)->update(['show_on_main' => 0]);
        Attribute::whereIn('id', $show_attrs)->update(['show_on_main' => 1]);



        //Header menu
        if($request->has('top_menu_items')){
            $Home_top_menu = GeneralSetting::firstOrNew(array('key' => 'top_menu_items'));
            $Home_top_menu->value = json_encode($request->input('top_menu_items'));
            $Home_top_menu->save();
        }

        //top_menu logo
        if($request->has('top_logo')){
            $Top_logo = GeneralSetting::firstOrNew(array('key' => 'top_logo'));
            $Top_logo->value = ($request->input('top_logo'));
            $Top_logo->save();
        }

        //top_menu logo adaprive
        if($request->has('top_logo_adaptive')){
            $Logo_adaptive = GeneralSetting::firstOrNew(array('key' => 'top_logo_adaptive'));
            $Logo_adaptive->value = ($request->input('top_logo_adaptive'));
            $Logo_adaptive->save();
        }

        //banner logo
        if($request->has('banner_items')){
            $banner_items_array = [];
            foreach($request->input('banner_items') as $banner_key => $banner_item){

                if(isset($request->file('banner_items')[$banner_key]['video']) && $request->file('banner_items')[$banner_key]['video'] != ''){
                    $path = Storage::putFile('public/banner_items',$request->file('banner_items')[$banner_key]['video']);
                    $banner_item['video'] = $path;
                }
                $banner_items_array[] = $banner_item;
            }

            $banner = GeneralSetting::firstOrNew(array('key' => 'banner-items'));
            $banner->value = json_encode($banner_items_array);
            $banner->save();
        }

        //Home locales
         if($request->has('home_locales')){
             $home_locales_config = GeneralSetting::firstOrNew(array('key' => 'home_locales'));
             $home_locales_config->value = json_encode($request->input('home_locales'));
             $home_locales_config->save();
         }
    }
}
