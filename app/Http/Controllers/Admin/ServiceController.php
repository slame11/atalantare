<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use App\Http\Requests\SeviceValidation;
use Illuminate\Http\Request;
use Illumonte\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ServiceController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $services = Service::paginate($this->paginate);
        return view('admin.service.index',['services'=>$services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service = new Service();
        return view('admin.service.create',
            [
                'service' => $service,
                'locales' => $service->getLocale('create'),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeviceValidation $request)
    {
        $service = Service::create($request->all());
        $service->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $service->translateOrNew($locale)->name = $request->input('name-'.$locale);
                $service->translate($locale)->text = $request->input('text-'.$locale);
            }
        }

        $service->save();

        return redirect()
            ->route('services.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.service.edit',
            [
                'service' => $service,
                'locales' => $service->getLocale('edit'),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(SeviceValidation $request, Service $service)
    {
        $all = $request->all();
        if(!$request->has('status')){
            $all['status'] = false;
        }
        $service->update($all);

        $service->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $service->translateOrNew($locale)->name = $request->input('name-'.$locale);
                $service->translate($locale)->text = $request->input('text-'.$locale);
            }
        }
        $service->save();

        return redirect()
            ->route('services.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.update_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service $service
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect()
            ->route('services.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }
}
