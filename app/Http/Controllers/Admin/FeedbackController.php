<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FeedbackController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $feedbacks = Feedback::orderBy('id', 'desc')->paginate($this->paginate);
        foreach($feedbacks as $key=> $feedback){
            $class = 'danger';
            switch($feedback->status){
                case Feedback::FEEDBACK_STATUS_PROCESSED:
                    $class = 'success';
                    break;
                case Feedback::FEEDBACK_STATUS_PENDING:
                    $class = 'warning';
                    break;
            }
            $feedback->class = $class;
        }
        return view('admin.feedback.index',['feedbacks'=>$feedbacks]);
    }

    public function sendReaction(Request $request){
        $Feedback = Feedback::where('id',$request->input('feedback_id'))->get()->first();

        $to      = $Feedback->email;

        $subject = 'Ответ на вопрос с  Atalantare.com';

        $headers = "From: atalantare@atalantare.com\r\n";
        $headers .= "Reply-To: atalantare@atalantare.com\r\n";
        $headers .= "CC: susan@example.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        $message = '
            <b>Дата обращения:</b> '.$Feedback->created_at.' <br/>
            <b>Страница обращения:</b> '.$Feedback->link_from.' <br/>
            <b>Ваш вопрос:</b> '.$Feedback->message.' <br/>
            <b>Ответ:</b> '.$request->input('message').' <br/>
        ';

        mail($to, $subject, $message, $headers);
        return json_encode(['success'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        return view('admin.feedback.edit',[
            'feedback'=>$feedback,
            'statuses'=>Feedback::getFeedbackStatus(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $feedback)
    {
        $feedback->update($request->all());

        $feedback->save();
        return redirect()->route('feedbacks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        $feedback->delete();
        return redirect()->route('feedbacks.index');
    }
}
