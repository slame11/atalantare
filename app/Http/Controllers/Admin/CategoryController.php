<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\CategoryToType;
use App\Models\ImageTool;
use App\Models\RealEstate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:categories');
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::with('types')->paginate($this->paginate);

        return view('admin.category.index',['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
        return view('admin.category.create',
            [
                'category' => $category,
                'locales' => $category->getLocale('create'),
                'categories' => RealEstate::getRealEstateTypes(),
                'selected' => $category->types->pluck('id')->toArray(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create($request->all());
        $category->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $category->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }

        $category->save();

        return redirect()
            ->route('categories.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $category['thumb'] = ImageTool::resize($category['image'],100,100);

        return view('admin.category.edit',
            [
                'category' => $category,
                'locales' => $category->getLocale('edit'),
                'categories' => RealEstate::getRealEstateTypes(),
                'selected' => $category->types->pluck('id')->toArray(),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->update($request->all());
        $category->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if($request->has('name-'.$locale) && !is_null($request->input('name-'.$locale))) {
                $category->translateOrNew($locale)->name = $request->input('name-'.$locale);
            }
        }

        if($request->has('type_id'))
        {
            CategoryToType::where('category_id',$category->id)->delete();
            foreach($request->input('type_id') as $type_id){
                CategoryToType::create([
                    'type_id' => $type_id,
                    'category_id' => $category->id
                ]);
            }

        }
        $category->save();

        return redirect()
            ->route('categories.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()
            ->route('categories.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }
}
