<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RealEstateValidation;
use App\Models\Agent;
use App\Models\Attribute;
use App\Models\ImageTool;
use App\Models\RealEstate;
use App\Models\RealEstateAttribute;
use App\Models\RealEstateImages;
use App\Models\RealEstateTranslation;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Country;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class RealEstateController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:realEstates');
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request);
        $realEstate = (new RealEstate())->newQuery();

        if($request->has('real_estate_type_id') && $request->input('real_estate_type_id') > 0) {
            $realEstate->where('real_estate_type_id',$request->input('real_estate_type_id'));
        }
        $name ='';
        if($request->has('filter_name') && $request->input('filter_name') != '') {
            $name = $request->input('filter_name');

            $realEstates = RealEstateTranslation::where(function ($query) use($request,$name){
                $query->where('title', 'like', '%'.$name.'%')
                    ->orWhere('title', 'like', '%'.$name)
                    ->orWhere('title', 'like', $name.'%');
            })->where('locale',\Config::get('app.locale'))->get()->pluck('real_estate_id');
            $realEstate->whereIn('id',$realEstates);
        }

        $keyword ='';
        if($request->has('filter_keyword') && $request->input('filter_keyword') != '') {
            $keyword = $request->input('filter_keyword');

            $realEstates = RealEstateTranslation::where(function ($query) use($request,$keyword){
                $query->where('keyword', 'like', '%'.$keyword.'%')
                    ->orWhere('keyword', 'like', '%'.$keyword)
                    ->orWhere('keyword', 'like', $keyword.'%');
            })->where('locale',\Config::get('app.locale'))->get()->pluck('real_estate_id');
            $realEstate->whereIn('id',$realEstates);
        }

        if($request->has('country_id') && $request->input('country_id') != 0){
            $realEstate->where('country_id', $request->input('country_id'));
        }

        if($request->has('city_id') && $request->input('city_id') != 0){
            $realEstate->where('city_id', $request->input('city_id'));
        }

        $category_id = 0;
        if($request->has('category')){
            $category_id = $request->input('category');

            if($request->input('category') != 0){
                $realEstate->where('category_id','=',$request->input('category'));
            }
        }
        //dd($realEstate->with('city', 'country')->paginate($this->paginate));

        $objects = $realEstate->with('city', 'country')->paginate($this->paginate)->appends(request()->query());

        return view('admin.realEstate.index',
            [
                'keyword' => $keyword,
                'name' => $name,
                'countries' => Country::get()->pluck('name','id'),
                'category_id' => $category_id,
                'cities' => $request->has('city_id') ? City::where('country_id',$request->input('country_id'))->get()->pluck('name','id') : [],
                'country_id' => $request->input('country_id'),
                'city_id' => $request->input('city_id'),
                'realEstates' => $objects,
                'categories' => Category::all()->pluck('name', 'id'),
                'real_estate_type_id' => $request->input('real_estate_type_id'),
                'real_estate_type' => isset(RealEstate::$realestateTypes[$request->input('real_estate_type_id')]['type_name']) ? RealEstate::$realestateTypes[$request->input('real_estate_type_id')]['type_name']: ''
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $realEstate = new RealEstate();
        $realEstate->real_estate_type_id = $request->input('real_estate_type_id');
        $realEstate->rent_period = $request->input('rent_period');
        $attributes = Attribute::get();
        $attr_assoc = [];
        foreach($attributes as $key => $attribute){
            $attributes[$key]['type'] =  Attribute::$attributeTypes[$attribute->attribute_type_id]['attribute_name'];
            $attr_assoc[$attribute->id] = $attribute;
        }

        $rent_period = RealEstate::getRentPeriods();

        foreach($rent_period as $key => $item){
            $rent_period[$key] = \Lang::get('general.'.$item);
        }
        return view('admin.realEstate.create',
            [
                'agents' => Agent::orderBy('name')->get()->pluck('name', 'id'),
                'realEstate' => $realEstate,
                'realEstateImages' => [],
                'realEstateTypes' => RealEstate::getRealEstateTypes(),
                'countries' => Country::get()->pluck('name','id'),
                'cities' => City::where('country_id', Country::get()->pluck('id')[0])->get()->pluck('name','id'),
                'categories' => Category::get()->pluck('name','id'),
                'attributes' => [],
                'all_attributes' => $attributes,
                'rent_period' => $rent_period,
                'locales' => $realEstate->getLocale('create'),
                'attr_assoc' => $attr_assoc
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RealEstateValidation $request)
    {
        $data = $request->all();
        if(isset($data['price_upon_request']) && $data['price_upon_request'])
        {
            if(!isset($data['price']))
                $data['price'] = 0;
            if(!isset($data['currency']))
                $data['currency'] = 'EUR';
        }
        else {
            $data['price_upon_request'] = 0;
        }
        $realEstate = RealEstate::create($data);

        $realEstate->available_translation = $request->all();
        foreach(config('translatable.locales') as $locale) {
            if ($request->has('title-' . $locale) && !is_null($request->input('title-' . $locale)))
            {
                $realEstate->translateOrNew($locale)->title = $request->input('title-' . $locale);
                $realEstate->translate($locale)->description = $request->input('description-'.$locale);
                $realEstate->translate($locale)->keyword = $request->input('keyword-'.$locale);
                $realEstate->translate($locale)->media_description = $request->input('media_description-'.$locale);
                $realEstate->translate($locale)->address = $request->input('address-'.$locale);

//                $realEstate->seo_url = $this->format_uri($this->noDiacritics($request->input('title-' . $locale)));

            }

        }
        switch (true) {
            case ($request->has ('seo_url') && !is_null($request->input('seo_url'))):
                $realEstate->seo_url = $request->input('seo_url');
                break;
            case ($request->has ('title-en')&& !is_null($request->input('title-en'))):
                $realEstate->seo_url = $this->format_uri($this->noDiacritics($request->input('title-en')));
                break;
            default:
                foreach(config('translatable.locales') as $locale) {
                    if ($request->isset('title-' . $locale) && !is_null($request->input('title-' . $locale))) {
                        $realEstate->seo_url = $this->format_uri($this->noDiacritics($request->input('title-' . $locale)));
                        break;
                    }
                }
                break;
        }

        if($request->has('agents'))
        {
            $realEstate->agents()->attach($request->input('agents'));
        }

        $realEstate->save();
        $this->saveAttributes($request,$realEstate);
        $this->saveImages($request, $realEstate);

        return redirect()->route('realEstates.index',['real_estate_type_id'=>$realEstate->real_estate_type_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RealEstate  $realEstate
     * @return \Illuminate\Http\Response
     */
    public function show(RealEstate $realEstate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RealEstate  $realEstate
     * @return \Illuminate\Http\Response
     */
    public function edit(RealEstate $realEstate)
    {
        $attributes =Attribute::get();
        $attr_assoc = [];
        foreach($attributes as $key => $attribute){
            $attributes[$key]['type'] =  Attribute::$attributeTypes[$attribute->attribute_type_id]['attribute_name'];
            $attr_assoc[$attribute->id] = $attribute;
        }

        $rent_period = RealEstate::getRentPeriods();

        foreach($rent_period as $key => $item){
            $rent_period[$key] = \Lang::get('general.'.$item);
        }
        return view('admin.realEstate.edit',
            [
                'agents' => Agent::orderBy('name')->get()->pluck('name', 'id'),
                'realEstate' => $realEstate,
                'realEstateImages' => $this->getImages($realEstate->id),
                'realEstateTypes' => RealEstate::getRealEstateTypes(),
                'countries' => Country::get()->pluck('name','id'),
                'cities' => City::where('country_id',$realEstate->country_id)->get()->pluck('name','id'),
                'categories' => Category::get()->pluck('name','id'),
                'attributes' => $this->getAttributes($realEstate->id),
                'all_attributes' => $attributes,
                'rent_period' => $rent_period,
                'locales' => $realEstate->getLocale('edit'),
                'all_locales' => config('translatable.locales'),
                'attr_assoc' => $attr_assoc
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RealEstate  $realEstate
     * @return \Illuminate\Http\Response
     */
    public function update(RealEstateValidation $request, RealEstate $realEstate)
    {
        $data = $request->except('show_on_home_slider');
        if(isset($data['price_upon_request']) && $data['price_upon_request'])
        {
            if(!isset($data['price']))
                $data['price'] = 0;
            if(!isset($data['currency']))
                $data['currency'] = 'EUR';
        }
        else {
            $data['price_upon_request'] = 0;
        }
        $realEstate->update($data);
        if(!$request->has('show_on_home_slider')){
            $all_request['show_on_home_slider'] = false;
        }
        $realEstate->available_translation = $request->all();

        foreach(config('translatable.locales') as $locale) {
            if ($request->has('title-' . $locale) && !is_null($request->input('title-' . $locale))) {
                $realEstate->translateOrNew($locale)->title = $request->input('title-' . $locale);
                $realEstate->translate($locale)->description = $request->input('description-' . $locale);
                $realEstate->translate($locale)->keyword = $request->input('keyword-' . $locale);
                $realEstate->translate($locale)->media_description = $request->input('media_description-' . $locale);
                $realEstate->translate($locale)->address = $request->input('address-' . $locale);

//                $realEstate->seo_url = $this->format_uri($this->noDiacritics($request->input('title-' . $locale)));
            }

        }
        switch (true) {
            case ($request->has ('seo_url') && !is_null($request->input('seo_url'))):
                $realEstate->seo_url = $request->input('seo_url');
                break;
            case ($request->has ('title-en')&& !is_null($request->input('title-en'))):
                $realEstate->seo_url = $this->format_uri($this->noDiacritics($request->input('title-en')));
                break;
            default:
                foreach(config('translatable.locales') as $locale) {
                    if ($request->isset('title-' . $locale) && !is_null($request->input('title-' . $locale))) {
                        $realEstate->seo_url = $this->format_uri($this->noDiacritics($request->input('title-' . $locale)));
                        break;
                    }
                }
                break;
        }

        if($request->has('agents'))
        {
            $realEstate->agents()->detach();
            $realEstate->agents()->attach($request->input('agents'));
        }

        $realEstate->save();
        $this->saveAttributes($request,$realEstate);
        $this->saveImages($request,$realEstate);

        return redirect()->route('realEstates.index',['real_estate_type_id'=>$realEstate->real_estate_type_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RealEstate  $realEstate
     * @return \Illuminate\Http\Response
     */
    public function destroy(RealEstate $realEstate)
    {
        $realEstate->delete();
        return redirect()->route('realEstates.index',['real_estate_type_id'=>$realEstate->real_estate_type_id]);
    }

    private function saveAttributes(Request $request, RealEstate $realEstate)
    {
        RealEstateAttribute::where('real_estate_id', $realEstate->id)->delete();
        if($request->has('attribute_id')) {
            $attributes = $request->input('attribute_id');
            foreach($attributes as $key => $value) {
                RealEstateAttribute::create([
                    'real_estate_id' => $realEstate->id,
                    'attribute_id' => $key,
                    'value' => $value,
                ]);
            }
        }
    }

    private function saveImages(Request $request, RealEstate $realEstate)
    {
        if($request->has('image')) {
            $images = $request->input('image');
            RealEstateImages::where('real_estate_id',$realEstate->id)->delete();

            foreach($images as $key => $value) {
                if($value){
                    $RealEstateImages = RealEstateImages::create([
                        'real_estate_id' => $realEstate->id,
                        'sort' => $request->input('image_sort.'.$key),
                        'path' => $value,
                    ]);

                    foreach(config('translatable.locales') as $locale) {
                        if($request->has('image_title.'.$key.'.'.$locale) && !is_null($request->input('image_title.'.$key.'.'.$locale)))
                        {
                            $RealEstateImages->translateOrNew($locale)->title = $request->input('image_title.'.$key.'.'.$locale);
                        }
                    }

                    $RealEstateImages->save();
                }
            }
        }

    }

    private function getAttributes($id)
    {
        $attributeArray = [];
        $key = 1;
        $attributes = RealEstateAttribute::where('real_estate_id', $id)->get();
        foreach($attributes as $attribute) {
            $tempAttr = Attribute::findOrFail($attribute->attribute_id);
            $attributeArray[$key]['attribute_id'] = $attribute->attribute_id;
            $attributeArray[$key]['name'] =  $tempAttr->name;
            $attributeArray[$key]['type'] =  Attribute::$attributeTypes[$tempAttr->attribute_type_id]['attribute_name'];
            $attributeArray[$key]['value'] =  $attribute->value;
            $key++;
        }
        return $attributeArray;
    }

    private function getImages($id)
    {
        $Images =  RealEstateImages::where('real_estate_id', $id)->get();
        foreach($Images as $key => $image){
            $Images[$key]['thumb'] = ImageTool::resize($image['path'], 100, 100);
            $Images[$key]['locales'] = $image->getLocale('edit');
        }

        return $Images;
    }

    public function multipleUpload(Request $request)
    {
        $file = $request->file('file');
        $file->path = Storage::putFile('public/images/catalog', $file);
        return view('admin.realEstate.result', [
            'locales' => config('translatable.locales'),
            'thumb' => $file->path,
            'path' => substr($file->path, 14),
        ]);
    }
}
