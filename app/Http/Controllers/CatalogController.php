<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 24.01.2019
 * Time: 09:58
 */

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\ImageTool;
use App\Models\RealEstate;
use App\Models\RealEstateAttribute;
use App\Models\RealEstateTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    public function index($country = null, Request $request)
    {

        $get_data = $request->all();
        if($country) {
            $country = Country::where('seo_url','=',$country)->first();
            $get_data['country'] = $country->id;
        }

        return view('catalog.index', [
            'attributes' => [
                'checkboxes' => Attribute::where([['attribute_type_id', Attribute::ATTRIBUTE_TYPE_CHECKBOX],['show_on_main', 1]])->get(),
                'number'     => Attribute::where([['attribute_type_id', Attribute::ATTRIBUTE_TYPE_NUMBER],['show_on_main', 1]])->get(),
                'slider'     => Attribute::where([['attribute_type_id', Attribute::ATTRIBUTE_TYPE_SLIDER],['show_on_main', 1]])->get()
            ],
            'country_cat' => $country,
            'filter' => $get_data,
            'products' => $this->getProducts($country,$request),
            'max_price' => preg_replace('/[^0-9.]+/', '', RealEstate::orderBy('price', 'desc')->first()['price']),
            'min_price' => preg_replace('/[^0-9.]+/', '', RealEstate::orderBy('price', 'asc')->first()['price']) ,
            'countries' => Country::all(),
            'cities'    => isset($get_data['country']) ? City::where('country_id',$get_data['country'])->get()  : [],
            'categories' => Category::all(),
            'types' => RealEstate::getRealEstateTypes(),
        ]);
    }

    public function getProducts($country = null, Request $request){
        $realEstate = (new RealEstate())->newQuery();

        $checkboxIds = [];
        $numberIds = [];
        $sliderIds = [];
        $ids = [];

        $get_data = $request->all();
        if($request->has('attributes')) {
            foreach($request->input('attributes') as $key => $value) {
                if($value == 'on') {
                    array_push($checkboxIds, $key);
                }else if(is_array($value)) {
                    $sliderIds[$key] = $value;
                } else {
                    $numberIds[$key] = $value;
                }
            }
        }

        // Checkboxes
        if(! empty($checkboxIds)) {
            $ids[] = array_unique(RealEstate::leftJoin('real_estate_attribute','real_estates.id', '=', 'real_estate_attribute.real_estate_id')
                ->whereIn('real_estate_attribute.attribute_id',$checkboxIds)
                ->pluck('real_estate_id')->toArray());
        }

        // Numbers
        foreach($numberIds as $attr_key => $attr_value){
            if((int)$attr_value <= 0){
                unset($numberIds[$attr_key]);
            }
        }
        if(! empty($numberIds)) {
            $ids[] = array_unique($RealEstates_numbers =  RealEstate::leftJoin('real_estate_attribute','real_estates.id', '=', 'real_estate_attribute.real_estate_id')
                ->where(function ($query) use($numberIds){
                    foreach($numberIds as $key => $value){
                        $query->orWhere([
                            ['attribute_id', '=', $key],
                            ['value', '=', $value],
                        ]);
                    }
                })->pluck('real_estate_id')->toArray());
        }
        // Sliders
        $sliders = Attribute::where(['attribute_type_id' => Attribute::ATTRIBUTE_TYPE_SLIDER])->get()->keyBy('id');
        foreach ($sliderIds as $slider_key => $sliders_item){
            if($sliders_item['min'] == $sliders[$slider_key]->val_min &&
                $sliders_item['max'] == $sliders[$slider_key]->val_max){
                unset($sliderIds[$slider_key]);
            }
        }
        if(! empty($sliderIds)) {
            $ids[] = array_unique(RealEstate::leftJoin('real_estate_attribute','real_estates.id', '=', 'real_estate_attribute.real_estate_id')
                ->where(function ($query) use($sliderIds){
                    foreach($sliderIds as $key => $value){
                        $query->orWhere([
                            ['attribute_id', '=', $key],
                            ['value', '>=', $value['min']],
                            ['value', '<=', $value['max']],
                        ]);
                    }
                })->pluck('real_estate_id')->toArray());
        }

        if($ids){
            if(count($ids) > 1){
                $merged = array_intersect(...$ids);
            }else{
                $merged = $ids[0];
            }
        }

        if((!isset($merged) || !$merged) && (!empty($sliderIds) || !empty($numberIds) || !empty($checkboxIds))){
            $merged[] = 0;
        }
        if($request->has('title') && $request->input('title') != '') {
            $realEstates = RealEstateTranslation::where(function ($query) use($request){
                $query->where('title', 'like', '%'.$request->input('title').'%')
                    ->orWhere('title', 'like', '%'.$request->input('title'))
                    ->orWhere('title', 'like', $request->input('title').'%');
            })->get()->pluck('real_estate_id');
            $realEstate->whereIn('id',$realEstates);
        }

        if($request->has('country')) {
            if($request->input('country') != 0){
                $realEstate->where('country_id','=',$request->input('country'));
            }
        }

        if($country) {
                $realEstate->where('country_id','=',$country->id);
        }

        if($request->has('city')) {
            if($request->input('city') != 0){
                $realEstate->where('city_id','=',$request->input('city'));
            }
        }

        if($request->has('category') && $request->input('category') && $request->input('category')[0] > 0) {
            $realEstate->whereIn('category_id',array_values($request->input('category')));
        }

        if($request->has('real_estate_type_id')) {
            if($request->input('real_estate_type_id') != 0){
                $realEstate->where('real_estate_type_id','=',$request->input('real_estate_type_id'));
            }
        }

        if($request->has('price')) {
            $realEstate->where([['price','>=',$request->input('price')['min']], ['price','<=',$request->input('price')['max']]]);
        }


        // And others parametrs
        if(! empty($merged)) {
            $realEstate->whereIn('id',$merged);
        }
        if($request->has('sort') && $request->input('sort') != '') {
            $sort = explode('.',$request->input('sort'))[0];
            $priority = explode('.',$request->input('sort'))[1];

            $realEstate->orderBy($sort, $priority);
        }else{
//            $realEstate->orderBy('id', 'desc');
            $realEstate->orderBy('price', 'desc');
        }

        $products = $realEstate->with('attributes','image')->paginate(15)->appends(request()->query());

        // use pivot for get column value
        if($products){
            foreach($products as $key => $realEstate){
                $numberAttributes = [];

                if($realEstate->image && $realEstate->image->first()){
                    $products[$key]['thumb'] = ImageTool::resize($realEstate->image->first()->path,435,290);
                }

                foreach ($realEstate->attributes as $k => $attribute) {
                    if(in_array($attribute->attribute_type_id, [2,3])){
                        $attribute->path = ImageTool::resize($attribute->path, 0,0);
                        $numberAttributes[] = $attribute;
                    }
                }
                $products[$key]['numberAttributes'] = $numberAttributes;
            }

        }

         return view('catalog.partials.products', [
             'products' => $products,
             'country_cat' => $country,
             'filter' => $get_data,
           ]);
    }

    public function setProductView(Request $request)
    {
        if($request->has('product_view_type')) {
            session(['product_view_type' => $request->input('product_view_type')]);
        }
    }

    public function setFilterToggle(Request $request)
    {
        if($request->has('filter_toggle')) {
             session(['filter_toggle' => (int)$request->input('filter_toggle')]);
        }
    }
}
