<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 09.01.2019
 * Time: 10:27
 */

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Article;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\GeneralSetting;
use App\Models\ImageTool;
use App\Models\Page;
use App\Models\RealEstate;
use App\Models\Review;
use App\Models\Service;
use Illuminate\Support\Facades\App;
use App\Models\Feedback;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * @param $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setLang($locale)
    {
        App::setLocale($locale);
        session(['locale'=>$locale]);

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $recommended =  RealEstate::with('attributes','image')->where('show_on_home_recommended',1)->take(6)->get();

        foreach($recommended as $key => $realEstate){
            $numberAttributes = [];

            if($realEstate->image && $realEstate->image->first()){
                $recommended[$key]['thumb'] = ImageTool::resize($realEstate->image->first()->path,435,290);
            }

            foreach ($realEstate->attributes as $k => $attribute) {
                if(in_array($attribute->attribute_type_id, [2,3])){
                    $attribute->path = ImageTool::resize($attribute->path, 0,0);
                    $numberAttributes[] = $attribute;
                }
            }
            $recommended[$key]['numberAttributes'] = $numberAttributes;
        }

        return view('main',[
            'page' => Page::findOrFail(Page::$dinamicPages[1]['page_id']),
            'realEstates_for_home_catalog' => $recommended,
            'banneritems' => json_decode(GeneralSetting::where('key','banner-items')->first()['value'], true),
            'countries' => Country::with('cities')->get()->sortBy('name'),
            'categories' => Category::all(),
            'types' => RealEstate::getRealEstateTypes(),
            'services' => Service::where('status',1)->take(8)->get(),
            'agents' => Agent::where('show_on_main',1)->take(8)->get(),
            'reviews' => Review::where('published',1)->orderBy('created_at','desc')->take(6)->get(),
            'articles' => Article::orderBy('date','desc')->where('published',1)->take(3)->get(),
            'latitude' => GeneralSetting::where('key', 'general_map_coords_latitude')->first(),
            'longitude' => GeneralSetting::where('key', 'general_map_coords_longitude')->first(),
            'address' => GeneralSetting::where('key', 'general_address')->first(),
            'extend' => 0,
            'attributes' => [
                'checkboxes' => Attribute::where([['attribute_type_id', Attribute::ATTRIBUTE_TYPE_CHECKBOX],['show_on_main', 1]])->get(),
                'number'     => Attribute::where([['attribute_type_id', Attribute::ATTRIBUTE_TYPE_NUMBER],['show_on_main', 1]])->get(),
                'slider'     => Attribute::where([['attribute_type_id', Attribute::ATTRIBUTE_TYPE_SLIDER],['show_on_main', 1]])->get()
            ],
            'homeattributes' => json_decode(GeneralSetting::where('key','home_filter_config')->first()['value'], true),
        ]);
    }

    public function getCitiesByCountryId(Request $request)
    {
        if($request->has('country_id')) {
            $cities = City::where('country_id',$request->input('country_id'))
                ->get()
                ->sortBy('name')
                ->pluck('name','id');

            return $cities;
        }
        return [];
    }

    public function getCategoriesByTypeId(Request $request)
    {
        if($request->has('type_id')) {
            if($request->input('type_id') != 0){
                $categories = Category::with('types')->whereHas('types',function ($query) use($request) {
                    $query->where('type_id',$request->input('type_id'));
                })
                    ->get()
                    ->sortBy('name')
                    ->pluck('name','id');
            }else{
                $categories = Category::all()
                    ->sortBy('name')
                    ->pluck('name','id');
            }

            return $categories;
        }
        return [];
    }


    /**
     * @param Request $request
     */
    public function feedback(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'nullable|string|max:16',
            'message' => 'required|string',
        ]);

        $feedback = Feedback::create([
           'user' => $request->input('name'),
           'email' => $request->input('email'),
           'phone' => $request->input('phone'),
           'link_from' => $request->input('link_from'),
           'agent' => $request->has('agent') ? $request->input('agent') :'',
           'message' =>  $request->input('message')
        ]);

        $settings = GeneralSetting::where('key','general_email')->get()->first();
        $to      = $settings['value'];

        $subject = 'Обращение с контактной формы Atalantare.com №'.$feedback->id;

        $headers = "From: atalantare@atalantare.com\r\n";
        $headers .= "Reply-To: atalantare@atalantare.com\r\n";
        $headers .= "CC: susan@example.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $agent = $request->input('agent') != '' ? '<b>Агент:</b> '.$request->input('agent').' <br/>' : '';
        $message = '
            <b>Дата обращения:</b> '.$feedback->created_at.' <br/>
              '.$agent.' 
            <b>Страница обращения:</b> '.$request->input('link_from').' <br/>
            <b>Имя пользователя:</b> '.$request->input('name').' <br/>
            <b>Email:</b> '.$request->input('email').' <br/>
            <b>Телефон:</b> '.$request->input('phone').' <br/>          
            <b>Сообщение:</b> '.$request->input('message').' <br/>
        ';

        mail($to, $subject, $message, $headers);
        if($feedback->save()){
            response(__('general.create_success'), 200)
                ->header('Content-Type', 'text/plain');
        }


    }

    public function subscribe(Request $request)
    {

    }

    /**
     * @param $url
     * @param $assoc
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendGet($url, $assoc = true)
    {
        $client  = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, []);
        $answer = json_decode($response->getBody()->getContents(), $assoc);
        return $answer;
    }

    public function setCurrency($currency='EUR')
    {
        $currency = strtoupper($currency);
        if($currency == 'EUR') {
            session(['currency' => $currency]);
            session(['rate' => 1]);
            return redirect()->back();
        }

        $result = \Config::get('currency_rates');
        if(array_key_exists($currency,$result['rates'])){
            session(['currency' => $currency]);
            session(['rate' => $result['rates'][$currency]]);
        }
        return redirect()->back();
    }
}
