<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 09.01.2019
 * Time: 10:27
 */

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\GeneralSetting;
use App\Models\ImageTool;
use App\Models\RealEstate;
use App\Models\RealEstateImages;
use Illuminate\Http\Request;

class RealEstateController extends Controller
{
    /**
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($name){

        $numberAttributes = [];
        $checkboxAttributes = [];
        $agents = [];

        $realEstate = RealEstate::where('seo_url',$name)->with(['attributes', 'agents'])->firstOrFail();
        if($realEstate){
            $agents = $realEstate->agents;
            foreach ($realEstate->attributes as $attribute) {

                $attribute->path = ImageTool::resize($attribute->path, 50,50);

                if($attribute->attribute_type_id == 1){
                    $checkboxAttributes[] = $attribute;
                }

                if(in_array($attribute->attribute_type_id, [2,3])){
                    $numberAttributes[] = $attribute;
                }
            }
        }
        $images = RealEstateImages::where('real_estate_id',$realEstate->id)->get();
        foreach($images as $key => $image){
            $images[$key]['thumb'] = ImageTool::resize($image->path, 0,0);
        }

        $rates = \Config::get('currency_rates');
        $price_array = [];

        foreach(config('currency.currencies') as $currency){
            if($currency == $realEstate->currency){
                $price_array[$currency] = $realEstate->price;
            }elseif($currency == 'EUR') {
                $eur_price = $realEstate->price/$rates['rates'][$realEstate->currency];
                $price_array[$currency]  = round($eur_price);
            }else{
                $price_array[$currency] = $realEstate->price*$rates['rates'][$currency];
            }

        }


        return view('product.index',
            [
                'realEstate' => $realEstate,
                'realEstateImages' =>$images,
                'numberAttributes' =>$numberAttributes,
                'checkboxAttributes' =>$checkboxAttributes,
                'agents' => $agents,
                'extend' => 0,
                'popular_real_states' => RealEstate::with('image','country','city')->orderBy('viewed','desc')->where('country_id',$realEstate->country_id)->take(20)->get()->toArray(),
                'latitude' => GeneralSetting::where('key', 'general_map_coords_latitude')->first(),
                'longitude' => GeneralSetting::where('key', 'general_map_coords_longitude')->first(),
                'address' => GeneralSetting::where('key', 'general_address')->first(),
                'prices' => $price_array
            ]
        );
    }

    public function viewed(Request $request)
    {
        if($request->has('realEstateId') && $request->has('viewed')) {
            $realEstate = RealEstate::find($request->input('realEstateId'));
            $realEstate->viewed += 1;
            $realEstate->save();

            return response()->json(['viewed' => $realEstate->viewed]);
        }
        return response()->json(['viewed' => $request->input('viewed')]);
    }
}
