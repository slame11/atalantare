<?php

namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use App\Models\Page;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.index', [
            'page' => Page::findOrFail(Page::PAGE_CONTACTS),
            'address' => GeneralSetting::where('key', 'general_address')->first(),
            'phone1' => GeneralSetting::where('key', 'general_phone_1')->first(),
            'phone2' => GeneralSetting::where('key', 'general_phone_2')->first(),
            'email' => GeneralSetting::where('key', 'general_email')->first(),
            'site' => GeneralSetting::where('key', 'general_site_url')->first(),
            'latitude' => GeneralSetting::where('key', 'general_map_coords_latitude')->first(),
            'longitude' => GeneralSetting::where('key', 'general_map_coords_longitude')->first(),
            'extend' => 1
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
