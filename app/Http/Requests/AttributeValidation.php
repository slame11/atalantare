<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AttributeValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->can('attributes')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $translateRule = [];

        foreach (config('translatable.locales') as $locale) {
            $translateRule['available_translation-' . $locale] = 'integer|min:1|max:1';
            $translateRule['name-' . $locale] = 'required_with:available_translation-'. $locale.'|max:255';
        }

        return array_merge([
            'attribute_type_id' => 'required|integer',
            'val_min' => 'integer|min:0',
            'val_max' => 'integer|min:0',
            'created_by' => 'integer|min:0',
            'modified_by' => 'integer|min:0',
        ], $translateRule);
    }
}
