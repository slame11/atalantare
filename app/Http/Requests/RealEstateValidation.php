<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class RealEstateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->can('realEstates')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $translateRule = [];

        $isLocale = false;
        foreach (config('translatable.locales') as $locale) {
            if (! is_null($this['title-' . $locale]) && $this['available_translation-' . $locale] == 1) {
                $isLocale = true;
            }
        }

        if ($isLocale) {
            foreach (config('translatable.locales') as $locale) {
                $translateRule['available_translation-' . $locale] = 'integer|min:1|max:1';
                $translateRule['title-' . $locale] = 'required_with:available_translation-' . $locale;
                $translateRule['description-' . $locale] = 'required_with:available_translation-' . $locale;
                $translateRule['address-' . $locale] = 'required_with:available_translation-' . $locale . '|max:255';
            }
        } else {
            $translateRule['seo_url'] = 'required|string';
        }



        return array_merge([
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
//            'seo_url' => 'required',
            'real_estate_type_id' => 'required|integer',
            'map_point' => 'regex:/^[0-9\-\.]+ [0-9\-\.]+$/',
            'show_on_home_slider' => 'integer|min:0|max:1',
            'price'               => 'integer|min:0',
            'attribute_id.*'      =>  'integer|min:0'

        ], $translateRule);
    }
}
