<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ReviewValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->can('reviews')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $translateRule = [];

        foreach (config('translatable.locales') as $locale) {
            $translateRule['available_translation-' . $locale] = 'integer|min:1|max:1';
            $translateRule['review-' . $locale] = 'required_with:available_translation-'. $locale;
        }

        return array_merge([
            'full_name'=> 'required|max:255',
            'email'=>'required|email|max:255',
            'published'=>'integer|min:1|max:1',
        ],$translateRule);
    }
}
