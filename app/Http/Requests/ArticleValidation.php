<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ArticleValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->can('articles')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $translateRule = [];

        foreach (config('translatable.locales') as $locale) {
            $translateRule['available_translation-' . $locale] = 'integer|min:1|max:1';
            $translateRule['name-' . $locale] = 'required_with:available_translation-'. $locale.'|max:255';
            $translateRule['description_short-' . $locale] = 'required_with:available_translation-'. $locale.'|max:255';
            $translateRule['description-' . $locale] = 'string|nullable';
            $translateRule['meta_title-' . $locale] = 'required_with:available_translation-'. $locale.'|max:255';
            $translateRule['meta_description-' . $locale] = 'required_with:available_translation-'. $locale.'|max:255';
            $translateRule['meta_keyword-' . $locale] = 'string|nullable|max:255';
        }


        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return array_merge([
                    'image_show' => 'integer|min:1|max:1',
                    'published' => 'integer|min:1|max:1',
                    'viewed' => 'integer|min:0',
                    'date' => 'date',
                    'slug' => 'required|unique:articles,slug,'
                ], $translateRule);
            }
            case 'PUT':
            case 'PATCH': {
                return array_merge([
                    'image_show' => 'integer|min:1|max:1',
                    'published' => 'integer|min:1|max:1',
                    'viewed' => 'integer|min:0',
                    'date' => 'date',
                    'slug' => 'required|unique:articles,slug,' . $this->article->id,
                ], $translateRule);
            }
            default:
                return [];
        }
    }
}
