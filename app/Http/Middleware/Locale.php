<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 02.04.2019
 * Time: 10:02
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->lang;
        if(in_array($locale, Config::get('translatable.locales'))) {
            App::setLocale($locale);
            session(['locale' => $locale]);
        } else {
            App::setLocale(Config::get('app.locale'));
        }
        return $next($request);
    }
}
