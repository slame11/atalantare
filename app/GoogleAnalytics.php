<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleAnalytics extends Model
{
    //
    protected $fillable = ['head','body'];

    public static function getHead(){
        $ga = GoogleAnalytics::find(1);
        if($ga)
        {
            return htmlspecialchars_decode($ga->head);
        }
        return false;
    }

    public static function getBody(){
        $ga = GoogleAnalytics::find(1);
        if($ga)
        {
            return htmlspecialchars_decode($ga->body);
        }
        return false;
    }
}
