<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 18.01.2019
 * Time: 14:04
 */

namespace App\Models;

use Dimsav\Translatable\Translatable;

class Review extends TranslateModel
{
    use Translatable;

    protected $fillable = [
        'full_name',
        'email',
        'published',
        'available_translation',
        'created_by',
        'modified_by',
    ];

    public $translatedAttributes = [
        'review',
    ];

    protected $with = ['translations'];
}
