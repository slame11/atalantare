<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class CategoryToType extends Model
{

    protected $fillable = [
        'category_id',
        'type_id',
    ];

    public $timestamps = false;
    protected $table = 'category_to_type';

    public function categories()
    {
        return $this->belongsTo('App\Models\Category');
    }
}