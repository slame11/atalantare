<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 10.01.2019
 * Time: 09:45
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTranslation extends Model
{
    public $timestamps = false;
}
