<?php
/**
 * Created by PhpStorm.
 * User: Root
 * Date: 09.01.2019
 * Time: 19:52
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    protected $fillable = [
        'message',
        'action',
    ];

    public $timestamps = false;
}
