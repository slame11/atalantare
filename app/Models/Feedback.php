<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{

    const FEEDBACK_STATUS_NEW = 0;
    const FEEDBACK_STATUS_PROCESSED = 1;
    const FEEDBACK_STATUS_PENDING = 2;

    /**
     * @var array of Feedback Types
     */

    public static $feedbackStatuses = [
        self::FEEDBACK_STATUS_NEW => [
            'feedback_status_id'   =>  self::FEEDBACK_STATUS_NEW,
            'feedback_status_name' => 'NEW',
        ],
        self::FEEDBACK_STATUS_PROCESSED => [
            'feedback_status_id'   =>  self::FEEDBACK_STATUS_PROCESSED,
            'feedback_status_name' => 'PROCESSED',
        ],
        self::FEEDBACK_STATUS_PENDING => [
            'feedback_status_id'   =>  self::FEEDBACK_STATUS_PENDING,
            'feedback_status_name' => 'PENDING',
        ]
    ];

    public static function getFeedbackStatus()
    {
        $feedbackStatuses = [];
        foreach(self::$feedbackStatuses as $key => $value){
            $feedbackStatuses[$key] = $value['feedback_status_name'];
        }

        return $feedbackStatuses;
    }


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feedbacks';

    protected $fillable = [
        'user',
        'email',
        'phone',
        'status',
        'available_translation',
        'created_by',
        'modified_by',
        'link_from',
        'message',
        'agent',
        'reaction'
    ];
}
