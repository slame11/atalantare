<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class RealEstate extends TranslateModel
{
    use Translatable;

    const REALESTATE_TYPE_SALE = 1;
    const REALESTATE_TYPE_RENT = 2;
    const REALESTATE_TYPE_INVESTING = 3;

    const RENT_PER_YEAR =1;
    const RENT_PER_MONTH =2;
    const RENT_PER_WEEK =3;
    const RENT_PER_DAY =4;
    const RENT_PER_HOUR =5;


    /**
     * @var array of Realestate Types
     */
    public static $realestateTypes = [
        self::REALESTATE_TYPE_SALE => [
            'type_id'   =>  self::REALESTATE_TYPE_SALE,
            'type_key'  => 1,
            'type_name' => 'SALE',
        ],
        self::REALESTATE_TYPE_RENT => [
            'type_id'   =>  self::REALESTATE_TYPE_RENT,
            'type_key'  => 2,
            'type_name' => 'RENT',
        ],
        self::REALESTATE_TYPE_INVESTING => [
            'type_id'   =>  self::REALESTATE_TYPE_INVESTING,
            'type_key'  => 3,
            'type_name' => 'INVESTMENT',
        ]
    ];

    public static $rentPeriods = [
        self::RENT_PER_YEAR => [
            'type_id'   =>  self::RENT_PER_YEAR,
            'type_key'  => 1,
            'type_name' => 'per_year',
        ],
        self::RENT_PER_MONTH => [
            'type_id'   =>  self::RENT_PER_MONTH,
            'type_key'  => 2,
            'type_name' => 'per_month',
        ],
        self::RENT_PER_WEEK => [
            'type_id'   =>  self::RENT_PER_WEEK,
            'type_key'  => 3,
            'type_name' => 'per_week',
        ],
        self::RENT_PER_DAY => [
            'type_id'   =>  self::RENT_PER_DAY,
            'type_key'  => 4,
            'type_name' => 'per_day',
        ],
        self::RENT_PER_HOUR => [
            'type_id'   =>  self::RENT_PER_HOUR,
            'type_key'  => 5,
            'type_name' => 'per_hour',
        ],

    ];

    public static function getRealEstateTypes()
    {
        $realestateTypes = [];
        foreach(self::$realestateTypes as $type_id => $type){
            $realestateTypes[$type_id] = __('general.real_estate_type_id.'.$type_id);
        }

        return $realestateTypes;
    }

    public static function getRentPeriods()
    {
        $rentPeriods = [];
        foreach(self::$rentPeriods as $type_id => $type){
            $rentPeriods[$type_id] = $type['type_name'];
        }

        return $rentPeriods;
    }

    protected $fillable = [
        'country_id',
        'city_id',
        'price',
        'price_upon_request',
        'real_estate_type_id',
        'map_point',
        'category_id',
        'created_by',
        'modified_by',
        'available_translation',
        'rent_period',
        'seo_url',
        'show_on_home_slider',
        'show_on_home_recommended',
        'import_id',
        'import_object_id',
        'currency'
    ];

    public $translatedAttributes = [
        'description',
        'title',
        'keyword',
        'media_description',
        'address',
    ];

    protected $with = ['translations'];

    public static $currencySign = [
      'GBP' => '£',
      'EUR' => '€',
      'USD' => '$',
      'CNY' => '¥',
      'RUB' => '₽',
      'CHF' => '₣',
    ];
    protected $appends = array('price_format');

    public function getPriceFormatAttribute()
    {
        if(isset($this->attributes['price'],$this->attributes['currency'])){
            $rent_str = '';
            if(isset($this->attributes['rent_period'], $this->attributes['real_estate_type_id']) && $this->attributes['real_estate_type_id'] == self::REALESTATE_TYPE_RENT){
                $rent_str = '<span> / '.\Lang::get('general.'.self::$rentPeriods[$this->attributes['rent_period']]['type_name']).'</span>';
            }
            return '<p class="price_container"><span class="price_container-symbol">'.self::$currencySign[$this->attributes['currency']] .' </span><span class="price_container-price">'.number_format($this->attributes['price'] ,0,',',',').' </span>'.$rent_str.'</p>' ;
        }
    }

    public function image(){
        return $this->hasMany(RealEstateImages::class);
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function attributes()
    {
        // withPivot($value) return all values from `real_estate_attribute` table
        return $this->belongsToMany('App\Models\Attribute','real_estate_attribute')->withPivot('value');
    }

    public function agents()
    {
        return $this->belongsToMany('App\Models\Agent','agent_real_estate');
    }
}
