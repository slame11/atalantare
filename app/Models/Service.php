<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;

class Service extends TranslateModel
{
    use Translatable;

    protected $fillable = [
        'image',
        'status',
        'available_translation',
        'created_by',
        'modified_by',
    ];

    public $translatedAttributes = [
        'name',
        'text',
    ];

    protected $with = ['translations'];
}
