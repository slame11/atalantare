<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;

class Article extends TranslateModel
{
    use Translatable;

    protected $fillable = [
        'image',
        'image_big',
        'image_show',
        'published',
        'viewed',
        'date',
        'slug',
        'available_translation',
        'created_by',
        'modified_by',
    ];

    public $translatedAttributes = [
        'name',
        'description_short',
        'description',
        'meta_title',
        'meta_description',
        'meta_keyword',
    ];

    protected $with = ['translations'];

    public function getAuthor(){
        return User::findOrFail($this->attributes['created_by'])->name;
    }

    public function getFrontImage(){
        return ImageTool::resize($this->attributes['image'], 0, 0);
    }

    public function getBannerImage(){
        return ImageTool::resize($this->attributes['image_big'], 0, 0);
    }
}
