<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;

class Country extends TranslateModel
{
    use Translatable;

    protected $fillable = [
        'map_point',
        'symbols',
        'available_translation',
        'seo_url',
        'currency',
        'created_by',
        'modified_by',
    ];

    public $translatedAttributes = [
        'name',
    ];

    protected $with = ['translations'];

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }
}
