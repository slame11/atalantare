<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealEstateTranslation extends Model
{

    protected $fillable = [
        'title',
        'description',
        'keyword',
        'address',
        'media_description'
    ];
    public $timestamps = false;
}
