<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageTool extends Model
{
    public static function resize($filename, $width, $height) {
        $DIR_IMAGE = $_SERVER['DOCUMENT_ROOT'].'/storage/images/';

        if(strpos($filename,'http') !== false){
            return $filename;
        }

        if (!is_file($DIR_IMAGE . $filename)) {
            return null;
        }


        $request = request();
        if($width == 0 && $height == 0){
            return $request->server->get('REQUEST_SCHEME') .'://'. $request->server->get('HTTP_HOST') . '/storage/images/' . $filename;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $image_old = $filename;
        $image_new = 'cache/' . mb_substr($filename, 0, mb_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file($DIR_IMAGE . $image_new) || (filemtime($DIR_IMAGE . $image_old) > filemtime($DIR_IMAGE . $image_new))) {
            list($width_orig, $height_orig, $image_type) = getimagesize($DIR_IMAGE . $image_old);

            if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
                return $DIR_IMAGE . $image_old;
            }

            $path = '';

            $directories = explode('/', dirname($image_new));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir($DIR_IMAGE . $path)) {
                    @mkdir($DIR_IMAGE . $path, 0777);
                }
            }

            if ($width_orig != $width || $height_orig != $height) {
                $image = new Image($DIR_IMAGE . $image_old);
                $image->resize($width, $height, 'w');
                $image->create($DIR_IMAGE . $image_new);
            } else {
                copy($DIR_IMAGE . $image_old, $DIR_IMAGE . $image_new);
            }
        }

        return $request->server->get('REQUEST_SCHEME') .'://'. $request->server->get('HTTP_HOST') . '/storage/images/' . $image_new;
    }
}
