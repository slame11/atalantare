<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Page extends TranslateModel
{
    use Translatable;

    const PAGE_HOME = 1;
    const PAGE_ABOUS_US = 1;
    const PAGE_CONTACTS = 1;

    static public $dinamicPages = [
        self::PAGE_HOME => [
            'page_id' => self::PAGE_HOME,
            'page_name' => 'Home',
        ],
        self::PAGE_ABOUS_US => [
            'page_id' => self::PAGE_ABOUS_US,
            'page_name' => 'About us',
        ],
        self::PAGE_CONTACTS => [
            'page_id' => self::PAGE_CONTACTS,
            'page_name' => 'Contacts',
        ],
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    protected $fillable = [
        'seo_url',
        'available_translation'
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'meta_title',
        'meta_keyword',
        'meta_description'
    ];

    protected $with = ['translations'];
}
