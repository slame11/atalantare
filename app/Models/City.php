<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class City extends TranslateModel
{
    use Translatable;

    protected $fillable = [
        'country_id',
        'map_point',
        'zip_code',
        'available_translation',
        'created_by',
        'modified_by',

    ];

    public $translatedAttributes = [
        'name',
    ];

    protected $with = ['translations'];

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function realestates()
    {
        return $this->hasMany('App\Models\RealEstate');
    }
}
