<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Attribute extends TranslateModel
{
    use Translatable;

    const ATTRIBUTE_TYPE_CHECKBOX = 1;
    const ATTRIBUTE_TYPE_SLIDER = 2;
    const ATTRIBUTE_TYPE_NUMBER = 3;

    /**
     * @var array of Attribute Types
     */

    public static $attributeTypes = [
        self::ATTRIBUTE_TYPE_CHECKBOX => [
            'attribute_id'   =>  self::ATTRIBUTE_TYPE_CHECKBOX,
            'attribute_name' => 'CHECKBOX',
        ],
        self::ATTRIBUTE_TYPE_SLIDER => [
            'attribute_id'   =>  self::ATTRIBUTE_TYPE_SLIDER,
            'attribute_name' => 'SLIDER',
        ],
        self::ATTRIBUTE_TYPE_NUMBER => [
            'attribute_id'   =>  self::ATTRIBUTE_TYPE_NUMBER,
            'attribute_name' => 'NUMBER',
        ]
    ];

    public static function getAttributeTypes()
    {
        $attributeTypes = [];
        foreach(self::$attributeTypes as $attribute_id => $attribute){
            $attributeTypes[$attribute_id] = $attribute['attribute_name'];
        }

        return $attributeTypes;
    }

    protected $fillable = [
        'attribute_type_id',
        'val_min',
        'val_max',
        'path',
        'show_on_main',
        'available_translation',
        'created_by',
        'modified_by',
    ];

    public $translatedAttributes = [
        'name',
    ];

    protected $with = ['translations'];

    public function realEstates()
    {
        return $this->belongsToMany('App\Models\RealEstate','real_estate_attribute');
    }

}
