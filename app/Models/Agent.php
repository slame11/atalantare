<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable = [
        'photo',
        'name',
        'email',
        'address',
        'phone',
        'mobile',
        'show_on_main',
        'organization',
    ];

    public function realestates()
    {
        return $this->belongsToMany('App\Models\RealEstate','agent_real_estate');
    }
}
