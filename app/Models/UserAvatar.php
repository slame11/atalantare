<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAvatar extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_avatars';

    protected $fillable = [
        'user_id', 'name', 'path',
    ];

}
