<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class RealEstateImages extends TranslateModel
{
    use Translatable;

    public $timestamps = false;

    protected $fillable = [
        'real_estate_id',
        'sort',
        'path',
    ];

    public $translatedAttributes = [
        'locale',
        'title',
    ];

    protected $with = ['translations'];



    public function real_estate()
    {
        return $this->belongsTo(RealEstate::class);
    }
}
