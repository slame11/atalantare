<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Category extends TranslateModel
{
    use Translatable;

    protected $fillable = [
        'image',
        'created_by',
        'modified_by',
        'available_translation',
    ];

    public $translatedAttributes = [
        'name',
    ];

    protected $with = ['translations'];

    public function types()
    {
        return $this->hasMany('App\Models\CategoryToType','category_id', 'id');
    }
}