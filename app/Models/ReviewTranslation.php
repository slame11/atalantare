<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 18.01.2019
 * Time: 14:19
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewTranslation extends Model
{
    public $timestamps = false;
}
