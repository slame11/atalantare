<?php
/**
 * Created by PhpStorm.
 * User: Medoff
 * Date: 15.01.2019
 * Time: 16:32
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealEstateAttribute extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'real_estate_attribute';

    public $timestamps = false;

    protected $fillable = [
        'real_estate_id',
        'attribute_id',
        'value',
    ];

    public function real_estate()
    {
        return $this->belongsTo('App\Models\RealEstate','real_estate_id');
    }

    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute','attribute_id');
    }
}
