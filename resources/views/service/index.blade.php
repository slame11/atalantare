<!-- Services block START -->
<div class="section_services">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_services_header">
                    <p class="main_header">
                        @isset($home_locales[Config('app.locale')]['service']['title']) {{$home_locales[Config('app.locale')]['service']['title']}} @else {{__('general.services')}} @endisset

                        <span>@isset($home_locales[Config('app.locale')]['service']['title']) {{$home_locales[Config('app.locale')]['service']['title']}} @else {{__('general.services')}} @endisset</span>
                    </p>
                </div>
            </div>
            <div class="col-12">
                <div class="row justify-content-center">
                    @foreach($services as $service)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="section_services_item">
                            <div class="section_services_item_image">
                                @php
                                    $service->thumb = \App\Models\ImageTool::resize($service->image,0,0);
                                @endphp
                                <img src="{{$service->thumb}}" alt="">
                                <div class="section_services_item_image_square"></div>
                            </div>
                            <div class="section_services_item_title">
                                <p>{{$service->name}}</p>
                            </div>
                            <div class="section_services_item_desc">
                                <p>{{ str_limit($service->text, $limit = 100, $end = '...') }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Services block END -->