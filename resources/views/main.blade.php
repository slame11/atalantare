
@extends('layouts-na.index')
@section('title'){{$page->meta_title}}@stop
@section('meta-section')
    <meta name="description" content="{{ $page->meta_description }}">
    <meta name="keywords" content="{{ $page->meta_keyword }}">
@stop
@section('content')
    <!-- Main banner -->
    @include('layouts-na.banner')
    <!-- Main banner end -->

    <!-- Main catalog -->
    @include('home_catalog.index')
    <!-- Main catalog end -->

    <!-- Services block START -->
    @include('service.index')
    <!-- Services block END -->

    <!-- Why us block START -->
    @isset($home_locales[Config('app.locale')]['why_us']['title'])
    <div class="section_why_us">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-12">
                            <p class="main_header">
                                @isset($home_locales[Config('app.locale')]['why_us']['title']) {{$home_locales[Config('app.locale')]['why_us']['title']}} @else {{__('general.why_us')}} @endisset
                                <span>   @isset($home_locales[Config('app.locale')]['why_us']['title']) {{$home_locales[Config('app.locale')]['why_us']['title']}} @else {{__('general.why_us')}} @endisset</span>
                            </p>
                            <p class="section_why_us_subtitle">
                                @isset($home_locales[Config('app.locale')]['why_us']['subtitle']) {!! $home_locales[Config('app.locale')]['why_us']['subtitle'] !!} @endisset
                            </p>
                        </div>
                        <div class="col-12">
                            @isset($home_locales[Config('app.locale')]['why_us']['description']) {!! $home_locales[Config('app.locale')]['why_us']['description'] !!} @endisset
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 section_why_us_image_left">
                    <div class="section_why_us_image_left_wrapper">
                        <img src="{{ Storage::url('public/images/why_us/why_us_house.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- Why us block END -->

    <!-- Happy client block START -->
    @if(count($reviews) > 0)
    @include('reviews.index')
    @endif
    <!-- Happy client block END -->


    <!-- Modal -->
    <div class="modal fade" id="happyClientReview" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">{{__('general.add_review')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nameReview">{{__('general.user')}}</label>
                        <input type="text" class="form-control" id="nameReview">
                    </div>
                    <div class="form-group">
                        <label for="emailReview">{{__('general.email')}}</label>
                        <input type="email" class="form-control" id="emailReview">
                    </div>
                    <div class="form-group">
                        <label for="review">{{__('general.review')}}</label>
                        <textarea rows="8" class="form-control" id="review"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('general.cancel')}}</button>
                    <button type="button" class="btn btn-primary" id="send_review_form">{{__('general.send')}}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- World map block START -->
    @include('worldmap.index')
    <!-- World map block END -->

    <!-- Our agent block START -->
    @include('agent.index')
    <!-- Our agent block END -->

    <!-- Latest posts block START -->
    @include('article.index')
    <!-- Latest posts block END -->

    <!-- Contacts block START -->
    @include('contact.contact')
    <!-- Contacts block END -->
@endsection

