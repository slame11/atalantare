<div class="section_happy_clients">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="main_header">
                    @isset($home_locales[Config('app.locale')]['reviews']['title']) {{$home_locales[Config('app.locale')]['reviews']['title']}} @else {{__('general.reviews')}} @endisset
                    <span>@isset($home_locales[Config('app.locale')]['reviews']['title']) {{$home_locales[Config('app.locale')]['reviews']['title']}} @else {{__('general.reviews')}} @endisset</span>
                </p>
            </div>
            <div class="col-12">
                <div class="section_happy_clients_slider_wrapper">
                    <div class="owl-carousel owl-theme section_happy_clients_slider_wrapper_carousel">
                        @foreach($reviews as $review)
                        <div class="section_happy_clients_slider_wrapper_carousel_item item">
                            <div class="section_happy_clients_slider_wrapper_carousel_item_text">
                                <p>{{$review->review}}</p>
                            </div>
                            <div class="section_happy_clients_slider_wrapper_carousel_item_info">
                                <span class="name">{{$review->full_name}}</span>
                                <span class="work"></span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="section_happy_clients_button_wrapper">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-default text-black text-uppercase" data-toggle="modal" data-target="#happyClientReview">
                        {{__('general.add_review')}}
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>