@extends('layouts-na.index')

@section('content')

    @if($article->getBannerImage())
    <!-- Banner blog START -->
    <div class="banner-blog" style="background-image: url({{ $article->getBannerImage() }});">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner-blog-wrapper">
                        {{--<p>{{ __('general.blog-header') }}</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner blog END -->
    @endif
    
    <!-- Breadcrumbs block START -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul>
                        <li><a href="/">{{ __('general.home_bread') }}</a></li>
                        <li><a href="{{ route('articles') }}">{{ __('general.blog_home_bread') }}</a></li>
                        <li>{{ $article->name }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs block END -->

    <!-- Latest posts block START -->
    <div class="blog_list_post">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="main_header">{{ $article->name }}</p>
                    <p>
                        <span class="blog_list_post_author">by {{ $article->getAuthor() }}</span>
                        <span class="blog_list_post_date">{{ date('M d, Y', strtotime($article->date)) }} at {{ date('g:i a', strtotime($article->date)) }}</span>
                    </p>
                </div>
                <div class="col-12">
                    <div class="blog_list_post_description">
                        <p>{!! $article->description !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection