<div class="section_latest_posts">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="main_header">
                    @isset($home_locales[Config('app.locale')]['blog']['title']) {{$home_locales[Config('app.locale')]['blog']['title']}} @else {{__('general.blog')}} @endisset
                    <span>  @isset($home_locales[Config('app.locale')]['blog']['title']) {{$home_locales[Config('app.locale')]['blog']['title']}} @else {{__('general.blog')}} @endisset</span>
                </p>
            </div>
            <div class="col-12">
                <div class="section_latest_posts_wrapper">
                    <div class="row">
                        @foreach($articles as $article)
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <a href="{{ route('article', ['article'=>$article]) }}" class="section_latest_posts_wrapper_post">
                                <div class="section_latest_posts_wrapper_post_image">
                                    <img src="{{ $article->getFrontImage() }}" alt="">
                                </div>
                                <div class="section_latest_posts_wrapper_post_information">
                                    <div class="section_latest_posts_wrapper_post_information_address">
                                        <p>{{$article->name}}</p>
                                    </div>
                                    <div class="section_latest_posts_wrapper_post_information_description">
                                        <p>{{$article->description_short}}</p>
                                    </div>
                                    <div class="section_latest_posts_wrapper_post_information_date">
                                        <i class="fa fa-calendar-alt"></i>
                                        <span>{{$article->date}}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="section_latest_posts_button_wrapper text-center">
                    <a href="{{ route('articles') }}" class="btn btn-default text-black text-uppercase">{{ __('general.show_all') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>