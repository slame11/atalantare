@extends('layouts-na.index')

@section('content')
    <!-- Breadcrumbs block START -->
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">{{ __('general.home_bread') }}</a></li>
                <li>{{ __('general.blog_home_bread') }}</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumbs block END -->

    <!-- Latest posts block START -->
    <div class="section_latest_posts blog_list">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="main_header">{{ __('general.our_blog_title') }}<span>{{ __('general.our_blog_title') }}</span></p>
                </div>
                <div class="col-12">
                    <div class="section_latest_posts_wrapper">
                        <div class="row">
                            @forelse($articles as $article)
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                    <a href="{{ route('article', ['article'=>$article]) }}" class="section_latest_posts_wrapper_post">
                                        <div class="section_latest_posts_wrapper_post_image">
                                            @if($article->image_show == 1)
                                                <img src="{{ $article->getFrontImage() }}" alt="">
                                            @endif
                                        </div>
                                        <div class="section_latest_posts_wrapper_post_information">
                                            <div class="section_latest_posts_wrapper_post_information_address">
                                                <p>{{$article->name}}</p>
                                            </div>
                                            <div class="section_latest_posts_wrapper_post_information_description">
                                                <p>{{$article->description_short}}</p>
                                            </div>
                                            <div class="section_latest_posts_wrapper_post_information_date">
                                                <i class="fa fa-calendar-alt"></i>
                                                <span>{{$article->date}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @empty
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                    {{__('general.empty')}}
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Latest posts block END -->
    <div class="pagination">
        <div class="container">
           {{$articles->links()}}
        </div>
    </div>
@endsection
