<div class="home_catalog-filter">
    <form action="/catalog" method="get">
        <div class="row py-1">
            <div class="col-6 col-lg-3 col-sm-6 col-xl-4 col-md-3 home_catalog-filter__search">
                <input type="text" value="" name="title" placeholder="{{__('general.where')}}" class="form-control" />
            </div>
            <div class="d-flex col-sm-12 flex-wrap justify-content-md-start justify-content-center col-xl-6 col-lg-7 col-md-6 p-0">

                <select id="transactions_filter" name="real_estate_type_id">
                    @foreach($types as $key => $name)
                        <option value="{{$key}}">{{$name}}</option>
                    @endforeach
                </select>

                <select id="country_filter" placeholder="{{__('general.country')}}" name="country">
                    @foreach($countries as $country)
                        <option value="{{$country->id}}">{{$country->name}}</option>
                    @endforeach
                </select>
                <select id="city_filter" placeholder="{{__('general.city')}}" name="city">
                    <option value="0" selected="selected">{{__('general.city')}}</option>
                </select>

                <select id="category_filter" placeholder="{{__('general.categories')}}" title="{{__('general.categories')}}" name="category[]" >
                    <option value="">{{__('general.categories')}}</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>

                {{--<input type="text" id="date_filter" placeholder="Choose date" value="" />--}}
            </div>
            <div class="col-6 col-lg-2 col-sm-6 col-xl-2 col-md-3 home_catalog-filter__buttons d-flex justify-content-center justify-content-md-end">

                <button class="btn btn-default text-uppercase">
                    {{__('general.search_button')}}
                </button>
                <button class="btn btn-primary d-none" type="button" id="advanced_search">
                    <span><i class="fas fa-sliders-h"></i></span>
                </button>
            </div>
        </div>
        <div class="row home_catalog-filter__advanced m-0">
           {{-- <input type="hidden" name="advanced_search" value="0" />
            <div class="d-flex justify-content-center flex-sm-row flex-column home_catalog-filter-counters col-sm-12">
                @foreach($attributes['number'] as $number)
                    <div class="home_catalog-filter__countinput">
                        <label for="beds_filter" class="d-flex align-items-center mb-0 justify-content-center"><span>{{$number->name}}:</span><button type="button" class="btn btn-simple btn_minus"><i class="fas fa-minus"></i></button><input class="form-control" name="attributes[{{$number->id}}]" type="text" value="@if(isset($homeattributes['number'][$number->id])){{$homeattributes['number'][$number->id]}}@else 1 @endif" id="attribute_number-{{$number->id}}" /> <button type="button" class="btn btn-simple background-gray btn_plus"><i class="fas fa-plus  text-white"></i></button></label>
                    </div>
                @endforeach
            </div>
            <div class="col-md-6">
                @foreach($attributes['checkboxes'] as $checkbox)
                    <label class="custom_checkbox text-uppercase">
                        <input type="checkbox" name="attributes[{{$checkbox->id}}]" @if(isset($homeattributes['checkboxes'][$checkbox->id]) && $homeattributes['checkboxes'][$checkbox->id] == 1) checked @endif />
                        <span class="checkmark"></span>{{$checkbox->name}}
                    </label>
                @endforeach
            </div>
            <div class="col-md-6">
                @foreach($attributes['slider'] as $slider)
                    <div class="home_catalog-filter__advanced-slider">
                        <input type="hidden" class="min_value" name="attributes[{{$slider->id}}][min]" />
                        <input type="hidden" class="max_value" name="attributes[{{$slider->id}}][max]" />
                        <p><span class="option-name">{{$slider->name}}</span> <span class="options-info"><b>{{ __('general.word_from') }}</b>  <span class="home_catalog-filter__advanced-slider__min">23</span>  <b>{{ __('general.word_to') }}</b>  <span class="home_catalog-filter__advanced-slider__max">600</span></span></p>
                        <div data-min="{{$slider->val_min}}" data-max="{{$slider->val_max}}" data-selected-min="@if(isset($homeattributes['slider'][$slider->id]['min'])){{$homeattributes['slider'][$slider->id]['min']}}@endif" data-selected-max="@if(isset($homeattributes['slider'][$slider->id]['max'])){{$homeattributes['slider'][$slider->id]['max']}}@endif" class="filter-slider" id="slider-{{$slider->id}}"></div>
                    </div>
                @endforeach

            </div>--}}
        </div>
    </form>
</div>
@push('js')
    <script type="text/javascript">
    </script>
@endpush