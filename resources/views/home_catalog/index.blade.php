<div class="home_catalog">
    <div class="container">
        @include('home_catalog.filter')
    </div>
    <div class="container">

        @include('home_catalog.products')
    </div>
</div>