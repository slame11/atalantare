<div class="home_catalog-products">
    <div id="home_product_carousel" class="owl-carousel">

        @foreach($realEstates_for_home_catalog as $realEstate)
        <div class="item ">
            <div class="home_catalog-products__item">
                <div class="home_catalog-products__item-content">
                    <div class="home_catalog-products__item-content__background">
                        <div class="home_catalog-products__item-content__background-thumb">
                            <a href="{{ route('realestate', ['name' => $realEstate->seo_url]) }}">
                                <img src="{{$realEstate->thumb}}" />
                            </a>
                            <div class="home_catalog-products__item-content__background-thumb__price d-flex align-items-center justify-content-between">


                                @if($realEstate->price_upon_request)<p class="price_container"><span class="price_container-price">{!! __('general.price_upon_request') !!} </span></p>

                                @else
                                    {!! $realEstate->price_format !!}
                                @endif
                                <span><img src="{{ Storage::url('public/images/arrow-right.png') }}" /></span>
                            </div>
                        </div>
                    </div>
                    <div class="home_catalog-products__item-content__info">
                        <div class="home_catalog-products__item-content__info-address">
                            <span>{{$realEstate->city->name}}</span>
                            <p>{{$realEstate->address}}</p>
                        </div>
                        <div class="home_catalog-products__item-content__info-attributes row">
                            @foreach($realEstate->numberAttributes as $numberAttribute)
                                <div class="home_catalog-products__item-content__info-attributes__item col-sm-6">
                                    @if(! is_null($numberAttribute->path)) <img src="{{ $numberAttribute->path }}" /> @endif
                                    <span class="font-weight-bold">{{ $numberAttribute->pivot->value }} {{ $numberAttribute->name }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
