<!-- World map block START -->
<div class="section_world_map">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="main_header">{{ __('general.worldwide') }}</p>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="section_world_map_countries_wrapper">
                    <div class="row">
                        @foreach($countries as $country)
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="section_world_map_countries_wrapper_column">
                                <div class="section_world_map_countries_wrapper_column_group">
                                    <div class="title"><a href="{{ route('catalog',['country'=>$country->id]) }}">{{ $country->name }}</a></div>
                                    @if(isset($country->cities))
                                    <ul class="list">
                                        @foreach($country->cities as $city)
                                        <li><a href="{{ route('catalog',['country'=>$country->id,'city'=>$city->id]) }}">{{ $city->name }}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- World map block END -->
