<div class="section_our_agents">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="main_header">
                    @isset($home_locales[Config('app.locale')]['agents']['title']) {{$home_locales[Config('app.locale')]['agents']['title']}} @else {{__('general.agents')}} @endisset
                    <span>@isset($home_locales[Config('app.locale')]['agents']['title']) {{$home_locales[Config('app.locale')]['agents']['title']}} @else {{__('general.agents')}} @endisset</span>
                </p>
                <p class="section_our_agents_subtitle">
                    @isset($home_locales[Config('app.locale')]['agents']['subtitle']) {{$home_locales[Config('app.locale')]['agents']['subtitle']}} @else {{__('general.agents_subtitle')}} @endisset
                </p>
            </div>
            <div class="col-12">
                <div class="section_our_agents_wrapper">
                    <div class="row">
                        @foreach($agents as $agent)
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="section_our_agents_wrapper_agent">
                                <div class="section_our_agents_wrapper_agent_image">
                                    @php
                                        $agent->thumb = \App\Models\ImageTool::resize($agent->photo, 0, 0);
                                    @endphp
                                    <img src="{{ $agent->thumb }}" alt="">
                                    <div class="section_our_agents_wrapper_agent_image_overlay">
                                        <div class="section_our_agents_wrapper_agent_image_overlay_contact_wrapper">
                                            <div class="section_our_agents_wrapper_agent_image_overlay_contact_wrapper_title">Contact</div>
                                            <div class="section_our_agents_wrapper_agent_image_overlay_contact_wrapper_item">
                                                <div class="icon">
                                                    <img src="{{ Storage::url('public/images/agents/address.png') }}" alt="">
                                                </div>
                                                <div class="text">{{$agent->address}}</div>
                                            </div>
                                            <div class="section_our_agents_wrapper_agent_image_overlay_contact_wrapper_item">
                                                <div class="icon">
                                                    <img src="{{ Storage::url('public/images/agents/phone.png') }}" alt="">
                                                </div>
                                                <div class="text">
                                                    <a href="tel:{{$agent->phone}}">{{$agent->phone}}</a>
                                                </div>
                                            </div>
                                            <div class="section_our_agents_wrapper_agent_image_overlay_contact_wrapper_item">
                                                <div class="icon">
                                                    <img src="{{ Storage::url('public/images/agents/world.png') }}" alt="">
                                                </div>
                                                <div class="text">
                                                    <a href="mailto:{{$agent->email}}">{{$agent->email}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="section_our_agents_wrapper_agent_desc">
                                    <p class="name">{{$agent->name}}</p>
                                    <p class="works">{{$agent->organization}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>