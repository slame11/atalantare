<!-- Footer block START -->

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4">
                <p class="footer-title"> @isset($home_locales[Config('app.locale')]['footer']['seo_title']) {{$home_locales[Config('app.locale')]['footer']['seo_title']}} @else {{__('general.footer_title')}} @endisset </p>
                <p>@isset($home_locales[Config('app.locale')]['footer']['seo_description']) {!! $home_locales[Config('app.locale')]['footer']['seo_description'] !!} @else {{__('general.footer_description')}} @endisset</p>
                <label class="mb-0">
                    <input type="text" id="subscribe_input" value="" placeholder="Email address" />
                    <button class="footer-sent_mail"><img src="{{ Storage::url('public/images/sent-mail.png') }}" /></button>
                </label>
            </div>
            <div class="col-xl-3 col-lg-3 d-md-none d-lg-block">
                <p class="footer-title">@isset($home_locales[Config('app.locale')]['footer']['blog_title']) {!! $home_locales[Config('app.locale')]['footer']['blog_title'] !!} @else {{__('general.footer_description')}} @endisset</p>
                @foreach($footer_articles as $article)
                    <a href="{{ route('article', ['article'=>$article]) }}" class="footer-blog_title">{{$article->name}}</a>
                    <br/>
                @endforeach
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3">
                <p class="footer-title">
                    @isset($home_locales[Config('app.locale')]['footer']['menu_title']) {!! $home_locales[Config('app.locale')]['footer']['menu_title'] !!} @else {{__('general.footer_description')}} @endisset
                </p>
                <ul>
                    @foreach($top_menu_items as $menu_item)
                        <li><a href="{{$menu_item['url']}}">{{$menu_item['locales'][Config::get('app.locale')]}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-5">
                <p class="footer-title">
                    @isset($home_locales[Config('app.locale')]['footer']['contact_title']) {!! $home_locales[Config('app.locale')]['footer']['contact_title'] !!} @else {{__('general.footer_description')}} @endisset
                </p>
                <p class="footer-location">{{$footer_data['address']}}</p>
                <p class="footer-telephone">{{ $footer_data['phone1'] }} <br />{{ $footer_data['phone2'] }}</p>
                <p class="footer-planet">{{ $footer_data['email'] }} <br />{{ $footer_data['site'] }}</p>
            </div>
        </div>
    </div>
</div>
<div  id="contact_form_popup" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                @include('contact.partial.form')
            </div>

        </div>
    </div>
</div>
<div class="contacts_block_modal modal fade" id="contacts_block_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <span class="contacts_block_modal-shadow_text">Thank you</span>
                <p><span>{{ __('general.thank-you') }}</span>, {{ __('general.message-accept') }}</p>
                <p>{{ __('general.contact-soon') }}</p>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-default text-black" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Footer block END -->

