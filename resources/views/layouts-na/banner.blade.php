<div id="banner">
    <div class="banner-overlay">
        <div class="owl-carousel">
            @if(isset($banneritems))
                @foreach($banneritems as $banneritem)
                    @if($banneritem['type'] == 'image')
                        <div class="item">
                            <img src="{{ Storage::url('public/images/'.$banneritem['image']) }}"/>
                        </div>
                    @else
                        <div class="item" data-videosrc="{{ Storage::url($banneritem['video']) }}">
                            <img src="{{ Storage::url('public/images/'.$banneritem['thumb']) }}"/>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
</div>