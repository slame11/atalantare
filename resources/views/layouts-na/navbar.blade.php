
<div class="header @if(Request::is('/')) transparent_header @endif">
    <div class="container">
        <div class="align-items-center navbar navbar-expand-lg navbar-dark">
            <div class="header_item col-md-10 col-sm-9 col-10 col-lg-3 p-0 d-flex d-lg-block align-items-center">
                <a class="header_logo" href="/"><img src="{{$top_logo_thumb}}" /></a>
                <a class="header_logo_adaptive" href="/"><img src="{{$top_logo_thumb_adaptive}}" /></a>
                <div class="d-block d-lg-none ml-4 m-auto">
                    <a href="tel:{{  $footer_data['phone1'] }}">{{$footer_data['phone1']}}</a> <br />
                    <a href="tel:{{ $footer_data['phone2']}}">{{$footer_data['phone2']}}</a>
                </div>
            </div>
            <button class="navbar-toggler collapsed" id="menu_toggle_button" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="navbar-collapse collapse col-sm-9 col-md-9 col-12 justify-content-between"  id="navbarNavAltMarkup">
                <div class=" header_item">
                    <div class="header_item-menu navbar-nav">
                        @foreach($top_menu_items as $menu_item)
                            <a class="nav-item" href="{{$menu_item['url']}}">{{$menu_item['locales'][Config::get('app.locale')]}}</a>
                        @endforeach
                    </div>
                </div>

                <div class="header_item float-lg-right position-relative float-none">
                    <select id="lanugage_select">
                        <option @if(Config::get('app.locale') == 'en') selected @endif data-link="{{ route('setlang',['locale'=>'en']) }}" data-description="(EN)" data-imagesrc ="{{ Storage::url('public/images/flag-us.png') }}"></option>
                        <option @if(Config::get('app.locale') == 'ch') selected @endif data-link="{{ route('setlang',['locale'=>'ch']) }}" data-description="(CHN)" data-imagesrc ="{{ Storage::url('public/images/flag-chn.png') }}"></option>
                        <option @if(Config::get('app.locale') == 'es') selected @endif data-link="{{ route('setlang',['locale'=>'es']) }}" data-description="(ES)" data-imagesrc ="{{ Storage::url('public/images/flag-e.png') }}"></option>
                        <option @if(Config::get('app.locale') == 'ru') selected @endif data-link="{{ route('setlang',['locale'=>'ru']) }}" data-description="(RU)" data-imagesrc ="{{ Storage::url('public/images/flag-ru.png') }}"></option>
                    </select>
                </div>
            </div>

        </div>
    </div>
</div>
