<div class="row">
    <div class="col-md-8">
        <select class="" name="sort">
            <option @if(isset($filter['sort']) && $filter['sort'] == 'price.desc') selected @endif value="price.desc">{{__('general.sort_price_desc')}}</option>
            <option @if(isset($filter['sort']) && $filter['sort'] == 'price.asc') selected @endif value="price.asc">{{__('general.sort_price_asc')}}</option>
         </select>
    </div>

    <div class="catalog_page-content__products-config_panel col-md-4 justify-content-end d-none d-sm-none d-md-flex d-lg-flex d-xl-flex">

        @if(session()->has('product_view_type'))
            @if(session('product_view_type') == 'grid')
                <button class="active" data-change-product-view="{{ session('product_view_type') }}"></button>
                <button data-change-product-view="list"></button>
            @else
                <button data-change-product-view="grid"></button>
                <button class="active" data-change-product-view="{{ session('product_view_type') }}"></button>
            @endif
        @else
            <button class="active" data-change-product-view="grid"></button>
            <button data-change-product-view="list"></button>
        @endif
    </div>
</div>

<div class="row catalog_page-content__products-products" data-view="@if(session()->has('product_view_type')){{session('product_view_type')}}@else grid @endif">
@if(count($products) > 0)
        @foreach($products as $realEstate)
            <div class="col-md-4">
                <div class="home_catalog-products__item">
                    <div class="home_catalog-products__item-content">
                        <div class="home_catalog-products__item-content__background">
                            <div class="home_catalog-products__item-content__background-thumb">
                                <a href="{{ route('realestate', ['name' => $realEstate->seo_url]) }}">
                                    <img src="{{$realEstate->thumb}}" />
                                </a>
                                <div class="home_catalog-products__item-content__background-thumb__price d-flex align-items-center justify-content-between">
                                       <span class="text-white font-weight-bold">
                                           @if($realEstate->price_upon_request)
                                               {!! __('general.price_upon_request') !!}
                                           @else
                                               {!! $realEstate->price_format !!}
                                           @endif
                                       </span>
                                    <span><img src="{{ Storage::url('public/images/arrow-right.png') }}"  /></span>
                                </div>
                            </div>
                        </div>
                        <div class="home_catalog-products__item-content__info">
                            <div class="home_catalog-products__item-content__info-address">
                                <span>{{$realEstate->title}}</span>
                                <p>{{$realEstate->address}}</p>
                            </div>
                            <div class="home_catalog-products__item-content__info-attributes row">
                                @foreach($realEstate->numberAttributes as $numberAttribute)
                                    <div class="home_catalog-products__item-content__info-attributes__item col-sm-6">
                                        <img src="{{ $numberAttribute->path }}" />
                                        <span class="font-weight-bold">{{ $numberAttribute->pivot->value }} {{ $numberAttribute->name }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
    <div class="row">
        <div class="col-12">
            <p class="ml-1 ml-md-4 mt-md-3 font-weight-bold">{{ __('general.this_filter_doesnt_have_results') }}</p>
        </div>
    </div>
@endif

</div>

{{$products->links()}}

