
<div class="catalog_page-content__filter-content">

    <div class="custom-toogle-wrapper">
        <div class="custom-toogle-wrapper-header is-closed">
            {{ __('general.filter-catalog') }}
        </div>
        <div class="custom-toogle-wrapper-content is-closed" style="display: none;" >
            <form  id="filter_form" action="/catalog" method="get">
                <input type="hidden" name="advanced_search" value="1" />
                <input type="hidden" name="sort" value="@isset($filter['sort']){{$filter['sort']}}@endisset" />
                <div class="form-group">
                    <input class="form-control" type="text" value="@isset($filter['title']){{$filter['title']}}@endisset" name="title" placeholder="Search..." id="filter_title" />
                </div>

                <div class="form-group">
                    <select class="form-control" id="catalog-filter-type" name="real_estate_type_id">
                        <option value="0" selected="selected">{{__('general.types')}}</option>
                        @foreach($types as $key => $name)
                            <option value="{{$key}}" @if(isset($filter['real_estate_type_id']) && $filter['real_estate_type_id'] == $key) selected @endif>{{$name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <input type="hidden" id="countryId" value="@isset($filter['country']){{$filter['country']}}@endisset" />

                    <select name="country" class="form-control" id="catalog-filter-country" >
                        <option selected disabled value="0" selected="selected">{{__('general.country')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}" @if(isset($filter['country']) && $filter['country'] == $country->id) selected @endif>{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="hidden" id="cityId" value="@isset($filter['city']){{$filter['city']}}@endisset" />

                    <select class="form-control" id="catalog-filter-city" name="city">
                        <option value="0" selected="selected">{{__('general.city')}}</option>
                    </select>
                </div>
                <div class="form-group catalog_page-content__filter-content__checkboxes custom-checkbox-categories">
                    @if(isset($filter['real_estate_type_id']) && $filter['real_estate_type_id'] != 0)
                        @if(isset($filter['category']))
                            @php
                                $categories = \App\Models\Category::whereHas('types',function ($query) use($filter) {
                                $query->where('type_id',$filter['real_estate_type_id']);
                            })
                                ->get()
                                ->sortBy('name');
                            @endphp

                            @foreach($categories as $category)
                                <label class="custom_checkbox">
                                    <input type="checkbox" name="category[]" value="{{$category->id}}" @if(in_array($category->id, $filter['category'])) checked @endif>
                                    <span class="checkmark"></span>{{$category->name}}
                                </label>
                            @endforeach
                        @else
                            @php
                                $categories = \App\Models\Category::whereHas('types',function ($query) use($filter) {
                                 $query->where('type_id',$filter['real_estate_type_id']);
                             })
                                 ->get()
                                 ->sortBy('name');
                            @endphp

                            @foreach($categories as $category)
                                <label class="custom_checkbox">
                                    <input type="checkbox" name="category[]" value="{{$category->id}}">
                                    <span class="checkmark"></span>{{$category->name}}
                                </label>
                            @endforeach
                        @endif
                    @else
                        @if(isset($filter['category']))
                            @foreach($categories as $category)
                                <label class="custom_checkbox">
                                    <input type="checkbox" name="category[]" value="{{$category->id}}" @if(in_array($category->id, $filter['category'])) checked @endif>
                                    <span class="checkmark"></span>{{$category->name}}
                                </label>
                            @endforeach
                        @else
                            @foreach($categories as $category)
                                <label class="custom_checkbox">
                                    <input type="checkbox" name="category[]" value="{{$category->id}}">
                                    <span class="checkmark"></span>{{$category->name}}
                                </label>
                            @endforeach
                        @endif
                    @endif
                </div>

                <div class="form-group catalog_page-content__filter-content__slider-item d-none">
                    <input disabled type="hidden" class="min_value" value="@if(isset($filter['price']['min'])) {{$filter['price']['min']}} @endif" name="price[min]" />
                    <input disabled  type="hidden" class="max_value" value="@if(isset($filter['price']['max'])) {{$filter['price']['max']}} @endif" name="price[max]" />
                    <div class="form-group catalog_page-content__filter-content__slider-item-text">
                        <p>
                            {{ __('general.filter_price_range') }}
                        </p>
                        <p>
                            <span class="home_catalog-filter__advanced-slider__min"></span> - <span class="home_catalog-filter__advanced-slider__max"></span>
                        </p>
                    </div>
                    <div data-min="{{$min_price}}" data-max="{{$max_price}}" data-selected-min="@if(isset($filter['price']['min'])) {{$filter['price']['min']}} @else{{$min_price}} @endif" data-selected-max="@if(isset($filter['price']['max'])) {{$filter['price']['max']}} @else {{$max_price}} @endif" class="filter-slider" id="slider-dadawd"></div>
                </div>
                <br />
                <div class="catalog_page-content__filter-content-attributes">

                    <div class="catalog_page-content__filter-content-attributes__header is-closed">
                        {{__('general.attributes')}}
                    </div>
                    <div class="catalog_page-content__filter-content-attributes__content" style="display: none;">


                        @foreach($attributes['number'] as $number)
                            <div class="form-group">
                                <label>{{$number->name}}</label>
                                <select title="{{$number->name}}" class="form-control" name="attributes[{{$number->id}}]" id="number_attribute-{{$number->id}}">
                                    <option value="">{{ __('general.filter_all_select') }}</option>
                                    @for($i = $number->val_min; $i <= $number->val_max; $i++)
                                        <option @if(isset($filter['attributes'][$number->id]) && $filter['attributes'][$number->id] == $i ) selected @endif value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        @endforeach

                        <div class="form-group catalog_page-content__filter-content__slider">
                            @foreach($attributes['slider'] as $slider)
                                <div class="form-group catalog_page-content__filter-content__slider-item">
                                    <input type="hidden" class="min_value" value="@if(isset($filter['attributes'][$slider->id]['min'])) {{(int)$filter['attributes'][$slider->id]['min']}} @endif" name="attributes[{{$slider->id}}][min]" />
                                    <input type="hidden" class="max_value" value="@if(isset($filter['attributes'][$slider->id]['max'])) {{(int)$filter['attributes'][$slider->id]['max']}} @endif" name="attributes[{{$slider->id}}][max]" />
                                    <div class="form-group catalog_page-content__filter-content__slider-item-text">
                                        <p>
                                            {{$slider->name}}
                                        </p>
                                        <p>
                                            <span class="home_catalog-filter__advanced-slider__min"></span> - <span class="home_catalog-filter__advanced-slider__max"></span>
                                        </p>
                                    </div>
                                    <div data-min="{{$slider->val_min}}" data-max="{{$slider->val_max}}" data-selected-min="@if(isset($filter['attributes'][$slider->id]['min'])) {{(int)$filter['attributes'][$slider->id]['min']}} @else {{$slider->val_min}} @endif" data-selected-max="@if(isset($filter['attributes'][$slider->id]['max'])) {{(int)$filter['attributes'][$slider->id]['max']}} @else {{$slider->val_max}} @endif" class="filter-slider" id="slider-{{$slider->id}}"></div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group catalog_page-content__filter-content__checkboxes">
                            @foreach($attributes['checkboxes'] as $checkbox)
                                <label class="custom_checkbox">
                                    <input type="checkbox" name="attributes[{{$checkbox->id}}]" @if(isset($filter['attributes'][$checkbox->id])) checked @endif>
                                    <span class="checkmark"></span>{{$checkbox->name}}
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>

                <button class="btn btn-default text-black" >{{__('general.search')}}</button>

            </form>
        </div>
    </div>
</div>
