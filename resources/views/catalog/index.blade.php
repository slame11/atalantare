@extends('layouts-na.index')

@section('title')
@stop

@section('content')
    <div class="catalog_page">
        <div class="catalog_page-filter">
            <form  id="filter_form" action="/catalog" method="get">
                <input type="hidden" name="sort" value="@isset($filter['sort']){{$filter['sort']}}@endisset" />
                <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-7">
                        <div class="catalog_page-filter__search">
                            <input type="text" name="title" value="@isset($filter['title']){{$filter['title']}}@endisset" placeholder="Search..." />
                        </div>
                    </div>
                    <div class="d-lg-none col-5  text-center">
                        <button type="button" class="open_mobile_filter">Open filter <i class="fas fa-filter"></i></button>
                    </div>
                    <div style="display: none" class="col-lg-9 col-md-12 d-lg-block catalog_page-filter_container">
                        <div class="catalog_page-filter__menu">
                            <div class="catalog_page-filter__menu-item">
                                <input type="hidden"  name="real_estate_type_id" value="@if(isset($filter['real_estate_type_id'])) {{ $filter['real_estate_type_id']}} @endif" />
                                <a class="catalog_page-filter__menu-item__toggler down" href="#" role="button">
                                    @if(isset($filter['real_estate_type_id']) && $filter['real_estate_type_id'] > 0)
                                        @foreach($types as $key => $name)
                                            @if(isset($filter['real_estate_type_id']) && $filter['real_estate_type_id'] == $key)
                                                {{$name}}
                                            @endif
                                        @endforeach
                                        @else
                                        {{__('general.all_types')}}
                                    @endif
                                </a>
                                <div class="catalog_page-filter__menu-item__dropdown">
                                    @foreach($types as $key => $name)
                                        <a data-value="{{$key}}" class="catalog_page-filter__menu-item__dropdown-item @if(isset($filter['real_estate_type_id']) && $filter['real_estate_type_id'] == $key) active @endif" href="#">{{$name}}</a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="catalog_page-filter__menu-item">
                                <input type="hidden"  name="country" value="@if(isset($filter['country'])) {{ $filter['country']}} @endif" />

                                <a class="catalog_page-filter__menu-item__toggler down" href="#" role="button">
                                    @if(isset($filter['country']) && $filter['country'] > 0)
                                        @foreach($countries as $country)
                                            @if((isset($filter['country']) && $filter['country'] == $country->id) || ($country_cat == $country))
                                                {{$country->name}}
                                            @endif
                                        @endforeach
                                    @else
                                        {{__('general.country')}}
                                    @endif
                                </a>
                                <div class="catalog_page-filter__menu-item__dropdown catalog_page-filter__menu-item__dropdown-country">
                                    @foreach($countries as $country)
                                        <a data-currency="{{\App\Models\RealEstate::$currencySign[$country->currency]}}" data-seo-url="{{$country->seo_url}}" data-value="{{$country->id}}" class="country_link catalog_page-filter__menu-item__dropdown-item @if((isset($filter['country']) && $filter['country'] == $country->id) || ($country_cat == $country)) active @endif" href="{{route('catalog-country',$country->seo_url)}}">{{$country->name}}</a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="catalog_page-filter__menu-item">
                                <input type="hidden"  name="city" value="@if(isset($filter['city'])) {{ $filter['city']}} @endif" />

                                <a class="catalog_page-filter__menu-item__toggler down" href="#" role="button">
                                    @if(isset($filter['city']) && (int)$filter['city'] > 0)
                                        @foreach($cities as $city)
                                            @if((isset($filter['city']) && $filter['city'] == $city->id))
                                                {{$city->name}}
                                            @endif
                                        @endforeach
                                    @else
                                        {{__('general.city')}}
                                    @endif
                                </a>
                                <div class="catalog_page-filter__menu-item__dropdown">
                                    @if($cities)
                                        @foreach($cities as $city)
                                            <a data-value="{{$city->id}}" href="#" class="country_link catalog_page-filter__menu-item__dropdown-item @if((isset($filter['city']) && $filter['city'] == $city->id)) active @endif">{{$city->name}}</a>
                                        @endforeach
                                        @else
                                        <a data-value="0" data-disabled="1" href="#" class="country_link catalog_page-filter__menu-item__dropdown-item">You need to choose country</a>
                                    @endif
                                </div>
                            </div>
                            <div class="catalog_page-filter__menu-item">

                                <a class="catalog_page-filter__menu-item__toggler down" href="#" role="button">
                                    {{__('general.types')}}
                                </a>
                                <div class="catalog_page-filter__menu-item__dropdown catalog_page-filter__menu-item__dropdown-type" style="width: 320px;padding: 10px 16px;">
                                    @if(isset($filter['real_estate_type_id']) && $filter['real_estate_type_id'] != 0)
                                        @if(isset($filter['category']))
                                            @php
                                                $categories = \App\Models\Category::whereHas('types',function ($query) use($filter) {
                                                $query->where('type_id',$filter['real_estate_type_id']);
                                            })
                                                ->get()
                                                ->sortBy('name');
                                            @endphp

                                            @foreach($categories as $category)
                                                <label class="custom_checkbox">
                                                    <input type="checkbox" name="category[]" value="{{$category->id}}" @if(in_array($category->id, $filter['category'])) checked @endif>
                                                    <span class="checkmark"></span>{{$category->name}}
                                                </label>
                                            @endforeach
                                        @else
                                            @php
                                                $categories = \App\Models\Category::whereHas('types',function ($query) use($filter) {
                                                 $query->where('type_id',$filter['real_estate_type_id']);
                                             })
                                                 ->get()
                                                 ->sortBy('name');
                                            @endphp

                                            @foreach($categories as $category)
                                                <label class="custom_checkbox">
                                                    <input type="checkbox" name="category[]" value="{{$category->id}}">
                                                    <span class="checkmark"></span>{{$category->name}}
                                                </label>
                                            @endforeach
                                        @endif
                                    @else
                                        @if(isset($filter['category']))
                                            @foreach($categories as $category)
                                                <label class="custom_checkbox">
                                                    <input type="checkbox" name="category[]" value="{{$category->id}}" @if(in_array($category->id, $filter['category'])) checked @endif>
                                                    <span class="checkmark"></span>{{$category->name}}
                                                </label>
                                            @endforeach
                                        @else
                                            @foreach($categories as $category)
                                                <label class="custom_checkbox">
                                                    <input type="checkbox" name="category[]" value="{{$category->id}}">
                                                    <span class="checkmark"></span>{{$category->name}}
                                                </label>
                                            @endforeach
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="catalog_page-filter__menu-item">

                                <a class="catalog_page-filter__menu-item__toggler down" href="#" role="button">
                                    {{ __('general.filter_price_range') }}
                                </a>
                                <div class="catalog_page-filter__menu-item__dropdown" style="width: 250px;padding: 10px 16px;">
                                    <div class="form-group catalog_page-content__filter-content__slider-item">
                                        <input type="hidden" class="min_value" value="@if(isset($filter['price']['min'])) {{$filter['price']['min']}} @endif" name="price[min]" />
                                        <input type="hidden" class="max_value" value="@if(isset($filter['price']['max'])) {{$filter['price']['max']}} @endif" name="price[max]" />
                                        <div class="form-group catalog_page-content__filter-content__slider-item-text">
                                            <p>
                                                {{ __('general.filter_price_range') }}
                                            </p>

                                        </div>
                                        <div data-min="{{$min_price}}" data-max="{{$max_price}}" data-selected-min="@if(isset($filter['price']['min'])) {{$filter['price']['min']}} @else{{$min_price}} @endif" data-selected-max="@if(isset($filter['price']['max'])) {{$filter['price']['max']}} @else {{$max_price}} @endif" class="filter-slider" id="slider-price"></div>
                                        <p>
                                            <span class="home_catalog-filter__advanced-slider__min"></span> - <span class="home_catalog-filter__advanced-slider__max"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog_page-filter__menu-item">

                                <a class="catalog_page-filter__menu-item__toggler down" href="#" role="button">
                                    {{__('general.more')}}
                                </a>
                                <div class="catalog_page-filter__menu-item__dropdown catalog_page-filter__menu-item__dropdown-attributes" style="width: 320px;padding: 10px 16px;">

                                    <div class="catalog_page-filter__menu-item__dropdown-attributes__checkboxes">
                                        @foreach($attributes['checkboxes'] as $checkbox)
                                            <label class="custom_checkbox">
                                                <input type="checkbox" name="attributes[{{$checkbox->id}}]" @if(isset($homeattributes['checkboxes'][$checkbox->id]) && $homeattributes['checkboxes'][$checkbox->id] == 1) checked @endif />
                                                <span class="checkmark"></span>{{$checkbox->name}}
                                            </label>
                                        @endforeach
                                    </div>
                                    <div class="catalog_page-filter__menu-item__dropdown-attributes__sliders">
                                    @foreach($attributes['slider'] as $slider)
                                        <div class="form-group catalog_page-content__filter-content__slider-item">
                                            <input type="hidden" class="min_value" value="@if(isset($filter['attributes'][$slider->id]['min'])) {{(int)$filter['attributes'][$slider->id]['min']}} @endif" name="attributes[{{$slider->id}}][min]" />
                                            <input type="hidden" class="max_value" value="@if(isset($filter['attributes'][$slider->id]['max'])) {{(int)$filter['attributes'][$slider->id]['max']}} @endif" name="attributes[{{$slider->id}}][max]" />
                                            <div class="form-group catalog_page-content__filter-content__slider-item-text">
                                                <p>
                                                    {{$slider->name}}
                                                </p>

                                            </div>
                                            <div data-min="{{$slider->val_min}}" data-max="{{$slider->val_max}}" data-selected-min="@if(isset($filter['attributes'][$slider->id]['min'])) {{(int)$filter['attributes'][$slider->id]['min']}} @else {{$slider->val_min}} @endif" data-selected-max="@if(isset($filter['attributes'][$slider->id]['max'])) {{(int)$filter['attributes'][$slider->id]['max']}} @else {{$slider->val_max}} @endif" class="filter-slider" id="slider-{{$slider->id}}"></div>
                                            <p>
                                                <span class="home_catalog-filter__advanced-slider__min"></span> - <span class="home_catalog-filter__advanced-slider__max"></span>
                                            </p>
                                        </div>
                                    @endforeach
                                    </div>
{{--
                                        <div class="catalog_page-filter__menu-item__dropdown-attributes__number">
                                            @foreach($attributes['number'] as $number)
                                                <div class="home_catalog-filter__countinput">
                                                    <label for="beds_filter" class="d-flex align-items-center mb-0 justify-content-center"><span>{{$number->name}}:</span><button type="button" class="btn btn-simple btn_minus"><i class="fas fa-minus"></i></button><input class="form-control" name="attributes[{{$number->id}}]" type="text" value="@if(isset($homeattributes['number'][$number->id])){{$homeattributes['number'][$number->id]}}@else 1 @endif" id="attribute_number-{{$number->id}}" /> <button type="button" class="btn btn-simple background-gray btn_plus"><i class="fas fa-plus  text-white"></i></button></label>
                                                </div>
                                            @endforeach
                                        </div>
                                      --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="{{ route('home') }}">{{ __('general.home_bread') }}</a></li>
                    <li>{{ __('general.catalog_bread') }}</li>
                </ul>
            </div>
        </div>
        <div class="catalog_page-content">
            <div class="container">

                <div class="row">
                  {{--  <div class="col-md-12 col-lg-3 catalog_page-content__filter">
                        @include('catalog.partials.filter')
                    </div>--}}
                    <div class="catalog_page-content__loader_wrapper ">
                        <div class="sk-folding-cube">
                            <div class="sk-cube1 sk-cube"></div>
                            <div class="sk-cube2 sk-cube"></div>
                            <div class="sk-cube4 sk-cube"></div>
                            <div class="sk-cube3 sk-cube"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 catalog_page-content__products">
                    {!! $products !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
