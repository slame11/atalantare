@extends('layouts-na.index')
@section('title'){{$realEstate->title}}@stop
@section('meta-section')
    <meta name="description" content="{{$realEstate->media_description}}">
    <meta name="keywords" content="{{$realEstate->keyword}}">
@stop
@section('content')

<div class="breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="{{ route('home') }}">{{ __('general.home_bread') }}</a></li>
            <li><a href="{{ route('catalog') }}">{{ __('general.catalog_bread') }}</a></li>
            <li>{{$realEstate->title}}</li>
        </ul>
    </div>
</div>

<div class="product_card">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="product_card_header">
                    <div class="product_card_header_info">
                        @if ( isset ($realEstate->category->name))
                            <div class="product_card_header_info_category">{{$realEstate->category->name}}</div>
                        @endif
                        <h1 class="main_header product_card_header_info_header">{{$realEstate->title}}</h1>
                        <div class="product_card_header_info_address">{{$realEstate->address}}</div>
                    </div>
                    <div class="product_card_header_price">
                        <div class="product_card_header_price_wrapper">
                            {{--   <div class="float-lg-right position-relative float-none">
                                   <select id="currency_select">
                                       <option @if($realEstate->currency =='EUR') selected @endif data-description="€" data-price="{{$prices['EUR']}}" data-imagesrc =""></option>
                                       <option @if($realEstate->currency =='USD') selected @endif data-description="$" data-price="{{$prices['USD']}}" data-imagesrc =""></option>
                                       <option @if($realEstate->currency =='CNY') selected @endif data-description="¥" data-price="{{$prices['CNY']}}" data-imagesrc =""></option>
                                       <option @if($realEstate->currency =='RUB') selected @endif data-description="₽" data-price="{{$prices['RUB']}}" data-imagesrc =""></option>
                                       <option @if($realEstate->currency =='GBP') selected @endif data-description="£" data-price="{{$prices['GBP']}}" data-imagesrc =""></option>
                                   </select>
                               </div>
                               <div class="product_card_header_price_wrapper-price d-flex flex-column"><span>@php echo number_format($realEstate->price ,0,',',',') @endphp </span> @if($realEstate->real_estate_type_id == 2) <span>{{__('general.'.\App\Models\RealEstate::$rentPeriods[$realEstate->rent_period]['type_name'])}}</span>@endif</div>
   --}}<div class="product_card_header_price_wrapper-price d-flex flex-column"><span>
                                           @if($realEstate->price_upon_request)
                                        {!! __('general.price_upon_request') !!}
                                    @else
                                        {!! $realEstate->price_format !!}
                                    @endif
                                </span> </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="realEstateId" value="{{ $realEstate->id }}">
                <input type="hidden" id="realEstateViewed" value="{{ $realEstate->viewed }}">
            </div>
            <div class="col-12">
                <div class="product_card_slider_wrapper">
                    <div class="fotorama"
                         data-nav="thumbs"
                         data-thumbheight="100"
                         data-thumbwidth="170"
                         data-thumbmargin="10"
                         data-arrows="true"
                         data-click="true"
                         data-swipe="true"
                         data-minwidth="100%"
                         data-maxheight="900"
                         data-allowfullscreen="true"
                    >
                        @foreach($realEstateImages as $image)
                            <img src="{{$image->thumb}}">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <div class="product_card_conditions">
                            <div class="product_card_conditions_title">
                                <p class="product_subtitle">{{ __('general.product_page_condition') }}</p>
                            </div>
                            <div class="product_card_conditions_content">
                                <div class="row">

                                    @foreach($numberAttributes as $numberAttribute)
                                        <div class="col-6">
                                            <div class="product_card_conditions_content_item">
                                                <div class="product_card_conditions_content_item_wrapper">
                                                    <div class="product_card_conditions_content_item_wrapper_image">
                                                        <img src="{{ $numberAttribute->path }}" alt="">
                                                    </div>
                                                    <div class="product_card_conditions_content_item_wrapper_name">
                                                        <span>{{ $numberAttribute->pivot->value }} {{ $numberAttribute->name }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <div class="product_card_amenities">
                            <div class="product_card_amenities_title">
                                <p class="product_subtitle">{{ __('general.product_page_amenities') }}</p>
                            </div>
                            <div class="product_card_amenities_content">
                                <div class="row px-1 px-sm-0">

                                    @foreach($checkboxAttributes as $checkboxAttribute)
                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 px-0 px-sm-3">
                                            <div class="product_card_amenities_content_item">
                                                <div class="product_card_amenities_content_item_wrapper">
                                                    <div class="product_card_amenities_content_item_wrapper_image">
                                                        <img src="{{ Storage::url('public/images/product_card/icons/icon_checked.png') }}" alt="">
                                                    </div>
                                                    <div class="product_card_amenities_content_item_wrapper_name">
                                                        <span>{{ $checkboxAttribute->name }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($realEstate->description)
                <div class="row">
                    <div class="col-12">
                        <div class="product_card_description">
                            <p class="product_card_description_title">{{ __('general.product_page_desc') }}</p>
                            <p class="product_card_description_text">
                                {!! $realEstate->description !!}
                            </p>
                        </div>
                    </div>
                </div>
                @endif

                <div class="row">
                    <div class="col-12">

                        <div class="product_cart_coords" data-coords="{{ $realEstate->map_point }}"></div>
                        <div class="product_cart_title" data-title="{{ $realEstate->title }}"></div>
                        <div class="product_cart_address" data-address="{{$realEstate->address}}"></div>

                        <div class="product_card_map">
                            <div id="product_card_map"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @include('contact.partial.form')
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
                <div class="row">
                    <div class="col-12">
                        <div class="product_card_asside_agents">
                            <div class="product_card_asside_agents_title">
                                <p class="product_subtitle __line">{{ __('general.product_page_agents') }}</p>
                            </div>
                            <div class="row">

                                @foreach($agents as $agent)
                                    <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-6">
                                        <div class="product_card_asside_agents_item">
                                            <div class="product_card_asside_agents_item_image">
                                                @php
                                                    $agent->thumb = \App\Models\ImageTool::resize($agent->photo, 160,150);
                                                @endphp
                                                <img src="{{ $agent->thumb }}" alt="">
                                            </div>
                                            <div class="product_card_asside_agents_item_name">
                                                <p>{{ $agent->name }}</p>
                                            </div>
                                            <div class="product_card_asside_agents_item_desc">
                                                <p>{{ $agent->organization }}</p>
                                            </div>
                                            <div class="product_card_asside_agents_item_phones">
                                                <ul>
                                                    <li class="main_phone">
                                                        Main: <a href="tel:{{ $agent->phone }}">{{ $agent->phone }}</a>
                                                    </li>
                                                    <li class="fax">
                                                        <span>Phone: {{ $agent->fax }}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="product_card_asside_agents_item_more">
                                              {{--  <span>More</span>--}}
                                            </div>
                                            <button type="button" data-toggle="modal" data-target="#contact_form_popup" class="btn btn-sm btn-default text-black agent_popup_button" data-agent-name="{{ $agent->name }}">{{__('general.contact_me')}}</button>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="product_card_asside_houses">
                            <div class="product_card_asside_houses_title">
                                <p class="product_subtitle __line">{{ __('general.product_page_popular') }}</p>
                            </div>
                            <div class="row">
                                @for($i = 1; $i < 5; $i++)
                                    @php
                                    $popular = $popular_real_states[rand(0,count($popular_real_states) - 1)];

                                    if(isset($popular['image'][0])){
                                     $thumb = \App\Models\ImageTool::resize($popular['image'][0]['path'],0,0);
                                    }else{
                                    $thumb = '/storage/images/no-image-box.png';
                                    }

                                    @endphp
                                    <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-6">
                                        <a href="#" class="product_card_asside_houses_item">
                                            <div class="product_card_asside_houses_item_image">
                                                <a href="{{ route('realestate', ['name' => $popular['seo_url']]) }}"><img src="{{ $thumb  }}" alt=""></a>
                                            </div>
                                            <div class="product_card_asside_houses_item_title">
                                                <p>{{ $popular['address'] }}<br/>
                                                    {{ $popular['city']['name'] }}, {{ $popular['country']['name'] }}
                                                </p>
                                            </div>
                                            <div class="product_card_asside_houses_item_price">
                                                <span>{!! $popular['price_format'] !!}</span>
                                            </div>
                                        </a>
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>

               {{-- <div class="row">
                    <div class="col-12">
                        <div class="product_card_asside_tags">
                            <div class="product_card_asside_tags_title">
                                <p class="product_subtitle __line">Tags</p>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="product_card_asside_tags_items">
                                        <ul>
                                            <li>
                                                <button type="button">Appartment</button>
                                            </li>
                                            <li>
                                                <button type="button">Home</button>
                                            </li>
                                            <li>
                                                <button type="button">Test tag</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>
</div>

@endsection
