@extends('layouts-na.index')

@section('title')
    {{$page->meta_title}}
@stop

@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>{{$page->title}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="page-title main_header text-center">
            <h1>{{$page->title}}</h1>
            <span>{{$page->title}}</span>
        </div>
        <div class="page-content">
            {!! $page->description !!}
        </div>
    </div>
@endsection
