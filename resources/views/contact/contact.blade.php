<div class="contacts_block @if($extend == 1) contacts_page @endif">
    <div class="contacts_block-header_block">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="contacts_block-header_block__main_header main_header">
                        @isset($home_locales[Config('app.locale')]['contact_form']['title']) {{$home_locales[Config('app.locale')]['contact_form']['title']}} @else{{ __('general.contacts') }} @endisset

                        <span>@isset($home_locales[Config('app.locale')]['contact_form']['title']) {{$home_locales[Config('app.locale')]['contact_form']['title']}} @else{{ __('general.contacts') }} @endisset</span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    @if($extend == 1)
        <div class="contacts_block__general">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="contacts_block__general-info">

                            <div class="hidden contacts-block-coords-latitude" data-contacts-block-coords="{{ $latitude->value }}"></div>
                            <div class="hidden contacts-block-coords-longitude" data-contacts-block-coords="{{ $longitude->value }}"></div>
                            <div class="hidden contacts-block-address" data-contacts-block-address="{{ $address->value }}"></div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                <div class="contacts_block__general-info-item address">
                                    <div class="contacts_block__general-info-item-image">
                                        <img src="{{ Storage::url('public/images/address_contacts.png') }}" alt="">
                                    </div>
                                    <div class="contacts_block__general-info-item-content">
                                        <p>{{ $address->value }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                <div class="contacts_block__general-info-item phones">
                                    <div class="contacts_block__general-info-item-image">
                                        <img src="{{ Storage::url('public/images/call_contacts.png') }}" alt="">
                                    </div>
                                    <div class="contacts_block__general-info-item-content">
                                        <p>Telephone : <a href="tel:{{ $phone1->value }}">{{ $phone1->value }}</a></p>
                                        <p>Telephone : <a href="tel:{{ $phone2->value }}">{{ $phone2->value }}</a></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                <div class="contacts_block__general-info-item email">
                                    <div class="contacts_block__general-info-item-image">
                                        <img src="{{ Storage::url('public/images/world_contacts.png') }}" alt="">
                                    </div>
                                    <div class="contacts_block__general-info-item-content">
                                        <p>Email : <a href="mailto:{{ $email->value }}">{{ $email->value }}</a></p>
                                        <p>Web : <a href="{{ $site->value }}">{{ $site->value }}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="contacts_block-content_block">
        <div class="container">
            <div class="contacts_block-content_block__contacts_wrapper">
                <div class="row">

                    <div class="col-lg-6 col-md-12">
                        @include('contact.partial.form')
                    </div>

                    <div class="col-lg-6 col-md-12 p-0" style="position:inherit;">

                        <div class="hidden contacts-block-coords-latitude" data-contacts-block-coords="{{ $latitude->value }}"></div>
                        <div class="hidden contacts-block-coords-longitude" data-contacts-block-coords="{{ $longitude->value }}"></div>
                        <div class="hidden contacts-block-address" data-contacts-block-address="{{ $address->value }}"></div>

                        <div id="contact_map" class="contacts_block-content_block__contacts_wrapper-map_block">
                            <div id="contact_block_map"></div>
                            {{--<iframe--}}
                                    {{--id="gmap_canvas"--}}
                                    {{--src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed"--}}
                                    {{--frameborder="0"--}}
                                    {{--scrolling="no"--}}
                                    {{--marginheight="0"--}}
                                    {{--marginwidth="0"--}}
                            {{--></iframe>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
