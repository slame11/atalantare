@extends('layouts-na.index')

@section('meta-section')
    @if($extend == 1)
        <meta name="description" content="{{ $page->meta_description }}">
        <meta name="keywords" content="{{ $page->meta_keyword }}">
    @endif
@endsection

@section('content')
    <!-- Breadcrumbs block START -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>Contacts</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs block END -->

    @include('contact.contact')
@endsection