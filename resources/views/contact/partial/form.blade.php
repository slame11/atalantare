<div class="contacts_block-content_block__contacts_wrapper-contact_header">
    <p> @isset($home_locales[Config('app.locale')]['contact_form']['subtitle']) {{$home_locales[Config('app.locale')]['contact_form']['subtitle']}} @else{{ __('general.feedback') }}@endisset</p>
</div>
<div class="contacts_block-content_block__contacts_wrapper-contact_form">
    <input type="hidden" value="" name="agent" />
    <div class="form-group">
        <label for="name" class="require">{{__('general.user')}}</label>
        <input type="text" class="form-control" id="name">
    </div>
    <div class="form-group">
        <label for="phone">{{__('general.phone')}}</label>
        <input type="text" class="form-control" id="phone">
    </div>
    <div class="form-group">
        <label for="email" class="require">{{__('general.email')}}</label>
        <input type="email" class="form-control" id="email">
    </div>
    <div class="form-group">
        <label for="message">{{__('general.message')}}</label>
        <textarea rows="8" class="form-control" id="message"></textarea>
    </div>
    <button type="submit"  class="btn btn-default text-black send_contact_form">{{__('general.send')}}</button>
</div>
