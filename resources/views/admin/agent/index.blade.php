@extends('admin.layouts.admin-lte')

@section('title', __('general.agents'))
@section('content-title', __('general.agents'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-users"></i>{{ __('general.agents') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.agents_list') }}</h3>
                    <div class="pull-right">
                        <a href="{{ route('agents.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.photo') }}</th>
                            <th>{{ __('general.full_name') }}</th>
                            <th>{{ __('general.phone') }}</th>
                            <th>{{ __('general.email') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($agents as $agent)
                            @php
                                $agent['thumb'] = \App\Models\ImageTool::resize($agent->photo,100,100);
                            @endphp
                            <tr>
                                <td><img src="{{ $agent->photo != '' ? $agent->thumb : Storage::url('public/images/no-image-box.png') }}" /></td>
                                <td>{{ $agent->name }}</td>
                                <td>{{ $agent->phone }}</td>
                                <td>{{ $agent->email }}</td>
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('agents.edit', $agent) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('agents.destroy', $agent) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$agents->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection


