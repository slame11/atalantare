<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('show_on_main') ? 'has-error' : ''}}">
            {!! Form::label('show_on_main', __('general.show_on_main')) !!}
            {!! Form::checkbox('show_on_main',1, $agent->show_on_main) !!}
            {!! $errors->first('show_on_main', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('name', __('general.full_name')) !!}
            {!! Form::text('name',$agent->name,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            {!! Form::label('email', __('general.email')) !!}
            {!! Form::text('email',$agent->email,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
            {!! Form::label('address', __('general.address')) !!}
            {!! Form::text('address',$agent->address,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
            {!! Form::label('phone', __('general.phone')) !!}
            {!! Form::text('phone',$agent->phone,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group php artisan migrate:refresh --seed">
            {!! Form::label('fax', __('general.phone').' 2') !!}
            {!! Form::text('fax',$agent->fax,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('fax', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('organization') ? 'has-error' : ''}}">
            {!! Form::label('organization', __('general.organization')) !!}
            {!! Form::text('organization',$agent->organization,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('organization', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
            {!! Form::label('photo', __('general.photo')) !!}
            <a href="" id="thumb-image-photo" data-toggle="image" class="img-thumbnail">
                <img src="{{ $agent->photo != '' ? $agent->thumb : Storage::url('public/images/no-image-box.png') }}" alt="" title="" data-placeholder="test" />
            </a>

            <input type="hidden" name="photo" value="{{$agent->photo}}" id="input-image-photo" />
        </div>
    </div>
</div>


