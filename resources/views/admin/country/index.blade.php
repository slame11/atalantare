@extends('admin.layouts.admin-lte')

@section('title', __('general.countries'))
@section('content-title', __('general.countries'))

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.countries_list') }}</h3>
                    <div class="pull-right">
                        <a href="{{ route('countries.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.country') }}</th>
                            <th>{{ __('general.country_symbol') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($countries as $country)
                            <tr>
                                <td>{{ $country->name }}</td>
                                <td>{{ $country->symbols }}</td>
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('countries.edit', $country) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('countries.destroy', $country) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$countries->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

