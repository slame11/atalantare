<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('symbols') ? 'has-error' : ''}}">
            {!! Form::label('symbols', __('general.symbols')) !!}
            {!! Form::text('symbols',$country->symbols ?? null,['class' => 'form-control','placeholder' => __('general.symbols'),'size' => 255,]) !!}
            {!! $errors->first('symbols', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('seo_url') ? 'has-error' : ''}}">
            {!! Form::label('seo_url', __('general.seo_url')) !!}
            {!! Form::text('seo_url',$country->seo_url ?? null,['class' => 'form-control','placeholder' => __('general.seo_url'),'size' => 255,'required']) !!}
            {!! $errors->first('seo_url', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('map_point') ? 'has-error' : ''}}">
            {!! Form::label('map_point', __('general.map_point')) !!}
            {!! Form::text('map_point',$country->map_point ?? '0 0',['class' => 'form-control','placeholder' => __('general.map_point'),'size' => 255]) !!}
            {!! $errors->first('map_point', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('currency') ? 'has-error' : ''}}">
            {!! Form::label('currency', __('general.currency')) !!}
            {!! Form::select('currency', config('currency.currencies'), $country->currency ?? 'EUR',['class' => 'form-control']) !!}
            {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

@include('admin.country.partials.translations')
