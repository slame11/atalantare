@extends('admin.layouts.admin-lte')

@section('title', __('general.attributes'))
@section('content-title', __('general.attributes'))

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.attributes_list') }}</h3>
                    <div class="pull-right">
                        <a href="{{ route('attributes.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.name') }}</th>
                            <th>{{ __('general.type') }}</th>
                            <th>{{ __('general.minimal') }}</th>
                            <th>{{ __('general.maximum') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($attributes as $attribute)
                            <tr>
                                <td>{{ $attribute->name }}</td>
                                <td>{{ $attribute::$attributeTypes[$attribute->attribute_type_id]['attribute_name'] }}</td>
                                <td>{{ $attribute->val_min }}</td>
                                <td>{{ $attribute->val_max }}</td>
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('attributes.edit', $attribute) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('attributes.destroy', $attribute) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$attributes->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
