<div class="row">

    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('show_on_main', __('general.show_on_main')) !!}
            {!! Form::checkbox('show_on_main',1,$attribute->show_on_main ?? false) !!}
            {!! $errors->first('show_on_main', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group">
            <a href="" id="attr_path" data-toggle="image" class="img-thumbnail">
                <img src="{{ $icon ?? ''}}" alt="" title="" data-placeholder="test" />
            </a>
            <input type="hidden" name="path" value="{{ $attribute->path }}" id="input-image-attr" />
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('attribute_type_id', __('general.attribute_type')) !!}
            {!! Form::select('attribute_type_id', $attributeTypes, $attribute->attribute_type_id ?? null,['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="need-val">
        <div class="col-sm-4">
            <div class="form-group">
            {!! Form::label('val_min', __('general.value')) !!}
            {!! Form::number('val_min',$attribute->val_min ?? 0,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('val_min', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="need-max">
        <div class="col-sm-4">
            <div class="form-group">
            {!! Form::label('val_max', __('general.maximum')) !!}
            {!! Form::number('val_max',$attribute->val_max ?? 0,['class' => 'form-control', 'size' => 255]) !!}
            {!! $errors->first('val_max', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        </div>
</div>

@include('admin.attribute.partials.translations')

@push('js')
    <script>

        $(document).ready(function () {
            var selected = $('#attribute_type_id option:selected').text().toLowerCase();
            if(selected == 'slider'){
                $('.need-val').show();
                $('.need-max').show();
            }else if(selected == 'number'){
                $('.need-val').show();
                $('.need-max').show();
            }else if(selected == 'checkbox'){
                $('.need-val').hide();
                $('.need-max').hide();
            }
        });

        $('#attribute_type_id').change(function () {
            $('#val_max').val(0);
            $('#val_min').val(0);

            if($(this).find("option:selected").text().toLowerCase() == 'slider'){
                $('.need-val').show();
                $('.need-max').show();
            }else if($(this).find("option:selected").text().toLowerCase() == 'number'){
                $('.need-val').show();
                $('.need-max').show();
            }else if($(this).find("option:selected").text().toLowerCase() == 'checkbox'){
                $('.need-val').hide();
                $('.need-max').hide();
            }
        });
    </script>
@endpush