@extends('admin.layouts.admin-lte')

@section('title', __('general.services'))
@section('content-title', __('general.services'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-star"></i>{{ __('general.services') }}</li>
    </ol>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('general.services') }}</h3>
            <div class="pull-right">
                <a href="{{ route('services.create') }}" class="btn btn-sm btn-success add_new_service">
                    {{ __('general.new') }}
                </a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-hover table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>{{ __('general.name') }}</th>
                    <th>{{ __('general.text') }}</th>
                    <th>{{ __('general.status') }}</th>
                    <th>
                    </th>
                </tr>
                </thead>
                <tbody>
                @forelse($services as $service)
                    <tr>
                        <td>{{ $service->name }}</td>
                        <td>{{ str_limit($service->text, $limit = 120, $end = '...') }}</td>
                        <td>{{ $service->status }}</td>
                        <td class="td-action">
                            <a href="{{ route('services.edit', $service) }}" class="btn btn-success btn-sm pull-left">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('services.destroy', $service) }}" method="post">
                                <input type="hidden" name="_method" value="delete" />
                                {{csrf_field()}}
                                <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td><h2>{{ __('general.empty_data') }}</h2></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <nav aria-label="page navigation">
                <ul class="pagination">
                    {{$services->links()}}
                </ul>
            </nav>
        </div>
    </div>
@endsection

