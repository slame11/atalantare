@extends('admin.layouts.admin-lte')

@section('title', __('general.services'))
@section('content-title', __('general.services'))


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('services.index') }}"><i class="fa fa-star"></i> {{ __('general.services') }}</a></li>
        <li class="active"><i class="fa fa-star"></i> {{ __('general.edit') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.edit') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'service_create_form']) !!}

                        <a href="{{ route('services.index') }}" class="btn btn-default"><i class="fa fa fa-share"></i></a>

                    </div>
                </div>
                {!! Form::model($service, ['route' => ['services.update', $service],'method' => 'put','files' => true, 'id' => 'service_create_form']) !!}
                <div class="box-body">
                    @include('admin.service.partials.form')
                    {!! Form::hidden('modified_by', Auth::id()) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('textarea').ckeditor({
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
        });
    </script>
@endpush