<div class="row">
    <div class="col-sm-6">
        <div class="checkbox {{ $errors->has('status') ? 'has-error' : ''}}">
            <label>
                {!! Form::checkbox('status', 1, $service->status) !!}
                {{ __('general.show_on_main') }}
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
            {!! Form::label('image', __('general.image')) !!}
            @php
                $thumb = \App\Models\ImageTool::resize($service->image, 100, 100);
            @endphp
            <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                <img src="@if($thumb){{$thumb}} @else {{Storage::url('public/images/no-image-box.png')}} @endif" alt="" title="" data-placeholder="test" />
            </a>
            <input type="hidden" name="image" value="{{$service->image}}" id="input-image" />
        </div>

    </div>
</div>

@include('admin.service.partials.translations')


