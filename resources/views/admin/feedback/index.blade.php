@extends('admin.layouts.admin-lte')

@section('title', __('general.feedbacks'))
@section('content-title', __('general.feedbacks'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-feed"></i>{{ __('general.feedbacks') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.feedbacks_list') }}</h3>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.date') }}</th>
                            <th>{{ __('general.user') }}</th>
                            <th>{{ __('general.email') }}</th>
                            <th>{{ __('general.phone') }}</th>
                            <th>{{ __('general.status') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($feedbacks as $feedback)
                            <tr>
                                <td>{{ $feedback->created_at }}</td>
                                <td>{{ $feedback->user }}</td>
                                <td>{{ $feedback->email }}</td>
                                <td>{{ $feedback->phone }}</td>
                                <td><div class="label label-{{$feedback->class}}">{{ __('general.'.$feedback::$feedbackStatuses[$feedback->status]['feedback_status_name']) }}</div></td>
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('feedbacks.edit', $feedback) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('feedbacks.destroy', $feedback) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$feedbacks->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
