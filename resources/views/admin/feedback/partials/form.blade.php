<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>{{__('general.date')}}: {{$feedback->created_at}}</label>
        </div>
        <div class="form-group">
            {!! Form::label('user', __('general.user')) !!}
            {!! Form::text('user',$feedback->user,['class' => 'form-control','size' => 255, 'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', __('general.email')) !!}
            {!! Form::text('email',$feedback->email,['class' => 'form-control','size' => 255, 'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', __('general.link_from')) !!}
            @if($feedback->link_from != '')<a href="{{$feedback->link_from}}">{{$feedback->link_from}}</a>@endif
            {!! Form::text('email',$feedback->link_from,['class' => 'form-control','size' => 255, 'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('phone', __('general.phone')) !!}
            {!! Form::text('phone',$feedback->phone,['class' => 'form-control','size' => 255, 'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('agent', __('general.agent')) !!}
            {!! Form::text('agent',$feedback->agent,['class' => 'form-control','size' => 255, 'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('message', __('general.message')) !!}
            {!! Form::textarea('message',$feedback->message,['class' => 'form-control','rows'=>'8','readonly']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            {!! Form::label('status', __('general.status')) !!}
            {!! Form::select('status',$statuses,$feedback->status ?? null,['class' => 'form-control']) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('reaction') ? 'has-error' : ''}}">
            {!! Form::label('reaction', __('general.action')) !!}
            {!! Form::textarea('reaction',$feedback->reaction ?? null,['class' => 'form-control','placeholder' => __('general.action'),'rows'=>'8']) !!}
            {!! $errors->first('reaction', '<p class="help-block">:message</p>') !!}
        </div>
        <button type="button" id="send_reaction" class="btn btn-success">{{__('general.send')}}</button>
    </div>
</div>

@push('js')
    <script>
        $(document).ready(function () {
            $('#send_reaction').on('click',function () {
                $.post('{{route('send_feedback_reaction')}}', {message:$('textarea[name=reaction]').val(),feedback_id: {{$feedback->id}}}
                ).done(function (response) {

                    $('#send_reaction').attr('disabled','disabled').after('<div class="alert alert-success">Сообщение было отправлено</div>');
                    setTimeout(function () {
                        $('.alert-success').fadeOut();
                    },8000)
                    $('select[name="status"]').val(1);
                }).fail(function (response) {
                    console.log(response);
                });
            })
        })
    </script>    
@endpush

