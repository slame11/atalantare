@extends('admin.layouts.admin-lte')

@section('title', __('general.generalSettings'))
@section('content-title', __('general.general_settings'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-star"></i>{{ __('general.general_settings') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        @if(session()->has('general-settings-success'))
        <div class="col-md-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session()->get('general-settings-success') }}
            </div>
        </div>
        @endif
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.general_settings') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-sm btn-success', 'form' => 'general_settings_form']) !!}
                    </div>
                </div>
                {!! Form::open(['route' => 'generalSettings.store', 'method' => 'post', 'id' => 'general_settings_form']) !!}
                <div class="box-body">
                    @foreach($settings as $setting)
                    <div class="form-group">
                        <label>{{ __('general.setting_'.$setting->key) }}</label>
                        <input type="text" value="{{ $setting->value ?? '0' }}" class="form-control" name="setting[{{ $setting->key }}]">
                    </div>
                    @endforeach
                    {!! Form::hidden('modified_by', Auth::id()) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
