@extends('admin.layouts.admin-lte')

@section('title', __('general.import'))
@section('content-title', __('general.import'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-building"></i>{{ __('general.import') }}</li>
    </ol>
@endsection

@section('content')
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{__('general.import')}} </h3>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="{{route('import_glamourapartments')}}" method="post" id="import_form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>{{__('general.load_file')}}</label>
                            {{ Form::file('json_file')}}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">{{__('general.import')}}</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection

