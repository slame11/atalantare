@extends('admin.layouts.admin-lte')

@section('title', __('general.categories'))
@section('content-title', __('general.categories'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-star"></i>{{ __('general.categories') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.categories_list') }}</h3>
                    <div class="pull-right">
                        <a href="{{ route('categories.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.image') }}</th>
                            <th>{{ __('general.name') }}</th>
                            <th>{{ __('general.type') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($categories as $category)
                            <tr>
                                <td><img src="{{ Storage::url($category->image) }}" alt=""></td>
                                <td>{{ $category->name }}</td>
                                <td>@php $types= []; foreach($category->types as $type){ $types[] = trans('general.real_estate_type_id.'.$type->type_id); } @endphp
                                {{ implode(',',$types) }}</td>
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('categories.edit', $category) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('categories.destroy', $category) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$categories->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

