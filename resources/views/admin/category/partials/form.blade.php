<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
            {!! Form::label('input-image-photo', __('general.image')) !!}
            <a href="" id="thumb-image-photo" data-toggle="image" class="img-thumbnail">
                <img src="{{ $category->image != '' ? $category->thumb : Storage::url('public/images/no-image-box.png') }}" alt="" title="" data-placeholder="test" />
            </a>
            <input type="hidden" name="image" value="{{$category->image}}" id="input-image-photo" />
            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('type_id', __('general.real_estate_type')) !!}
            <select class="select2 form-control" multiple name="type_id[]">
                @foreach(\App\Models\RealEstate::$realestateTypes as $type => $type_descr)
                    <option @if(in_array($type, $selected)) selected @endif value="{{$type}}">{{__('general.real_estate_type_id.'.$type)}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

@include('admin.category.partials.translations')
@push('js')
 <script>
     $('.select2').select2({
         placeholder: '{{__('general.choose')}}',
         tags: true,
         width: 'resolve',
     });
 </script>
    @endpush
