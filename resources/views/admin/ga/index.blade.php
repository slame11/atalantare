@extends('admin.layouts.admin-lte')

@section('title', 'Google Analytics')
@section('content-title', 'Google Analytics')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-file"></i> Google Analytics</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                {{ Form::open(['method'=>'post']) }}
                <div class="box-header with-border">
                    <h3 class="box-title">Google Analytics scripts</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('head','<head> script code') }}
                            {{ Form::textarea('head', $ga->head, ['rows' => 4, 'class' => 'col-sm-12']) }}
                        </div>
                        <div class="col-sm-12">
                            {{ Form::label('body','<body> script code') }}
                            {{ Form::textarea('body', $ga->body, ['rows' => 4, 'class' => 'col-sm-12']) }}
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

