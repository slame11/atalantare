<div class="nav-tabs-custom nav-tabs-translation">
    <ul class="nav nav-tabs">
        @foreach ($locales as $locale)
            @php
                $tabHeaderClass = '';
                if($review->availableTranslation($locale['locale'])) {
                    $tabHeaderClass = 'has-translation';
                }
                if ($errors->has('*-'.$locale['locale'])) {
                    $tabHeaderClass .= ' error-tab-header';
                }
                if ($loop->first) {
                    $tabHeaderClass .= ' active';
                }
            @endphp
            <li class="{{ $tabHeaderClass }}">
                <a href="#tab_{{ $loop->index }}" data-toggle="tab">
                    {{ __('general.translation_language.'.$locale['locale']) }}
                    <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($locales as $locale)
            <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="tab_{{$loop->index}}">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox(
                                    'available_translation-'.$locale['locale'],
                                    1,
                                    $review->availableTranslation($locale['locale']),
                                    [
                                    'class'=>'tab-switch',
                                    'data-controltab'=>'langtab-'.$locale['locale'],
                                    ]
                                ) !!}
                                {{ __('general.activate_translation.'.$locale['locale']) }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row langtab-{{$locale['locale']}}
                {{($review->availableTranslation($locale['locale']) || old('available_translation-'.$locale['locale'])) ? '' : 'inactive-tab'}}">
                    <div class="col-sm-12">
                        <div class="form-group {{ $errors->has('review-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('review-'.$locale['locale'], __('general.review', [], $locale['locale'])) !!}
                            {!! Form::textarea('review-'.$locale['locale'],array_key_exists('review',$locale)?$locale['review']:null,['class' => 'form-control','rows'=>'8','placeholder' => __('general.review', [], $locale['locale'])]) !!}
                            {!! $errors->first('review-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
