<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('full_name') ? 'has-error' : ''}}">
            {!! Form::label('full_name', __('general.full_name')) !!}
            {!! Form::text('full_name',$review->full_name ?? null,['class' => 'form-control','placeholder' => __('general.full_name'),'size' => 255]) !!}
            {!! $errors->first('full_name', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            {!! Form::label('email', __('general.email')) !!}
            {!! Form::text('email',$review->email ,['class' => 'form-control',]) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="checkbox {{ $errors->has('published') ? 'has-error' : ''}}">
            <label>
                {!! Form::checkbox('published', 1, $review->published ?? false) !!}
                {{ __('general.published') }}
                {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>

    <div class="col-sm-6"></div>
</div>

@include('admin.review.partials.translations')