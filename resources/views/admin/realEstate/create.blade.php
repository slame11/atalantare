@extends('admin.layouts.admin-lte')

@section('title', __('general.real_estate'))
@section('content-title', __('general.real_estate'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('realEstates.index') }}"><i class="fa fa-building"></i> {{ __('general.real_estate') }}</a></li>
        <li class="active"><i class="fa fa-building"></i> {{ __('general.new') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.create') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'real_estate_create_form']) !!}

                        <a href="{{ route('realEstates.index',['real_estate_type_id'=>$realEstate->real_estate_type_id]) }}" class="btn btn-default"><i class="fa fa fa-share"></i></a>

                    </div>
                </div>
                {!! Form::open(['route' => 'realEstates.store','method' => 'post','id'=>'real_estate_create_form']) !!}
                <div class="box-body">

                    @include('admin.realEstate.partials.form')

                    {!! Form::hidden('created_by', Auth::id()) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

