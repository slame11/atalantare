@extends('admin.layouts.admin-lte')

@section('title', __('general.real_estate'))
@section('content-title', __('general.real_estate'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-building"></i>{{ __('general.real_estate') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> {{ __('general.filter-catalog') }}</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('realEstates.index')}}">
                        <div class="form-group">
                            <label class="control-label" for="input-name">{{ __('general.name') }}</label>
                            <input type="text" name="filter_name" value="{{$name}}" placeholder="{{ __('general.name') }}" id="input-name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="input-keyword">{{ __('general.keyword') }}</label>
                            <input type="text" name="filter_keyword" value="{{$keyword}}" placeholder="{{ __('general.keyword') }}" id="input-keyword" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="filter_category">{{__('general.categories')}}</label>
                            <select id="filter_category" name="category" class="form-control">
                                <option value="0">{{__('general.filter_all_select')}}</option>
                                @foreach($categories as $value => $name)
                                    <option value="{{$value}}" @if($value == $category_id) selected @endif>{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="filter_country">{{__('general.country')}}</label>
                            <select id="filter_country" name="country_id" class="form-control">
                                <option value="0">{{__('general.filter_all_select')}}</option>
                            @foreach($countries as $key => $country)
                                    <option value="{{$key}}" @if($key == $country_id) selected @endif>{{$country}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="filter_city">{{__('general.city')}}</label>
                            <select id="filter_city" name="city_id" class="form-control">
                                <option value="0">{{__('general.filter_all_select')}}</option>
                                @foreach($cities as $key => $city)
                                    <option value="{{$key}}" @if($key == $city_id) selected @endif>{{$city}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="real_estate_type_id" value="{{$real_estate_type_id}}">
                        <div class="form-group text-left">
                            <button type="submit" id="button-filter" class="btn btn-default"><i class="fa fa-filter"></i> {{ __('general.filter-catalog') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">@if(($real_estate_type) != ''){{ $real_estate_type  }} -@endif {{ __('general.real_estate') }} </h3>
                    <div class="pull-right">
                        <a href="{{ route('realEstates.create',['real_estate_type_id'=>$real_estate_type_id]) }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.image') }}</th>
                            <th>{{ __('general.name') }}</th>
                            <th>{{ __('general.category') }}</th>
                            <th>{{ __('general.price') }}</th>
                            <th>{{ __('general.city') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($realEstates as $realEstate)
                            <tr>
                                @if (isset ($realEstate->image))
                                    <?php
                                    $realEstate->thumb = \App\Models\ImageTool::resize($realEstate->image->first()['path'],100,100);
                                    ?>
                                    <td><div class="img-thumbnail"><img src="{{ $realEstate->thumb }}" /></div></td>
                                @else
                                    <td></td>
                                @endif
                                @if (isset ($realEstate->title))
                                    <td>{{ $realEstate->title }}</td>
                                @else
                                    <td></td>
                                @endif
                                @if ( isset ($realEstate->category->name))
                                    <td>{{ $realEstate->category->name }}</td>
                                @else
                                    <td></td>
                                @endif
                                @if ( $realEstate->price_upon_request )
                                    <td>{!! __('general.price_upon_request') !!}</td>
                                @elseif ( isset ($realEstate->price_format))
                                    <td>{!! $realEstate->price_format !!}</td>
                                @else
                                    <td></td>
                                @endif

                                @if ( isset ($realEstate->city->name))
                                    <td>{{ $realEstate->city->name }}</td>
                                @else
                                    <td></td>
                                @endif
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('realEstates.edit', $realEstate) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('realEstates.destroy', $realEstate) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$realEstates->appends(['real_estate_type_id' => $real_estate_type_id])->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script>
    $('document').ready(function () {
        $('select[name=country_id]').on('change',function () {
            $('select[name=city_id]').empty();
            if(parseInt($(this).val()) == 0){
                $('select[name=city_id]').append('<option value="0">{{__('general.choose')}}</option>');
            }else {
                $.post('/getcities', {country_id: $(this).val()}
                ).done(function (response) {
                    $('select[name=city_id]').append('<option value="0">{{__('general.choose')}}</option>');
                    $.each(response, function (index, value) {
                        $('select[name=city_id]').append('<option value="' + index + '">' + value + '</option>');
                    });
                }).fail(function (response) {
                    console.log(response);
                });
            }
        });
    })
</script>    
@endpush

