<tr data-key="replacementKey">
    <td>
        @foreach ($locales as $locale)
            <div class="input-group">
                <span class="input-group-addon"><i class=" flag-icon flag-icon-{{$locale}}"></i></span>
                <input class="form-control" type="text" name="image_title[replacementKey][{{$locale}}]" value="" />
            </div>
        @endforeach
    </td>
    <td>
        <input class="form-control" type="text" name="image_sort[replacementKey]" value="0" />
    </td>

    <td>
        <a href="" id="thumb-image-replacementRandomStr" data-toggle="image" class="img-thumbnail">
            <img src="{{ Storage::url($thumb)}}" alt="" title="" data-placeholder="test" />
        </a>
        <input type="hidden" name="image[replacementKey]" value="{{$path}}" id="input-image-replacementRandomStr" />
    </td>

    <td><button type="button" class="btn btn-danger remove_image_from_table"><i class="fa fa-trash"></i></button></td>
</tr>