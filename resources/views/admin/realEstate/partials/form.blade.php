<div class="row">
    <div class="col-sm-12">
    </div>
    <div class="col-sm-12">
    </div>
</div>

<div class="nav-tabs-custom nav-tabs-general nav-tabs-translation">
    <ul class="nav nav-tabs nav-ul-general">
        <li  class="active @if($errors->has('attribute_id.*')) error-tab-header @endif">
            <a href="#tab_general" data-toggle="tab" class="general-tabs">
                {{__('general.general')}}
                <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
            </a>
        </li>
        <li>
            <a href="#tab_descriptions" data-toggle="tab" class="general-tabs">
                {{__('general.descriptions')}}
                <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
            </a>
        </li>
        <li>
            <a href="#tab_images" data-toggle="tab" class="general-tabs">
                {{__('general.images')}}
                <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
            </a>
        </li>
        <li>
            <a href="#tab_agents" data-toggle="tab" class="general-tabs">
                {{__('general.agents')}}
                <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
            </a>
        </li>
        <li>
            <a href="#tab_seo" data-toggle="tab" class="general-tabs">
                {{__('general.seo')}}
                <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_general">
            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
                        {!! Form::label('country_id', __('general.country')) !!}
                        {!! Form::select('country_id',$countries,$realEstate->country_id ?? null,['class' => 'form-control']) !!}
                        {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('city_id') ? 'has-error' : ''}}">
                        {!! Form::label('city_id', __('general.city')) !!}
                        {!! Form::select('city_id',$cities,$realEstate->city_id ?? null, ['class' => 'form-control select2']) !!}
                        {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group  {{ $errors->has('real_estate_type_id') ? 'has-error' : ''}}">
                        {!! Form::label('real_estate_type_id', __('general.type')) !!}
                        {!! Form::select('real_estate_type_id',$realEstateTypes,$realEstate->real_estate_type_id ?? null, ['class' => 'form-control realEstateType']) !!}
                        {!! $errors->first('real_estate_type_id', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group  {{ $errors->has('category_id') ? 'has-error' : ''}}">
                        {!! Form::label('category_id', __('general.category')) !!}
                        {!! Form::select('category_id',$categories,$realEstate->category_id ?? null, ['class' => 'form-control']) !!}
                        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('map_point') ? 'has-error' : ''}}">
                        {!! Form::label('map_point', __('general.map_point')) !!}
                        {!! Form::text('map_point',$realEstate->map_point ?? '0 0',['class' => 'form-control','placeholder' => __('general.map_point'),'size' => 255]) !!}
                        {!! $errors->first('map_point', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                        <div class="row">
                            <div class="col-sm-4">
                                {!! Form::label('price', __('general.price')) !!}
                                {!! Form::number('price', preg_replace("/[^0-9]/", '', $realEstate->real_price) ? preg_replace("/[^0-9]/", '', $realEstate->price) :  null,['class' => 'form-control','placeholder' => __('general.price'),'disabled'=>($realEstate->price_upon_request || old('price_upon_request'))?true:false]) !!}
                                {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-sm-4">
                                {!! Form::label('currency','' ) !!}
                                <select name="currency" class="form-control" {{($realEstate->price_upon_request || old('price_upon_request'))?'disabled':''}}>
                                    <option @if($realEstate->currency =='EUR') selected @endif  >EUR</option>
                                    <option @if($realEstate->currency =='USD') selected @endif  >USD</option>
                                    <option @if($realEstate->currency =='RUB') selected @endif  >RUB</option>
                                    <option @if($realEstate->currency =='GBP') selected @endif  >GBP</option>
                                    <option @if($realEstate->currency =='CNY') selected @endif  >CNY</option>
                                    <option @if($realEstate->currency =='CHF') selected @endif  >CHF</option>
                                </select>
                            </div>
                            <div class="col-sm-4 rentPeriod {{ $errors->has('rent_period') ? 'has-error' : ''}}">
                                {!! Form::label('rent_period', __('general.rent_period')) !!}
                                {!! Form::select('rent_period',$rent_period,$rent_period->rent_period_type_id ?? null, ['class' => 'form-control']) !!}
                                {!! $errors->first('rent_period', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                {!! Form::checkbox('price_upon_request', 1, $realEstate->price_upon_request ?? 0) !!}
                                {!! __('general.price_upon_request') !!}
                            </div>
                        </div>
                    </div>
                    <div class="checkbox {{ $errors->has('show_on_home_slider') ? 'has-error' : ''}}">
                        <label>
                            {!! Form::checkbox('show_on_home_slider', 1, $realEstate->show_on_home_slider ?? 0) !!}
                            {{ __('general.show_on_home_slider') }}
                            {!! $errors->first('show_on_home_slider', '<p class="help-block">:message</p>') !!}
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <table class="table table-responsive table-striped table-bordered resultAttributes">
                        <tr>
                            <td>{{__('general.name')}}</td>
                            <td>{{__('general.value')}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="form-group position-relative">
                                   <select name="attribute-search" id="attribute-search" class="form-control select2">
                                       @foreach($all_attributes as $attr)
                                           @if(!in_array($attr['id'], array_column($attributes, 'attribute_id')))
                                               <option data-type="{{$attr['type']}}" value="{{$attr['id']}}">{{$attr['name']}}</option>
                                           @endif
                                       @endforeach
                                   </select>
                                </div>
                            </td>
                            <td><button type="button" class="btn btn-primary" id="add_atribute"><i class="fa fa-plus"></i></button></td>
                        </tr>

                        @if(old('attribute_id'))
                            @foreach(old('attribute_id') as $attribute_id => $attr_value)
                                <tr>
                                    <td @if($errors->has('attribute_id.'.$attribute_id)) style="color:red;" @endif>{{$attr_assoc[$attribute_id]['name']}}</td>
                                    @if(strtolower($attr_assoc[$attribute_id]['type']) === 'checkbox')
                                        <td>
                                           <div class="form-group @if($errors->has('attribute_id.'.$attribute_id)) has-error @endif" >
                                               <input class="form-control" data-type="{{$attr_assoc[$attribute_id]['type']}}" type="hidden" name="attribute_id[{{$attribute_id}}]" value="{{$attr_value}}">
                                           </div>
                                        </td>
                                    @else
                                        <td>
                                            <div class="form-group @if($errors->has('attribute_id.'.$attribute_id)) has-error @endif" >
                                                <input class="form-control" data-type="{{$attr_assoc[$attribute_id]['type']}}" type="text" name="attribute_id[{{$attribute_id}}]" value="{{$attr_value}}">
                                            </div>
                                           </td>
                                    @endif
                                    <td><button type="button" class="btn btn-danger remove_attr_item"><i class="fa fa-trash"></i></button></td>
                                </tr>
                            @endforeach
                        @else
                            @if(! empty($attributes))
                                @foreach($attributes as $attribute)
                                    <tr>
                                        <td>{{$attribute['name']}}</td>
                                        @if(strtolower($attribute['type']) === 'checkbox')
                                            <td><input class="form-control"  data-type="{{$attribute['type']}}" type="hidden" name="attribute_id[{{$attribute['attribute_id']}}]" value="{{$attribute['value']}}"></td>
                                        @else
                                            <td><input class="form-control"  data-type="{{$attribute['type']}}" type="text" name="attribute_id[{{$attribute['attribute_id']}}]" value="{{$attribute['value']}}"></td>
                                        @endif
                                        <td><buton type="button" class="btn btn-danger remove_attr_item"><i class="fa fa-trash"></i></buton></td>
                                    </tr>
                                @endforeach
                            @endif
                            @endif
                    </table>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab_images">
            <div id="multipleAddImg"><div class="dz-default dz-message"></div></div>
            <table class="table table-striped table-bordered" id="image_table">
                <tr data-key="0">
                    <td>Title</td>
                    <td>
                        Sort
                    </td>
                    <td colspan="2">
                        Image <button type="button" class="pull-right btn btn-primary " id="add_image"><i class="fa fa-plus"></i></button>
                    </td>
                </tr>
                @if(old('image'))
                    @foreach(old('image') as $key => $image)
                        @php
                            $bytes = random_bytes(10);
                            $str = bin2hex($bytes);

                            $thumb = \App\Models\ImageTool::resize($image,100,100);
                        @endphp
                        <tr data-key="{{ $loop->index + 1}}">
                            <td>
                                @foreach ($locales as $locale)
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class=" flag-icon flag-icon-{{$locale['locale']}}"></i></span>
                                        <input class="form-control" type="text" name="image_title[{{$key}}][{{$locale['locale']}}]" value="{{old('image_title')[$key][$locale['locale']]}}" />
                                    </div>
                                @endforeach
                            </td>
                            <td>
                                <input class="form-control" type="text" name="image_sort[{{$key}}]" value="{{old('image_sort')[$key]}}" />
                            </td>
                            <td>
                                <a href="" id="thumb-image-{{$str}}" data-toggle="image" class="img-thumbnail">
                                    <img src="{{$thumb}}" alt="" title="" data-placeholder="test" />
                                </a>
                                <input type="hidden" name="image[{{$key}}]" value="{{$image}}" id="input-image-{{$str}}" />
                            </td>
                            <td><button type="button" class="btn btn-danger remove_image_from_table"><i class="fa fa-trash"></i></button></td>
                        </tr>
                    @endforeach
                @else
                @foreach ($realEstateImages as $key =>$image)
                    @php
                        $bytes = random_bytes(10);
                        $str = bin2hex($bytes);
                    @endphp
                    <tr data-key="{{ $loop->index + 1}}">
                        <td>
                            @foreach ($image['locales'] as  $locale)
                                <div class="input-group">
                                    <span class="input-group-addon"><i class=" flag-icon flag-icon-{{$locale['locale']}}"></i></span>
                                    <input class="form-control" type="text" name="image_title[{{$key + 1}}][{{$locale['locale']}}]" value="@if(isset($locale['title'])){{$locale['title']}}@endif" />
                                </div>
                            @endforeach
                        </td>
                        <td>
                            <input class="form-control" type="text" name="image_sort[{{$key + 1}}]" value="{{$image['sort']}}" />
                        </td>
                        <td>
                            <a href="" id="thumb-image-{{$str}}" data-toggle="image" class="img-thumbnail">
                                <img src="{{$image['thumb']}}" alt="" title="" data-placeholder="test" />
                            </a>
                            <input type="hidden" name="image[{{$key + 1}}]" value="{{$image['path']}}" id="input-image-{{$str}}" />
                        </td>
                        <td><button type="button" class="btn btn-danger remove_image_from_table"><i class="fa fa-trash"></i></button></td>
                    </tr>
                @endforeach
                @endif
            </table>
        </div>
        <div  class="tab-pane" id="tab_descriptions">
            @include('admin.realEstate.partials.translations')
        </div>
        <div class="tab-pane" id="tab_agents">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{ $errors->has('agents') ? 'has-error' : ''}}">
                        {!! Form::label('agents', __('general.agents')) !!}
                        {!! Form::select('agents[]',$agents,$realEstate->agents->pluck('id') ?? null,['class' => 'form-control agents_select select2', 'name' => 'agents[]', 'multiple','style' => 'width: 100%;']) !!}
                        {!! $errors->first('agents', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>

        </div>
        <div  class="tab-pane" id="tab_seo">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{ $errors->has('seo_url') ? 'has-error' : ''}}" >
                        <label id="seo_url" class="col-sm-2 control-label">{{__('general.seo_url')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{$realEstate->seo_url}}" name="seo_url" />
                            {!! $errors->first('seo_url', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@push('js')
    <script>
        $(document).ready(function () {
            $('a[href="#tab_agents"]').on('click',function () {
                $('.agents_select').select2("destroy");
                $('.agents_select').select2({
                    placeholder: '{{__('general.choose')}}',
                    style: 'width:100%',
                    tags: true,
                    width: 'resolve',
                });
            })
            $('.select2').select2({
                placeholder: '{{__('general.choose')}}',
                style: 'width:100%',
            });

            $('.agents_select').select2({
                placeholder: '{{__('general.choose')}}',
                style: 'width:100%',
                tags: true,
                width: 'resolve',
            });
            $('body').on('click','.remove_image_from_table',function () {
                $(this).parent().parent().remove();
            }).on('change','#country_id',function (e) {
                $.post('/getcities', {country_id: $(this).val()}
                ).done(function (response) {
                    $('#city_id').empty();
                    $.each(response, function(index, value) {
                        $('#city_id').append('<option value="'+index+'">'+value+'</option>').select2('destroy').select2();
                    });

                }).fail(function (response) {
                    console.log(response);
                });
            });
            // var images_exist = $('#image_table').find('tr').length-1; //tr in head doesnt counted
             //tr in head doesnt counted
            $('#add_image').on('click',function () {
                var str =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                var images_exist = $('#image_table tr').last().data('key');

                images_exist++;

                $(this).parent().parent().parent().append(
                    '<tr data-key="'+images_exist+'">\n' +
                    '                <td>\n' +
                        @foreach ($locales as $key => $locale)
                    '<div class="input-group">'+
                    '<span class="input-group-addon"><i class=" flag-icon flag-icon-{{$locale["locale"]}}"></i></span>'+
                    '                    <input class="form-control" type="text" name="image_title['+images_exist+'][{{$locale["locale"]}}]" value="" />' +
                    '</div>'+
                        @endforeach
                    '                </td>' +
                    '                <td>' +
                    '                    <input class="form-control" type="text" name="image_sort['+images_exist+']" value="0" />\n' +
                    '                </td>\n' +
                    '                <td>\n' +
                    '                    <a href="" id="thumb-image-'+str+'" data-toggle="image" class="img-thumbnail">\n' +
                    '                        <img src="/storage/images/no-image-box.png" alt="" title="" data-placeholder="test" />\n' +
                    '                    </a>\n' +
                    '                    <input type="hidden" name="image['+images_exist+']" value="" id="input-image-'+str+'" />\n' +
                    '                </td>\n' +
                    '                <td><button type="button" class="btn btn-danger remove_image_from_table"><i class="fa fa-trash"></i></button></td>'+
                    '            </tr>'
                );
            })
        })
    </script>

@endpush
