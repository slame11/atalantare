<div class="nav-tabs-custom nav-tabs-translation">
    <ul class="nav nav-tabs">
        @foreach ($locales as $locale)
            @php
                $tabHeaderClass = '';
                if($realEstate->availableTranslation($locale['locale'])) {
                    $tabHeaderClass = 'has-translation';
                }
                if ($errors->has('*-'.$locale['locale'])) {
                    $tabHeaderClass .= ' error-tab-header';
                }
                if ($loop->first && !$errors->has('*-'.$locale['locale'])) {
                    $tabHeaderClass .= ' active';
                }
            @endphp
            <li class="{{ $tabHeaderClass }}">
                <a href="#tab_{{ $loop->index }}" data-toggle="tab">
                    <i class="flag-icon flag-icon-{{$locale['locale']}}"></i>
                    <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($locales as $locale)
            <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="tab_{{$loop->index}}">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox(
                                    'available_translation-'.$locale['locale'],
                                    1,
                                    $realEstate->availableTranslation($locale['locale']),
                                    [
                                    'class'=>'tab-switch',
                                    'data-controltab'=>'langtab-'.$locale['locale'],
                                    ]
                                ) !!}
                                {{ __('general.activate_translation.'.$locale['locale']) }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row langtab-{{$locale['locale']}}
                {{($realEstate->availableTranslation($locale['locale']) || old('available_translation-'.$locale['locale'])) ? '' : 'inactive-tab'}}">
                    <div class="col-sm-12">
                        <div class="form-group {{ $errors->has('title-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('title-'.$locale['locale'], __('general.title', [], $locale['locale'])) !!}
                            {!! Form::text('title-'.$locale['locale'],array_key_exists('title',$locale)?$locale['title']:null,['class' => 'form-control','placeholder' => __('general.title', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('title-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('keyword-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('keyword-'.$locale['locale'], __('general.keyword', [], $locale['locale'])) !!}
                            {!! Form::text('keyword-'.$locale['locale'],array_key_exists('keyword',$locale)?$locale['keyword']:null,['class' => 'form-control','placeholder' => __('general.keyword', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('keyword-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('media_description-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('media_description-'.$locale['locale'], __('general.media_description', [], $locale['locale'])) !!}
                            {!! Form::text('media_description-'.$locale['locale'],array_key_exists('media_description',$locale)?$locale['media_description']:null,['class' => 'form-control','placeholder' => __('general.media_description', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('media_description-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('address-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('address-'.$locale['locale'], __('general.address', [], $locale['locale'])) !!}
                            {!! Form::text('address-'.$locale['locale'],array_key_exists('address',$locale)?$locale['address']:null,['class' => 'form-control','placeholder' => __('general.address', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('address-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('description-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('description-'.$locale['locale'], __('general.description', [], $locale['locale'])) !!}
                            {!! Form::textarea('description-'.$locale['locale'], array_key_exists('description',$locale)?$locale['description']:null, ['id' => 'description-'.$locale['locale'], 'class' => 'form-control realestate-textarea','rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                            {!! $errors->first('description-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@push('js')
    <script>
        $('textarea.realestate-textarea').ckeditor({
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
        });
    </script>
@endpush