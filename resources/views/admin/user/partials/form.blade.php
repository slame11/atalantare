<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12 ">
            <input type="hidden" name="avatar_id" id="avatar-id" value="@if(old('avatar_id')){{old('avatar_id')}}@else{{isset($user->avatar_id)?$user->avatar_id:null}}@endif">
            <input type="hidden" name="avatar_name" id="avatar-name" value="">
            <input type="hidden" name="avatar_path" id="avatar-path" value="">

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', __('general.user')) !!}
                {!! Form::text('name',$user->name ?? null,['class' => 'form-control','placeholder' => __('general.user'),'size' => 255,'required'=>'true']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', __('general.email')) !!}
                {!! Form::text('email',$user->email ?? null,['class' => 'form-control','placeholder' => __('general.email'),'size' => 255,'required'=>'true']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : ''}}">
                {!! Form::label('phone_number', __('general.phone')) !!}
                {!! Form::text('phone_number',$user->phone_number ?? null,['class' => 'form-control','placeholder' => __('general.phone'),'size' => 255,'required'=>'true']) !!}
                {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group">
                <label for="">{{ __('general.password') }}</label>
                <div class="input-group {{ $errors->has('password') ? 'has-error' : ''}}">
                    <input type="password" class="form-control" name="password" placeholder="{{ __('general.password') }}" size="255" data-character-set='a-z,A-Z,0-9' rel='gp' data-size='16' />
                    <span class="input-group-btn" id="hashpassword">
                        <button type="button" class="btn btn-default getNewPass">
                    <span class="fa fa-refresh"></span>
                </button>
            </span>
                </div>
            </div>


            <div class="form-group">
                <label for="">{{ __('general.password_confirm') }}</label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="{{ __('general.repeat password') }}" size="255" rel="gp_confirm" />
            </div>

            <div class="form-group">
                <label for="">{{ __('general.give_role') }}</label>
                <select class="form-control give-role-select2-multiple" name="roles[]">
                    @include('admin.user.partials.roles', ['roles' => $roles])
                </select>
            </div>

            <br /><br />

            @php
                $isActive = is_array($user) ? 0 : $user->is_active;
            @endphp

            <div class="form-group">
                <label>{{ __('general.status') }}</label>
                <input type="radio" {{ ($isActive == 0) ? 'checked=""' : '' }} value='0' name="is_active">{{ __('general.inactive') }}
                <input type="radio" {{ ($isActive == 1) ? 'checked=""' : '' }} value='1' name="is_active">{{ __('general.active') }}
            </div>

            <br />
        </div>

    </div>

    <div class="col-sm-6"></div>
</div>

{{--@include('admin.user.partials.translations')--}}