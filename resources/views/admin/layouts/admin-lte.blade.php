<!DOCTYPE html>
<html lang="@yield('lang', config('app.locale', 'en'))">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>@yield('title', config('app.name', 'AdminLTE'))</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="userId" content="{{ Auth::check() ? Auth::user()->id : 'null' }}">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Styles -->
  @section('styles')
    <link href="{{ asset('/vendor/unisharp/laravel-ckeditor/contents.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin-lte.css') }}" rel="stylesheet">
  @show

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  @stack('head')
</head>

<body class="hold-transition skin-{{ config('admin-lte.skin', 'black-light') }} {{ config('admin-lte.layout', 'sidebar-mini') }}">
  <div id="app" class="wrapper">
    <!-- Main Header -->
    @include('admin-lte::admin.layouts.main-header.main')
    <!-- Left side column. contains the logo and sidebar -->
    @include('admin-lte::admin.layouts.main-sidebar.main')

    <!-- Content Wrapper. Contains page content -->
    @include('admin-lte::admin.layouts.content-wrapper.main')

    <!-- Main Footer -->
    @include('admin-lte::admin.layouts.main-footer.main')

    <!-- Control Sidebar -->
    {{-- @include('admin-lte::layouts.control-sidebar.main') --}}
  </div>

  <form class="form-default" id="form-default">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
    <input type="hidden" name="user-id" id="user-id" value="{{ Auth::id() }}" />
  </form>

  @section('scripts')
  <script src="{{ mix('/js/admin-lte.js') }}" charset="utf-8"></script>
  <script src="{{ mix('/js/admin.js') }}" charset="utf-8"></script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}" charset="utf-8"></script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}" charset="utf-8"></script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/config.js') }}" charset="utf-8"></script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/styles.js') }}" charset="utf-8"></script>

  @stack('js')
    @yield('js')
  @show
  @stack('body')
</body>
</html>
