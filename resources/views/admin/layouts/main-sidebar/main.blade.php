<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

    <!-- Sidebar Menu -->
        @php
            list($currentRouteType) = explode('.', Route::currentRouteName())
        @endphp
        @section('sidebar-menu')
            <ul class="sidebar-menu" data-widget="tree">
                @can('admin')
                    <li @if (in_array($currentRouteType, ['dashboard']) ) class="active" @endif>
                        <a href="{{ route('dashboard') }}" class="text-gray">
                            <i class="fa fa-dashboard"></i>
                            <span>{{ __('general.dashboard') }}</span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['pages']) ) class="active" @endif>
                        <a href="{{ route('pages.index') }}" class="text-gray">
                            <i class="fa fa-file"></i>
                            <span>{{ __('general.pages') }}</span>
                        </a>
                    </li>

                    <li @if (in_array($currentRouteType, ['realEstates']) ) class="active" @endif>
                        <a href="{{ route('realEstates.index') }}" class="text-gray">
                            <i class="fa fa-building"></i>
                            <span>{{ __('general.real_estate') }}</span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if (in_array($currentRouteType, ['realEstates']) ) class="active" @endif>
                                <a href="{{ route('realEstates.index',['real_estate_type_id' => 1]) }}" class="text-gray">
                                    <i class="fa fa-globe"></i>
                                    <span>{{ __('general.sales') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['realEstates']) ) class="active" @endif>
                                <a href="{{ route('realEstates.index',['real_estate_type_id' => 2]) }}" class="text-gray">
                                    <i class="fa fa-globe"></i>
                                    <span>{{ __('general.rents') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['realEstates']) ) class="active" @endif>
                                <a href="{{ route('realEstates.index',['real_estate_type_id' => 3]) }}" class="text-gray">
                                    <i class="fa fa-dollar"></i>
                                    <span>{{ __('general.investments') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li @if (in_array($currentRouteType, ['agents']) ) class="active" @endif>
                        <a href="{{ route('agents.index') }}" class="text-gray">
                            <i class="fa fa-users"></i>
                            <span>{{ __('general.agents') }}</span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['services']) ) class="active" @endif>
                        <a href="{{ route('services.index') }}" class="text-gray">
                            <i class="fa fa-star"></i>
                            <span>{{ __('general.services') }}</span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['articles']) ) class="active" @endif>
                        <a href="{{ route('articles.index') }}" class="text-gray">
                            <i class="fa fa-newspaper-o"></i>
                            <span>{{ __('general.articles') }}</span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['feedbacks']) ) class="active" @endif>
                        <a href="{{ route('feedbacks.index') }}" class="text-gray">
                            <i class="fa fa-feed"></i>
                            <span>{{ __('general.feedbacks') }} <span class="label label-danger">{{ \App\Models\Feedback::where('status',\App\Models\Feedback::FEEDBACK_STATUS_NEW)->count() }}</span></span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['reviews']) ) class="active" @endif>
                        <a href="{{ route('reviews.index') }}" class="text-gray">
                            <i class="fa fa-thumbs-up"></i>
                            <span>{{ __('general.reviews') }}</span>
                        </a>
                    </li>
                    <li class="treeview @if (in_array($currentRouteType, ['users', 'countries', 'cities','categories','attributes','attribute_types','realEstate_statuses','currencies',]) ) active @endif">
                        <a href="#" class="text-gray">
                            <i class="fa fa-gear"></i>
                            <span>{{ __('general.settings') }}</span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if (in_array($currentRouteType, ['users']) ) class="active" @endif>
                                <a href="{{ route('users.index') }}" class="text-gray">
                                    <i class="fa fa-users"></i>
                                    <span>{{ __('general.users') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['countries']) ) class="active" @endif>
                                <a href="{{ route('countries.index') }}" class="text-gray">
                                    <i class="fa fa-globe"></i>
                                    <span>{{ __('general.countries') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['cities']) ) class="active" @endif>
                                <a href="{{ route('cities.index') }}" class="text-gray">
                                    <i class="fa fa-globe"></i>
                                    <span>{{ __('general.cities') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['categories']) ) class="active" @endif>
                                <a href="{{ route('categories.index') }}" class="text-gray">
                                    <i class="fa fa-tasks"></i>
                                    <span>{{ __('general.categories') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['attributes']) ) class="active" @endif>
                                <a href="{{ route('attributes.index') }}" class="text-gray">
                                    <i class="fa fa-tasks"></i>
                                    <span>{{ __('general.attributes') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['generalSettings']) ) class="active" @endif>
                                <a href="{{ route('generalSettings.index') }}" class="text-gray">
                                    <i class="fa fa-tasks"></i>
                                    <span>{{ __('general.general_settings') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview @if (in_array($currentRouteType, ['import.glamourapartments']) ) active @endif">
                        <a href="#" class="text-gray">
                            <i class="fa fa-gear"></i>
                            <span>{{ __('general.import') }}</span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if (in_array($currentRouteType, ['impoty.glamourapartments']) ) class="active" @endif>
                                <a href="{{ route('import.glamourapartments') }}" class="text-gray">
                                    <i class="fa fa-users"></i>
                                    <span>{{ __('general.glamourapartments') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li @if (in_array($currentRouteType, ['ga.index']) ) class="active" @endif>
                        <a href="{{ route('ga.index') }}" class="text-gray">
                            <i class="fa fa-gear"></i>
                            <span>Google Analytics</span>
                        </a>
                    </li>
                @endcan
            </ul>
    @show
    <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
