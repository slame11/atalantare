@extends('admin.layouts.admin-lte')

@section('title', __('general.dashboard'))
@section('content-title', __('general.dashboard'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="active"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('general.real_estate') }}</h3>
                            <div class="chart-responsive">
                                <canvas id="RealEstates"></canvas>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('general.feedbacks') }}</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>{{ __('general.id') }}</th>
                                    <th>{{ __('general.user') }}</th>
                                    <th>{{ __('general.email') }}</th>
                                    <th>{{ __('general.status') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(! empty($feedbacks))
                                @foreach($feedbacks as $feedback)
                                <tr>
                                    <td><a href="{{route('feedbacks.edit', $feedback)}}">{{$feedback->id}}</a></td>
                                    <td>{{$feedback->user}}</td>
                                    <td>{{$feedback->email}}</td>
                                    <td><span class="label label-success">@if($feedback->status == 0) New @endif</span></td>
                                </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="{{route('feedbacks.index')}}", class="btn btn-sm btn-default btn-flat pull-right">View All Feedbacks</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('general.articles') }}</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>{{ __('general.slug') }}</th>
                                    <th>{{ __('general.name') }}</th>
                                    <th>{{ __('general.published') }}</th>
                                    <th>{{ __('general.viewed') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(! empty($articles))
                                    @foreach($articles as $article)
                                        <tr>
                                            <td><a href="{{route('articles.edit', $article)}}">{{$article->slug}}</a></td>
                                            <td>{{$article->name}}</td>
                                            {{--<td>{{$article->published}}</td>--}}
                                            <td>@if($article->published == 1)<span class="label label-success">Published</span> @else <span class="label label-danger">Unpublished</span> @endif</td>
                                            <td>{{$article->viewed}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="{{route('articles.index')}}", class="btn btn-sm btn-default btn-flat pull-right">View All Articles</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('general.reviews') }}</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>{{ __('general.full_name') }}</th>
                                    <th>{{ __('general.email') }}</th>
                                    <th>{{ __('general.review') }}</th>
                                    <th>{{ __('general.published') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(! empty($reviews))
                                    @foreach($reviews as $review)
                                        <tr>
                                            <td><a href="{{route('reviews.edit', $review)}}">{{$review->full_name}}</a></td>
                                            <td>{{$review->email}}</td>
                                            <td>{{$review->review}}</td>
                                            <td>@if($review->published == 1)<span class="label label-success">Published</span> @else <span class="label label-danger">Unpublished</span> @endif</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="{{route('reviews.index')}}", class="btn btn-sm btn-default btn-flat pull-right">View All reviewss</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>
        <div class="col-sm-12">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('general.agents') }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                            <table class="table no-margin">
                                <tbody>
                                @if(! empty($agents))
                                    @foreach($agents as $agent)
                                            <div class="col-sm-3">
                                                <img src="{{ $agent->photo != '' ? $agent->thumb : Storage::url('public/images/no-image-box.png') }}">
                                                <a class="users-list-name" href="{{route('agents.edit', $agent)}}">{{$agent->name}}</a>
                                                <div>{{$agent->phone}}</div>
                                                <div>{{$agent->email}}</div>
                                            </div>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="{{route('agents.index')}}" class="uppercase">View All Agents</a>
                    </div>
                    <!-- /.box-footer -->
                <!--/.box -->
            </div>
    </div>
@endsection
