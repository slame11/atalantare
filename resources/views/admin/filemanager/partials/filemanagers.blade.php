<div id="filemanager" class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">{{ __('general.filemanager') }}</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-5">
                    <a href="{{ $data['parent'] }}" data-toggle="tooltip" title="{{ __('general.filemanager_button_parent') }}" id="button-parent" class="btn btn-default">
                        <i class="fa fa-level-up"></i>
                    </a>
                    <a href="{{ $data['refresh'] }}" data-toggle="tooltip" title="{{ __('general.filemanager_button_refresh') }}" id="button-refresh" class="btn btn-default">
                        <i class="fa fa-refresh"></i>
                    </a>
                    <button type="button" data-toggle="tooltip" title="{{ __('general.filemanager_button_upload') }}" id="button-upload" class="btn btn-primary">
                        <i class="fa fa-upload"></i>
                    </button>
                    <button type="button" data-toggle="tooltip" title="{{ __('general.filemanager_button_folder') }}" id="button-folder" class="btn btn-default">
                        <i class="fa fa-folder"></i>
                    </button>
                    <button type="button" data-toggle="tooltip" title="{{ __('general.filemanager_button_delete') }}" id="button-delete" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i>
                    </button>
                </div>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" name="search" value="{{ $data['filter_name'] }}" placeholder="{{ __('general.filemanager_search_placeholder') }}" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" data-toggle="tooltip" title="{{ __('general.filemanager_button_search') }}" id="button-search" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
            <hr />

            <div class="row">
                @foreach($data['images'] as $image)
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
                        @if($image['type'] == 'directory')
                            <div class="text-center">
                                <a href="{{ $image['href'] }}" class="directory" style="vertical-align: middle;">
                                    <i class="fa fa-folder fa-5x"></i>
                                </a>
                            </div>
                            <label>
                                <input type="checkbox" name="path[]" value="{{ $image['path'] }}" />
                                {{ $image['name'] }}
                            </label>
                        @endif
                        @if($image['type'] == 'image')
                            <a href="{{ $image['href'] }}" class="thumbnail">
                                <img src="{{ $image['thumb'] }}" alt="{{ $image['name'] }}" title="{{ $image['name'] }}" />
                            </a>
                            <label>
                                <input type="checkbox" name="path[]" value="{{ $image['path'] }}" />
                                {{ $image['name'] }}
                            </label>
                        @endif
                    </div>
                @endforeach
            </div>
            <div class="modal-footer">{!! $data['pagination'] !!}</div>
        </div>
        <div class="hidden custom-target" data-target="{{ $data['target'] }}"></div>
        <div class="hidden custom-thumb" data-thumb="{{ $data['thumb'] }}"></div>
        <div class="hidden custom-directory" data-directory="{{ $data['directory'] }}"></div>
    </div>
</div>

<script type="text/javascript">

    <!--
    $('a.thumbnail').on('click', function(e) {
        e.preventDefault();
            $('#{{ $data['thumb'] }}').find('img').attr('src', $(this).find('img').attr('src'));
        $('#{{ $data['target'] }}').val($(this).parent().find('input').val());

        $('#modal-image').modal('hide');
    });

    $('a.directory').on('click', function(e) {
        e.preventDefault();

        $('#modal-image').load($(this).attr('href'));
    });

    $('.pagination a').on('click', function(e) {
        e.preventDefault();

        $('#modal-image').load($(this).attr('href'));
    });

    $('#button-parent').on('click', function(e) {
        e.preventDefault();

        $('#modal-image').load($(this).attr('href'));
    });

    $('#button-refresh').on('click', function(e) {
        e.preventDefault();

        $('#modal-image').load($(this).attr('href'));
    });

    $('input[name=\'search\']').on('keydown', function(e) {
        if (e.which == 13) {
            $('#button-search').trigger('click');
        }
    });

    $('#button-search').on('click', function(e) {

        var filter_name = $('input[name=\'search\']').val();
        var target = $('#filemanager .custom-target').data('target');
        var thumb = $('#filemanager .custom-thumb').data('thumb');
        var directory = $('#filemanager .custom-directory').data('directory');

        var url = '/admin/filemanager/show?directory=' + directory + '';

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        if(thumb){
            url += '&thumb=' + thumb;
        }

        if(target){
            url += '&target=' + target;
        }

        $('#modal-image').load(url);
    });
    //-->
</script>

<script type="text/javascript">

    $('#button-upload').on('click', function() {
        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file[]" value="" multiple="multiple" /></form>');

        $('#form-upload input[name=\'file[]\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        var directory = $('#filemanager .custom-directory').data('directory');

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file[]\']').val() != '') {
                clearInterval(timer);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/admin/filemanager/upload?directory=' + directory + '',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#button-upload i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#button-upload').prop('disabled', true);
                    },
                    complete: function() {
                        $('#button-upload i').replaceWith('<i class="fa fa-upload"></i>');
                        $('#button-upload').prop('disabled', false);
                    },
                    success: function(json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $('#button-refresh').trigger('click');
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });

    $('#button-folder').popover({
        html: true,
        placement: 'bottom',
        trigger: 'click',
        title: '{{ __('general.filemanager_new_folder_placeholder') }}',
        content: function() {
            html  = '<div class="input-group">';
            html += '  <input type="text" name="folder" value="" placeholder="{{ __('general.filemanager_new_folder_placeholder') }}" class="form-control">';
            html += '  <span class="input-group-btn"><button type="button" title="{{ __('general.filemanager_button_folder') }}" id="button-create" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></span>';
            html += '</div>';

            return html;
        }
    });

    $('#button-folder').on('shown.bs.popover', function() {
        $('#button-create').on('click', function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ route('new-folder', ['directory' => $data['directory']]) }}',
                type: 'post',
                dataType: 'json',
                data: 'folder=' + encodeURIComponent($('input[name=\'folder\']').val()),
                beforeSend: function() {
                    $('#button-create').prop('disabled', true);
                },
                complete: function() {
                    $('#button-create').prop('disabled', false);
                },
                success: function(json) {
                    if (json['error']) {
                        alert(json['error']);
                    }

                    if (json['success']) {
                        alert(json['success']);

                        $('#button-refresh').trigger('click');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });

    $('#button-delete').on('click', function(e) {
        if (confirm('{{ __('general.filemanager_text_confirm') }}')) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ route('delete-items') }}',
                type: 'post',
                dataType: 'json',
                data: $('input[name^=\'path\']:checked'),
                beforeSend: function() {
                    $('#button-delete').prop('disabled', true);
                },
                complete: function() {
                    $('#button-delete').prop('disabled', false);
                },
                success: function(json) {
                    if (json['error']) {
                        alert(json['error']);
                    }

                    if (json['success']) {
                        alert(json['success']);

                        $('#button-refresh').trigger('click');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
</script>
