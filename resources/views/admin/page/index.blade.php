@extends('admin.layouts.admin-lte')

@section('title', __('general.pages'))
@section('content-title', __('general.pages'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-file"></i>{{ __('general.pages') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.pages') }}</h3>
                    <div class="pull-right">
                        <a href="{{ route('pages.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.title') }}</th>
                            <th>{{ __('general.seo_url') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pages as $page)
                            <tr>
                                <td>{{$page->title}}</td>
                                <td>{{$page->seo_url}}</td>
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('pages.edit', $page) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    @if($page->id != 1)
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('pages.destroy', $page) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$pages->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

