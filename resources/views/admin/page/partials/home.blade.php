
@include('admin.page.partials.translations')


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('general.header') }}</h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <table id="top_menu_table" class="table table-hover table-striped table-bordered table-condensed">
                    <tr>
                        <td>{{ __('general.title') }}</td>
                        <td>{{ __('general.link') }}</td>
                        <td></td>
                    </tr>
                  @php $counter = 0; @endphp
                    @foreach($top_menu_items as $key => $menu_item)
                        <tr>
                            <td>
                                @foreach($menu_item['locales'] as $locale_code => $locale)
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class=" flag-icon flag-icon-{{$locale_code}}"></i></span>
                                        <input class="form-control" type="text" name="top_menu_items[{{$counter}}][locales][{{$locale_code}}]" value="{{$locale}}">
                                    </div>
                                @endforeach
                            </td>
                            <td>
                                <input class="form-control" type="text" value="{{$menu_item['url']}}" name="top_menu_items[{{$counter}}][url]"  placeholder="Menu item url" />
                            </td>
                            <td>
                                <button class="btn btn-danger menu_item_remove" type="button"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>

                        @php $counter++; @endphp
                    @endforeach
                    <tr>
                        <td colspan="3">
                            <button type="button" class="btn btn-primary menu_item_add pull-right"><i class="fa fa-plus"></i></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>{{ __('general.logo') }}:</label>
                    <a href="" id="thumb-image-logo" data-toggle="image" class="img-thumbnail">
                        <img src="{{$top_logo_thumb}}" alt="" title="" data-placeholder="test">
                    </a>
                    <input type="hidden" name="top_logo" value="{{$top_logo}}" id="input-image-logo">
                </div>
                <div class="form-group">
                    <label>{{ __('general.logo_adaptive') }}:</label>
                    <a href="" id="thumb-image-logo-adaptive" data-toggle="image" class="img-thumbnail">
                        <img src="{{$top_logo_thumb_adaptive}}" alt="" title="" data-placeholder="test">
                    </a>
                    <input type="hidden" name="top_logo_adaptive" value="{{$top_logo_adaptive}}" id="input-image-logo-adaptive">
                </div>
              </div>
        </div>
    </div>
</div>
{{--
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('realEstates.index')}}">{{ __('general.slider') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}

        </div>
    </div>
    <div class="box-body">
        <select class="real-estates-select form-control" name="show_on_home_slider[]" multiple="multiple">
            @foreach($realEstates as $realEstate)
                <option @if($realEstate['show_on_home_slider']) selected @endif value="{{$realEstate->id}}">{{$realEstate->title}}</option>
            @endforeach
        </select>
    </div>
</div>--}}
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('attributes.index')}}">{{ __('general.banner') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="col-sm-6">
            <table id="main_banner" class="table table-hover table-striped table-bordered table-condensed">
                <tr>
                    <td>
                        Object
                    </td>
                    <td>
                        Mobile image
                    </td>
                    <td>

                    </td>
                </tr>
                    @if(isset($banneritems) && !empty($banneritems))
                    @foreach($banneritems as $banner_key => $banner_item)
                    <tr> 
                        <td>
                            <select name="banner_items[{{$banner_key}}][type]"  class="form-control banner_item_changer">
                                <option @if($banner_item['type'] == 'video') selected @endif value="video">Video</option>
                                <option @if($banner_item['type'] == 'image') selected @endif value="image">Image</option>
                            </select>
                            <div class="slider-item__video" @if($banner_item['type'] == 'image') style="display:none;" @endif>
                                <div class="form-group">
                                    <label>Video file:</label>
                                    @if(isset($banner_item['video']) && $banner_item['video'] != 'image')
                                    <video width="300" height="150">
                                        <source src="{{asset('storage/'.str_replace('public/','',$banner_item['video']))}}#t=0.1" type="video/mp4" />
                                    </video>
                                    <input type="hidden" value="{{$banner_item['video']}}" name="banner_items[{{$banner_key}}][video]" />
                                    @endif


                                    <input type="file" value="@if(isset($banner_item['video'])){{$banner_item['video']}}@endif" name="banner_items[{{$banner_key}}][video]"  />
                                 </div>
                            </div>
                            <div class="slider-item__image"  @if($banner_item['type'] == 'video') style="display:none;" @endif  >
                                <div class="form-group text-center">
                                    <a href="" id="image-thumb-image-banner-id{{$banner_key}}" data-toggle="image" class="img-thumbnail">
                                        @php
                                            $image = \App\Models\ImageTool::resize($banner_item['image'],100,100);
                                        @endphp
                                        <img src="{{$image}}" alt="" title="" data-placeholder="test">
                                    </a>
                                    <input type="hidden" name="banner_items[{{$banner_key}}][image]" value="{{$banner_item['image']}}" id="image-input-image-banner-id{{$banner_key}}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group text-center">
                                <a href="" id="video-thumb-image-banner-id{{$banner_key}}" data-toggle="image" class="img-thumbnail">
                                    @php
                                        $thumb = \App\Models\ImageTool::resize($banner_item['thumb'],100,100);
                                    @endphp
                                    <img src="{{$thumb}}" alt="" title="" data-placeholder="test">
                                </a>
                                <input type="hidden" name="banner_items[{{$banner_key}}][thumb]" value="{{$banner_item['thumb']}}" id="video-input-image-banner-id{{$banner_key}}">
                            </div>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger remove_banner_item"><i class="fa fa-times"></i></button>
                        </td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td colspan="3" class="text-right"> <button type="button" id="add-new-banner-item" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('attributes.index')}}">{{ __('general.filter') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-5">
                <label>{{ __('general.checkbox') }}</label>
                <table class="table table-hover table-striped table-bordered table-condensed">
                    <tr>
                        <td><b>{{ __('general.name') }}</b></td>
                        <td><b>{{ __('general.value') }}</b></td>
                        <td></td>
                    </tr>
                    @foreach($attributes['checkboxes'] as $checkbox)
                        @if($checkbox['show_on_main'])
                        <tr>
                            <td>{{$checkbox->name}}</td>
                            <td><input  type="text" name="attributes_checkboxes[{{$checkbox->id}}]" value="@if(isset($homeattributes['checkboxes'][$checkbox->id])){{$homeattributes['checkboxes'][$checkbox->id]}}@endif" /></td>
                            <td><button data-attr-id="{{$checkbox->id}}" type="button" class="btn btn-danger remove_attr_button"><i class="fa fa-trash"></i></button></td>
                        </tr>
                        @endif
                    @endforeach
                    <tr>
                        <td colspan="2">
                            <select class="form-control attr_add_select">
                                @foreach($attributes['checkboxes'] as $checkbox)
                                    @if(!$checkbox['show_on_main'])
                                        <option value="{{$checkbox->id}}">{{$checkbox->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td class="td-action"><button class="btn btn-primary add_attr_button" type="button"><i class="fa fa-plus"></i></button></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>{{ __('general.slider') }}</label>
                <table class="table table-hover table-striped table-bordered table-condensed">
                    <tr>
                        <td><b>{{ __('general.name') }}</b></td>
                        <td><b>{{ __('general.value') }}</b></td>
                        <td></td>
                    </tr>
                    @foreach($attributes['slider'] as $slider)
                        @if($slider['show_on_main'])
                            <tr>
                                <td>{{$slider->name}}</td>
                                <td>
                                    <input type="text"  name="attributes_slider[{{$slider->id}}][min]" value="@if(isset($homeattributes['slider'][$slider->id]['min'])){{$homeattributes['slider'][$slider->id]['min']}}@endif" />
                                    <input type="text" name="attributes_slider[{{$slider->id}}][max]" value="@if(isset($homeattributes['slider'][$slider->id]['max'])){{$homeattributes['slider'][$slider->id]['max']}}@endif" />
                                </td>
                                <td><button data-attr-id="{{$slider->id}}" type="button" class="btn btn-danger remove_attr_button"><i class="fa fa-trash"></i></button></td>
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <td colspan="2">
                            <select class="form-control attr_add_select">
                                @foreach($attributes['slider'] as $slider)
                                    @if(!$slider['show_on_main'])
                                    <option value="{{$slider->id}}">{{$slider->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td class="td-action"><button class="btn btn-primary add_attr_button slider" type="button"><i class="fa fa-plus"></i></button></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-5">
                <label>{{ __('general.number') }}</label>
                <table class="table table-hover table-striped table-bordered table-condensed">
                    <tr>
                        <td><b>{{ __('general.name') }}</b></td>
                        <td><b>{{ __('general.value') }}</b></td>
                        <td></td>
                    </tr>
                    @foreach($attributes['number'] as $number)
                        @if($number['show_on_main'])
                            <tr>
                                <td>{{$number->name}}</td>
                                <td><input  type="text" name="attributes_number[{{$number->id}}]" value="@if(isset($homeattributes['number'][$number->id])){{$homeattributes['number'][$number->id]}}@endif" /></td>
                                <td><button data-attr-id="{{$number->id}}" type="button" class="btn btn-danger remove_attr_button"><i class="fa fa-trash"></i></button></td>
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <td colspan="2">
                            <select class="form-control attr_add_select">
                                @foreach($attributes['number'] as $number)
                                    @if(!$number['show_on_main'])
                                    <option value="{{$number->id}}">{{$number->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td class="td-action"><button class="btn btn-primary add_attr_button" type="button"><i class="fa fa-plus"></i></button></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('realEstates.index')}}">{{ __('general.recommended') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label>{{ __('general.real estates on home page') }}:</label>
            <select class="real-estates-select form-control" name="show_on_home_recommended[]" multiple="multiple">
                @foreach($realEstates as $realEstate)
                    <option @if($realEstate['show_on_home_recommended']) selected @endif value="{{$realEstate->id}}">{{$realEstate->title}}</option>
                @endforeach
            </select>
        </div>

    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('services.index')}}">{{ __('general.services') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">

        <div class="nav-tabs-custom nav-tabs-translation">
            <ul class="nav nav-tabs">
                @foreach ($all_locales as $locale)
                    @php
                        $tabHeaderClass = '';
                        if($page->availableTranslation($locale)) {
                            $tabHeaderClass = 'has-translation';
                        }
                        if ($errors->has('*-'.$locale)) {
                            $tabHeaderClass .= ' error-tab-header';
                        }
                        if ($loop->first) {
                            $tabHeaderClass .= ' active';
                        }
                    @endphp
                    <li class="{{ $tabHeaderClass }}">
                        <a href="#services_tab_{{ $loop->index }}" data-toggle="tab">
                            <i class="flag-icon flag-icon-{{$locale}}"></i>
                            <span class="error-tab-header-label">(ошибка)</span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($all_locales as $locale)
                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="services_tab_{{$loop->index}}">
                        <div class="form-group">
                            <label>{{ __('general.title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][service][title]" value="@isset($home_locales[$locale]['service']['title']) {{$home_locales[$locale]['service']['title']}} @endisset" type="text" />
                        </div>
                     </div>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            <label>{{ __('general.services') }}:</label>
            <select class="real-estates-select form-control" name="services_on_home[]" multiple="multiple">
                @foreach($services as $service)
                    <option @if($service['status']) selected @endif value="{{$service->id}}">{{$service->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('general.why_us') }}</h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom nav-tabs-translation">
            <ul class="nav nav-tabs">
                @foreach ($all_locales as $locale)
                    @php
                        $tabHeaderClass = '';
                        if($page->availableTranslation($locale)) {
                            $tabHeaderClass = 'has-translation';
                        }
                        if ($errors->has('*-'.$locale)) {
                            $tabHeaderClass .= ' error-tab-header';
                        }
                        if ($loop->first) {
                            $tabHeaderClass .= ' active';
                        }
                    @endphp
                    <li class="{{ $tabHeaderClass }}">
                        <a href="#why_us_tab_{{ $loop->index }}" data-toggle="tab">
                            <i class="flag-icon flag-icon-{{$locale}}"></i>
                            <span class="error-tab-header-label">(ошибка)</span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($all_locales as $locale)
                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="why_us_tab_{{$loop->index}}">
                        <div class="form-group">
                            <label>{{ __('general.title') }}</label>
                            <input class="form-control" name="home_locales[{{$locale}}][why_us][title]" value="@isset($home_locales[$locale]['why_us']['title']) {{$home_locales[$locale]['why_us']['title']}} @endisset" type="text" />
                        </div>
                        <div class="form-group">
                            <label>{{ __('general.subtitle') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][why_us][subtitle]" value="@isset($home_locales[$locale]['why_us']['title']) {{$home_locales[$locale]['why_us']['subtitle']}} @endisset" type="text" />
                        </div>
                        <div class="form-group">
                            <label>{{ __('general.description') }}:</label>
                            <textarea class="form-control" name="home_locales[{{$locale}}][why_us][description]" >
                                @isset($home_locales[$locale]['why_us']['title']) {{$home_locales[$locale]['why_us']['description']}} @endisset
                            </textarea>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('reviews.index')}}">{{ __('general.reviews') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom nav-tabs-translation">
            <ul class="nav nav-tabs">
                @foreach ($all_locales as $locale)
                    @php
                        $tabHeaderClass = '';
                        if($page->availableTranslation($locale)) {
                            $tabHeaderClass = 'has-translation';
                        }
                        if ($errors->has('*-'.$locale)) {
                            $tabHeaderClass .= ' error-tab-header';
                        }
                        if ($loop->first) {
                            $tabHeaderClass .= ' active';
                        }
                    @endphp
                    <li class="{{ $tabHeaderClass }}">
                        <a href="#review_tab_{{ $loop->index }}" data-toggle="tab">
                            <i class="flag-icon flag-icon-{{$locale}}"></i>
                            <span class="error-tab-header-label">(ошибка)</span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($all_locales as $locale)
                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="review_tab_{{$loop->index}}">
                        <div class="form-group">
                            <label>{{ __('general.title') }}</label>
                            <input class="form-control" name="home_locales[{{$locale}}][reviews][title]"
                                   value="@isset($home_locales[$locale]['reviews']['title']) {{$home_locales[$locale]['reviews']['title']}} @endisset" type="text" />
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <label>{{ __('general.reviews_on_home_page') }}:</label>
            <select class="reviews-select form-control" name="reviews_on_home[]" multiple="multiple">
                @foreach($reviews as $review)
                    <option @if($review['published']) selected @endif value="{{$review->id}}">{{$review->email}}({{$review->full_name }})</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('agents.index')}}">{{ __('general.agents') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom nav-tabs-translation">
            <ul class="nav nav-tabs">
                @foreach ($all_locales as $locale)
                    @php
                        $tabHeaderClass = '';
                        if($page->availableTranslation($locale)) {
                            $tabHeaderClass = 'has-translation';
                        }
                        if ($errors->has('*-'.$locale)) {
                            $tabHeaderClass .= ' error-tab-header';
                        }
                        if ($loop->first) {
                            $tabHeaderClass .= ' active';
                        }
                    @endphp
                    <li class="{{ $tabHeaderClass }}">
                        <a href="#agents_tab_{{ $loop->index }}" data-toggle="tab">
                            <i class="flag-icon flag-icon-{{$locale}}"></i>
                            <span class="error-tab-header-label">(ошибка)</span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($all_locales as $locale)
                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="agents_tab_{{$loop->index}}">
                        <div class="form-group">
                            <label>{{ __('general.title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][agents][title]" value="@isset($home_locales[$locale]['agents']['title']) {{$home_locales[$locale]['agents']['title']}} @endisset" type="text" />
                        </div>
                        <div class="form-group">
                            <label>{{ __('general.subtitle') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][agents][subtitle]" value="@isset($home_locales[$locale]['agents']['subtitle']) {{$home_locales[$locale]['agents']['subtitle']}} @endisset" type="text" />
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <label>{{ __('general.agents_on_home_page') }}:</label>
            <select class="reviews-select form-control" name="agents_on_home[]" multiple="multiple">
                @foreach($agents as $agent)
                    <option @if($agent['show_on_main']) selected @endif value="{{$agent->id}}">{{$agent->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('articles.index')}}">{{ __('general.latest_blog') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom nav-tabs-translation">
            <ul class="nav nav-tabs">
                @foreach ($all_locales as $locale)
                    @php
                        $tabHeaderClass = '';
                        if($page->availableTranslation($locale)) {
                            $tabHeaderClass = 'has-translation';
                        }
                        if ($errors->has('*-'.$locale)) {
                            $tabHeaderClass .= ' error-tab-header';
                        }
                        if ($loop->first) {
                            $tabHeaderClass .= ' active';
                        }
                    @endphp
                    <li class="{{ $tabHeaderClass }}">
                        <a href="#blog_tab_{{ $loop->index }}" data-toggle="tab">
                            <i class="flag-icon flag-icon-{{$locale}}"></i>
                            <span class="error-tab-header-label">(ошибка)</span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($all_locales as $locale)
                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="blog_tab_{{$loop->index}}">
                        <div class="form-group">
                            <label>{{ __('general.title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][blog][title]" value="@isset($home_locales[$locale]['blog']['title']) {{$home_locales[$locale]['blog']['title']}} @endisset" type="text" />
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('generalSettings.index')}}">{{ __('general.contact_form') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom nav-tabs-translation">
            <ul class="nav nav-tabs">
                @foreach ($all_locales as $locale)
                    @php
                        $tabHeaderClass = '';
                        if($page->availableTranslation($locale)) {
                            $tabHeaderClass = 'has-translation';
                        }
                        if ($errors->has('*-'.$locale)) {
                            $tabHeaderClass .= ' error-tab-header';
                        }
                        if ($loop->first) {
                            $tabHeaderClass .= ' active';
                        }
                    @endphp
                    <li class="{{ $tabHeaderClass }}">
                        <a href="#contact_form_tab_{{ $loop->index }}" data-toggle="tab">
                            <i class="flag-icon flag-icon-{{$locale}}"></i>
                            <span class="error-tab-header-label">(ошибка)</span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($all_locales as $locale)
                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="contact_form_tab_{{$loop->index}}">
                        <div class="form-group">
                            <label>{{ __('general.title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][contact_form][title]" value="@isset($home_locales[$locale]['contact_form']['title']) {{$home_locales[$locale]['contact_form']['title']}} @endisset" type="text" />
                        </div>
                        <div class="form-group">
                            <label>{{ __('general.title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][contact_form][subtitle]" value="@isset($home_locales[$locale]['contact_form']['subtitle']) {{$home_locales[$locale]['contact_form']['subtitle']}} @endisset" type="text" />
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ route('generalSettings.index')}}">{{ __('general.footer') }}</a></h3>
        <div class="pull-right">
            {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom nav-tabs-translation">
            <ul class="nav nav-tabs">
                @foreach ($all_locales as $locale)
                    @php
                        $tabHeaderClass = '';
                        if($page->availableTranslation($locale)) {
                            $tabHeaderClass = 'has-translation';
                        }
                        if ($errors->has('*-'.$locale)) {
                            $tabHeaderClass .= ' error-tab-header';
                        }
                        if ($loop->first) {
                            $tabHeaderClass .= ' active';
                        }
                    @endphp
                    <li class="{{ $tabHeaderClass }}">
                        <a href="#footer_tab_{{ $loop->index }}" data-toggle="tab">
                            <i class="flag-icon flag-icon-{{$locale}}"></i>
                            <span class="error-tab-header-label">(ошибка)</span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($all_locales as $locale)
                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="footer_tab_{{$loop->index}}">
                        <div class="form-group">
                            <label>{{ __('general.title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][footer][seo_title]" value="@isset($home_locales[$locale]['footer']['seo_title']) {{$home_locales[$locale]['footer']['seo_title']}} @endisset" type="text" />
                        </div>
                        <div class="form-group">
                            <label>{{ __('general.description') }}:</label>
                            <textarea class="form-control" name="home_locales[{{$locale}}][footer][seo_description]">
                                @isset($home_locales[$locale]['footer']['seo_description']) {{$home_locales[$locale]['footer']['seo_description']}} @endisset
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label>{{ __('general.blog_title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][footer][blog_title]" value="@isset($home_locales[$locale]['footer']['blog_title']) {{$home_locales[$locale]['footer']['blog_title']}} @endisset" type="text" />

                        </div>
                        <div class="form-group">
                            <label>{{ __('general.menu_title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][footer][menu_title]" value="@isset($home_locales[$locale]['footer']['menu_title']) {{$home_locales[$locale]['footer']['menu_title']}} @endisset" type="text" />

                        </div>
                        <div class="form-group">
                            <label>{{ __('general.contacts_title') }}:</label>
                            <input class="form-control" name="home_locales[{{$locale}}][footer][contact_title]" value="@isset($home_locales[$locale]['footer']['contact_title']) {{$home_locales[$locale]['footer']['contact_title']}} @endisset" type="text" />

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@push('js')
    <script>
        $(function() {
            //start from 0
            var banner_item_count = $('#main_banner').find('tr').length - 2;
            $('#add-new-banner-item').on('click',function () {
                var id = Math.random().toString(36).substring(7);
                var item = '    <tr>\n' +
                    '                    <td>\n' +
                    '                        <select name="banner_items['+banner_item_count+'][type]"  class="form-control banner_item_changer">\n' +
                    '                            <option value="video">Video</option>\n' +
                    '                            <option value="image">Image</option>\n' +
                    '                        </select>\n' +
                    '                        <div class="slider-item__video">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label>Video file:</label>\n' +
                    '                                <input type="file" name="banner_items['+banner_item_count+'][video]"  />\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                        <div class="slider-item__image" style="display:none;">\n' +
                    '                            <div class="form-group text-center">\n' +
                    '                                <a href="" id="image-thumb-image-banner-'+id+'" data-toggle="image" class="img-thumbnail">\n' +
                    '                                    <img src="/storage/images/no-image-box.png" alt="" title="" data-placeholder="test">\n' +
                    '                                </a>\n' +
                    '                                <input type="hidden" name="banner_items['+banner_item_count+'][image]" value="" id="image-input-image-banner-'+id+'">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                    </td>\n' +
                    '                    <td>\n' +
                    '                        <div class="form-group text-center">\n' +
                    '                            <a href="" id="video-thumb-image-banner-'+id+'" data-toggle="image" class="img-thumbnail">\n' +
                    '                                <img src="/storage/images/no-image-box.png" alt="" title="" data-placeholder="test">\n' +
                    '                            </a>\n' +
                    '                            <input type="hidden" name="banner_items['+banner_item_count+'][thumb]" value="" id="video-input-image-banner-'+id+'">\n' +
                    '                        </div>\n' +
                    '                    </td>\n' +
                    '                    <td>\n' +
                    '                        <button type="button" class="btn btn-danger remove_banner_item"><i class="fa fa-times"></i></button>\n' +
                    '                    </td>\n' +
                    '                </tr>';
                $('#main_banner').find('tr:last-child').before(item);
                banner_item_count++;
            })

            $('body').on('change', '.banner_item_changer', function () {
                var val = $(this).val();
                var self = $(this);
                $(this).parent().find('>div').css('display','none');
                console.log(val);
                switch(val){
                    case 'video':
                        self.parent().find('.slider-item__video').css('display','block');
                        break;
                    case 'image':
                        self.parent().find('.slider-item__image').css('display','block');
                        break;
                }

            })
            
            $('.real-estates-select').select2({
                placeholder: '{{__('general.choose')}}',
                tags: true,
                width: 'resolve',
            });
            $('.reviews-select').select2({
                placeholder: '{{__('general.choose')}}',
                tags: true,
                width: 'resolve',
            });
            $('.attr_add_select').select2({
                placeholder: '{{__('general.choose')}}',
                tags: true,
                width: 'resolve',
            });
            
            //add attribute
            $('.add_attr_button').on('click',function (e) {
                e.stopPropagation();
                var option = $(this).parent().parent().find('select option:selected');
                if(!option.length){
                    return ;
                }
                var content =
                    '<tr>\n' +
                        '<td>'+option.text()+'</td>\n' +
                        '<td><input  type="text" name="attributes_checkboxes['+option.val()+']" value="0"></td>\n' +
                        '<td><button data-attr-id="'+option.val()+'" type="button" class="btn btn-danger remove_attr_button"><i class="fa fa-trash"></i></button></td>\n' +
                    '</tr>';

                if($(this).hasClass('slider')){
                    content =
                        '<tr>\
                            <td>'+option.text()+'</td>\
                            <td>\
                                <input type="text" name="attributes_slider['+option.val()+'][min]" value="0">\
                                <input type="text" name="attributes_slider['+option.val()+'][max]" value="0">\
                            </td>\
                            <td><button data-attr-id="'+option.val()+'" type="button" class="btn btn-danger remove_attr_button"><i class="fa fa-trash"></i></button></td>\
                        </tr>';
                }
                $(this).closest('table').find('tr:last-child').before(content);
                //remove current attr from variants
                option.remove();
                $(this).parent().parent().find('select').change();
            });

            $('body').on('click','.remove_banner_item', function () {
                $(this).parent().parent().remove();
            });
            
            
            //remove attribute
            $(document).on('click','.remove_attr_button',function () {
                var select = $(this).closest('table').find('.attr_add_select');

                select.append(
                    '<option value="'+$(this).attr('data-attr-id')+'">'+$(this).parent().parent().find('td:first-child').text()+'</option>'
                );

                select.change();
                //remove this attr from table
                $(this).parent().parent().remove();
            });
            
            //add menu
            var menu_top_counter = $('#top_menu_table').find('tr').length -2;
            $('.menu_item_add').on('click',function () {
                var tr = '<tr>\
                            <td>\
                             @foreach($all_locales as $locale)
                                <div class="input-group">\
                                    <span class="input-group-addon"><i class=" flag-icon flag-icon-{{$locale}}"></i></span>\
                                    <input class="form-control" type="text" name="top_menu_items['+menu_top_counter+'][locales][{{$locale}}]" value="">\
                                </div>\
                            @endforeach
                            </td>\
                            <td>\
                                <input class="form-control" type="text" value="" name="top_menu_items['+menu_top_counter+'][url]" placeholder="Menu item url" />\
                            </td>\
                            <td>\
                            <button class="btn btn-danger menu_item_remove" type="button"><i class="fa fa-trash"></i></button>\
                            </td>\
                        </tr>';
                menu_top_counter++;
                $(this).parent().parent().before(tr);
            })
            
            //remove menu
            
            $(document).on('click','.menu_item_remove',function () {
                $(this).parent().parent().remove();
            })
        });

        $('textarea').ckeditor({
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
        });
    </script>
@endpush
