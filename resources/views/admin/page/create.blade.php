@extends('admin.layouts.admin-lte')

@section('title', __('general.page'))
@section('content-title', __('general.page'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('pages.index') }}"><i class="fa fa-file"></i> {{ __('general.pages') }}</a></li>
        <li class="active"><i class="fa fa-file"></i> {{ __('general.new') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => 'pages.store','method' => 'post',]) !!}


                    @include('admin.page.partials.form')

                    {!! Form::hidden('created_by', Auth::id()) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
