@extends('admin.layouts.admin-lte')

@section('title', __('general.articles'))
@section('content-title', __('general.articles'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('articles.index') }}"><i class="fa fa-newspaper-o"></i> {{ __('general.articles') }}</a></li>
        <li class="active"><i class="fa fa-newspaper-o"></i> {{ __('general.new') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => 'articles.store','method' => 'post','files' => true]) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.article_creating') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}

                        <a href="{{ route('articles.index') }}" class="btn btn-default"><i class="fa fa-share"></i></a>
                    </div>
                </div>
                <div class="box-body">

                    @include('admin.article.partials.form')

                    {!! Form::hidden('created_by', Auth::id()) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
