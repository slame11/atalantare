<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('image_big') ? 'has-error' : ''}}">
            {!! Form::label('thumb-big-image-photo', __('general.image_big')) !!}
            <a href="" id="thumb-big-image-photo" data-toggle="image" class="img-thumbnail">
                <img src="{{ $article->image_big != '' ? $article->thumb_big : Storage::url('public/images/no-image-box.png') }}" alt="" title="" data-placeholder="test" />
            </a>
            <input type="hidden" name="image_big" value="{{$article->image_big}}" id="input-image-big-photo" />
            {!! $errors->first('image_big', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
            {!! Form::label('slug', __('general.slug')) !!}
            {!! Form::text('slug',$slug->slug ?? 'slug',['class' => 'form-control','placeholder' => __('general.slug'),'size' => 255]) !!}
            {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
            {!! Form::label('input-image-photo', __('general.image')) !!}
            <a href="" id="thumb-image-photo" data-toggle="image" class="img-thumbnail">
                <img src="{{ $article->image != '' ? $article->thumb : Storage::url('public/images/no-image-box.png') }}" alt="" title="" data-placeholder="test" />
            </a>
            <input type="hidden" name="image" value="{{$article->image}}" id="input-image-photo" />
            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="checkbox {{ $errors->has('image_show') ? 'has-error' : ''}}">
            <label>
                {!! Form::checkbox('image_show', 1, $article->image_show ?? false) !!}
                {{ __('general.image_show') }}
                {!! $errors->first('image_show', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('viewed', __('general.viewed')) !!}
            {!! Form::number('viewed',$article->viewed ?? 0,['class' => 'form-control', 'min'=>0]) !!}
            {!! $errors->first('viewed', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="checkbox {{ $errors->has('published') ? 'has-error' : ''}}">
            <label>
                {!! Form::checkbox('published', 1, $article->published ?? false) !!}
                {{ __('general.published') }}
                {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
        <div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
            {!! Form::label('date', __('general.date')) !!}
            {!! Form::date('date',$article->date ?? null,['class' => 'form-control','placeholder' => __('general.date')]) !!}
            {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

@include('admin.article.partials.translations')

@push('js')
    <script>
        $('textarea.article-textarea').ckeditor({
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
        });
    </script>
@endpush