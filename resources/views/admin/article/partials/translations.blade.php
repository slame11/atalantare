<div class="nav-tabs-custom nav-tabs-translation">
    <ul class="nav nav-tabs">
        @foreach ($locales as $locale)
            @php
                $tabHeaderClass = '';
                if($article->availableTranslation($locale['locale'])) {
                    $tabHeaderClass = 'has-translation';
                }
                if ($errors->has('*-'.$locale['locale'])) {
                    $tabHeaderClass .= ' error-tab-header';
                }
                if ($loop->first) {
                    $tabHeaderClass .= ' active';
                }
            @endphp
            <li class="{{ $tabHeaderClass }}">
                <a href="#tab_{{ $loop->index }}" data-toggle="tab">
                    {{ __('general.translation_language.'.$locale['locale']) }}
                    <span class="error-tab-header-label">{{ __('general.error-validation') }}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($locales as $locale)
            <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="tab_{{$loop->index}}">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox(
                                    'available_translation-'.$locale['locale'],
                                    1,
                                    $article->availableTranslation($locale['locale']),
                                    [
                                    'class'=>'tab-switch',
                                    'data-controltab'=>'langtab-'.$locale['locale'],
                                    ]
                                ) !!}
                                {{ __('general.activate_translation.'.$locale['locale']) }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row langtab-{{$locale['locale']}}
                {{($article->availableTranslation($locale['locale']) || old('available_translation-'.$locale['locale'])) ? '' : 'inactive-tab'}}">
                    <div class="col-sm-12">
                        <div class="form-group {{ $errors->has('name-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('name-'.$locale['locale'], __('general.name', [], $locale['locale'])) !!}
                            {!! Form::text('name-'.$locale['locale'],array_key_exists('name',$locale)?$locale['name']:null,['class' => 'form-control','placeholder' => __('general.title', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('name-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('description_short-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('description_short-'.$locale['locale'], __('general.description_short', [], $locale['locale'])) !!}
                            {!! Form::text('description_short-'.$locale['locale'],array_key_exists('description_short',$locale)?$locale['description_short']:null,['class' => 'form-control','placeholder' => __('general.description_short', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('description_short-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('description-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('description-'.$locale['locale'], __('general.description', [], $locale['locale'])) !!}
                            {!! Form::textarea('description-'.$locale['locale'],array_key_exists('description',$locale)?$locale['description']:null,['class' => 'form-control article-textarea','rows'=>'8','placeholder' => __('general.description', [], $locale['locale']),]) !!}
                            {!! $errors->first('description-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <hr>
                        <div class="form-group {{ $errors->has('meta_title-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('meta_title-'.$locale['locale'], __('general.meta_title', [], $locale['locale'])) !!}
                            {!! Form::text('meta_title-'.$locale['locale'],array_key_exists('meta_title',$locale)?$locale['meta_title']:null,['class' => 'form-control','placeholder' => __('general.meta_title', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('meta_title-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('meta_description-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('meta_description-'.$locale['locale'], __('general.meta_description', [], $locale['locale'])) !!}
                            {!! Form::text('meta_description-'.$locale['locale'],array_key_exists('meta_description',$locale)?$locale['meta_description']:null,['class' => 'form-control','placeholder' => __('general.meta_description', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('meta_description-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('meta_keyword-'.$locale['locale']) ? 'has-error' : ''}}">
                            {!! Form::label('meta_keyword-'.$locale['locale'], __('general.meta_description', [], $locale['locale'])) !!}
                            {!! Form::text('meta_keyword-'.$locale['locale'],array_key_exists('meta_keyword',$locale)?$locale['meta_keyword']:null,['class' => 'form-control','placeholder' => __('general.meta_keyword', [], $locale['locale']),'size' => 255]) !!}
                            {!! $errors->first('meta_keyword-'.$locale['locale'], '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
