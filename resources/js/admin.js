var Admin_custom = {
    attributePlaceHolder: function () {
        $('body').on('click','.placeholder_dropdown-item',function () {
            var data = $(this).data();
            $(this).parent().parent().find('input').val($(this).text());
            $(this).parent().parent().find('input').attr('data-id',data.id);
            $(this).parent().parent().find('input').attr('data-type',data.attribute_type_id);
            $(this).parent().remove();
        });

        $('body').on('click','.remove_attr_item',function () {
            $(this).parent().parent().remove();

            var name  = $(this).parent().parent().find('td:first-child').text();
            var id = $(this).parent().parent().find('input').attr('name');
            id = id.replace(/\D/g, '');
            var type = $(this).parent().parent().find('input').attr('data-type');
            $('#attribute-search').append('<option value="'+id+'" data-type="'+type+'">'+name+'</option>');
            $("#attribute-search").select2("destroy").select2();
        });

        $('#add_atribute').on('click',function () {
            var type = $(this).parent().parent().find('select option:selected').attr('data-type');
            var id = $(this).parent().parent().find('select option:selected').val();
            var name = $(this).parent().parent().find('select option:selected').text();
            switch(type){
                case 'SLIDER':
                    $('.resultAttributes tr:last-child').after(
                        '<tr>' +
                            '<td>' + name
                        +'</td>' +
                        '<td><input class="form-control" data-type="'+type+'" type="text" name="attribute_id['+id+']" value="" /></td>'+
                        '<td><buton type="button" class="btn btn-danger remove_attr_item"><i class="fa fa-trash"></i></buton></td>');
                    break;
                case 'NUMBER':
                    $('.resultAttributes tr:last-child').after(
                        '<tr>' +
                        '<td>' + name
                        +'</td>' +
                        '<td><input class="form-control" data-type="'+type+'"  type="text" name="attribute_id['+id+']" value="" /></td>'+
                        '<td><buton type="button" class="btn btn-danger remove_attr_item"><i class="fa fa-trash"></i></buton></td>');
                    break;
                case 'CHECKBOX':
                    $('.resultAttributes tr:last-child').after(
                        '<tr>' +
                        '<td>' + name
                        +'</td>' +
                        '<td><input data-type="'+type+'"  type="hidden" name="attribute_id['+id+']" value="1" /></td>'+
                        '<td><buton type="button" class="btn btn-danger remove_attr_item"><i class="fa fa-trash"></i></buton></td>');
                    break;
            }
            $('#attribute-search').find('option:selected').remove();
            $("#attribute-search").select2("destroy").select2();
        });

        $('.attribute_placeholder').keypress( function () {
            var self = $(this);
          $.get( $(this).data('ajax'),{query: $(this).val()}, function( data ) {
              self.parent().find('.placeholder_dropdown').remove();
              self.parent().append(
                  '<div class="placeholder_dropdown">'+
                  '</div>'
              );

              $.each(data,function (key,val) {
                  console.log('val',val);
                  self.parent().find('.placeholder_dropdown').append(
                      '<div data-id="'+val.id+'" data-attribute_type_id="'+val.attribute_type.attribute_name.toLowerCase()+'" class="placeholder_dropdown-item">'
                      +val.name+
                      '</div>'
                  );
              });
          },'json');
        });
    },
    init: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        this.attributePlaceHolder();

    },

};

$(document).ready(function () {
    Admin_custom.init();
    $('input[name="price_upon_request"]').on('change', function () {
        if($(this).prop('checked')) {
            $('input[name="price"]').prop('disabled',true).prop('required',false);
            $('select[name="currency"]').prop('disabled',true).prop('required',false);
        }
        else {
            $('input[name="price"]').prop('disabled',false).prop('required',true);
            $('select[name="currency"]').prop('disabled',false).prop('required',true);
        }
    });
});
