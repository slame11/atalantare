
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('gijgo/js/gijgo');
require('./ddselect');
require('owl.carousel');
require('./fotorama/fotorama');
require('bootstrap-select');

window.noUiSlider = require('nouislider');
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});


var Main = {

    banner_interval : {},
    interval_time : 8000,
    initLangSelect: function () {
        function number_format (number, decimals, dec_point, thousands_sep) {
            // Strip all characters but numerical ones.
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ' ' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }
        // Crutch
        let crutch = 1;
        $('select:not(.form-control):not(#currency_select,#country_filter,#category_filter, #transactions_filter, select[name=sort])').ddslick(
            {
                onSelected: function(data){
                    if($(data.selectedItem).closest('#lanugage_select-dd-placeholder').length > 0){
                        if(crutch >= 2){
                            location.href = data.selectedData.link;
                        }else{
                            crutch++;
                        }
                    }
                    if($(data.selectedItem).closest('#currency_select-dd-placeholder').length > 0){
                        var price = parseInt($('.product_card_header_price_wrapper >span').text());
                        var new_price = parseInt(data.selectedData.price);
                        if(crutch > 2) {
                            if(price > new_price){
                                price = new_price + 100;
                                var inv = setInterval(function() {
                                    if(price > new_price){
                                        price--;
                                        $('.product_card_header_price_wrapper-price span:first-child').html(number_format(price,0,',',','));
                                    }else{
                                        clearInterval(inv);
                                    }
                                }, 1);
                            }else{
                                price = new_price - 100;
                                var inv = setInterval(function() {
                                    if(price < new_price){
                                        price++;
                                        $('.product_card_header_price_wrapper-price span:first-child').html(number_format(price,0,',',','));
                                    }else{
                                        clearInterval(inv);
                                    }
                                }, 1);
                            }
                        }else{
                            crutch++;
                        }
                    }
                },
                embedCSS: false}
        );
    },

    initTypeSelect: function(){
        $.fn.selectpicker.Constructor.BootstrapVersion = '4';

       // $('#category_filter').selectpicker({selectedTextFormat:'values'});
    },

    setBannerInterval: function(){
        var self = this;
        clearInterval(this.banner_interval);
        this.banner_interval = setInterval(function () {
            var scrolltop = $('body').scrollTop();
            if( $('.navbar-toggler').hasClass('collapsed')){
                if($('.banner-content__item.active').is(':last-child')){
                    self.chooseBannerItem($('.banner-content__item:first-child')[0]);
                }else{
                    self.chooseBannerItem($('.banner-content__item.active').next()[0]);
                }
            }
        },self.interval_time)
    },


    chooseBannerItem: function(index){
        // var Main = this;
        // if($(self).hasClass('active')){
        //     return ;
        // }
        // var item = $(self).data('item');
        $('.banner-backgrounds__items').find('>div').removeClass('active');
        $('.banner-backgrounds__items').find('[data-item='+index+']').addClass('active');
        // Main.setBannerInterval();
        // var big_descr = $('.banner-content__item.active').find('.banner-content__item-text__description-big');
        // big_descr.hide();
        // setTimeout(function () {
        //     big_descr.show();
        // },800)
        // $('.banner-content__item').removeClass('active');
        // $(self).addClass('active');

        $('.banner-backgrounds__items').trigger('to.owl.carousel', [index]);
    },

    initBanner: function () {
        var self = this;

    /*    var start_item = $('.banner-content__item.active').data('item');
        $('.banner-backgrounds__items').trigger('to.owl.carousel', [start_item]);
*/
        $(document).on('click','.banner-content__item', function () {
            self.chooseBannerItem(parseInt($(this).data('item')) -1);
        });

        // document.addEventListener('scroll', function (event) {
        //     if($(document).scrollTop() > 120){
        //         clearInterval(self.banner_interval);
        //     }else{
        //         self.setBannerInterval();
        //     }
        // }, true /*Capture event*/);
        // this.setBannerInterval();

    },

    initOwl: function(){
        //BANNER
        var owl = $('#banner .owl-carousel');
        function isMobile(width) {
            if(width == undefined){
                width = 719;
            }
            if(window.innerWidth <= width) {
                return true;
            } else {
                return false;
            }
        }
        owl.owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots:false,
            items:1,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:false
        })

        if(!isMobile()){
            owl.on('translate.owl.carousel',function(e){
                $('.owl-item video').each(function(){
                    $(this).get(0).pause();
                });
            });
            owl.on('translated.owl.carousel',function(e){
                if($('.owl-item.active video').length > 0){
                    $('.owl-item.active video').get(0).play();
                }
            })
            $('.owl-item .item').each(function(){
                var attr = $(this).attr('data-videosrc');
                if (typeof attr !== typeof undefined && attr !== false) {
                    console.log('hit');
                    var videosrc = $(this).attr('data-videosrc');
                    $(this).prepend('<video muted><source src="'+videosrc+'" type="video/mp4"></video>');
                }
            });
            $('.owl-item.active video').attr('autoplay',true).attr('loop',true);
        }

        //BANNER
        var owl = $('.section_happy_clients_slider_wrapper_carousel');
        owl.owlCarousel({
            loop:true,
            margin:60,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });

        owl = $('#home_product_carousel');

        owl.owlCarousel({
            loop:true,
            margin:20,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });

        var owl = $('.banner-backgrounds__items');
        var self = this;

        owl.on('changed.owl.carousel', function(e) {
            self.setOwlPagination(e);
        }).on('initialized.owl.carousel',function(e){
            self.InitOwlPagination(e);
        });

        owl.owlCarousel({
            loop:true,
            margin:60,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    },
    InitOwlPagination:function(e){
        ($(e.target).trigger('to.owl.carousel', [1]));

        if (e.item) {
            var index = e.item.index - 1;
            var count = e.item.count;
            if (index > count) {
                index -= count;
            }
            if (index <= 0) {
                index += count;
            }
        }

        $('.banner-content').find('>div').removeClass('active');
        $('.banner-content').find('[data-item='+index+']').addClass('active');
    },
    setOwlPagination: function(e){

        if (!e.namespace || e.property.name != 'position') return ;

        if (e.item) {
            var index = e.item.index - 1;
            var count = e.item.count;
            if (index > count) {
                index -= count;
            }
            if (index <= 0) {
                index += count;
            }
        }
        $('.banner-content').find('>div').removeClass('active');
        $('.banner-content').find('[data-item='+index+']').addClass('active');
    },
    mainFilter: function () {
        // datepicker
        $('#date_filter').datepicker({
            uiLibrary: 'bootstrap4',
            icons :{
                rightIcon: '<img style="display:inline-block;" src="/storage/images/calendar.png" />'
            },
        });
        var self = this;

        // plus/minus
        $('.home_catalog-filter').on('click','.btn_plus',function () {
            var input = $(this).parent().find('input');
            input.val(parseInt(input.val()) + 1);
        }).on('click','.btn_minus',function () {
            var input = $(this).parent().find('input');
            var new_val = parseInt(input.val()) - 1;
            input.val(new_val > 0 ? new_val : 1);
        });

        $('input[name=title]').on('change',function () {
            self.refreshCatalogContent();
        })

        // sliders
        $.each($( ".filter-slider" ),function (key, val) {
            noUiSlider.create(val, {
                start: [parseInt($(val).data('selected-min')),parseInt($(val).data('selected-max'))],
                connect: true,
                // step: 1,
                // tooltips: true,
                range: {
                    'min': parseInt($(val).data('min')),
                    'max': parseInt($(val).data('max'))
                }
            });
            var timer;
            var crutch = 1;
            function formatNumber(num) {
                return parseInt(num).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
            }
            val.noUiSlider.on('update', function () {
                var values_event = val.noUiSlider.get();
                clearInterval(timer);
                var price1 = formatNumber(values_event[0]);
                var price2 = formatNumber(values_event[1]);
                var selected_country = $('input[name="country"]').parent().find('.country_link.active');
                var currency_sign = '€';
                if(selected_country.length > 0){
                    currency_sign=selected_country.data('currency');
                }

                $(val).parent().find('.home_catalog-filter__advanced-slider__min').text(price1 + ' ' + currency_sign);
                $(val).parent().find('.home_catalog-filter__advanced-slider__max').text(price2 + ' ' + currency_sign);

                $(val).parent().find('input.min_value').val(values_event[0]);
                $(val).parent().find('input.max_value').val(values_event[1]);
                if(crutch > 2){
                    timer = setTimeout(function () {
                        self.refreshCatalogContent();
                    }, 1000);
                }else{
                    crutch++;
                }

            });
            var values = val.noUiSlider.get();
            var currency_sign = '€';
            var selected_country = $('input[name="country"]').parent().find('.country_link.active');
            if(selected_country.length > 0){
                currency_sign= selected_country.data('currency');
            }
            var price1 = formatNumber(values[0]);
            var price2 = formatNumber(values[1]);
            $(val).parent().find('.home_catalog-filter__advanced-slider__min').text(price1+ ' ' + currency_sign);
            $(val).parent().find('.home_catalog-filter__advanced-slider__max').text(price2+ ' ' + currency_sign);
            $(val).parent().find('input.min_value').val(values[0]);
            $(val).parent().find('input.max_value').val(values[1]);
        });

        // Open/Close advanced search
        $('#advanced_search').on('click', function () {
            $('.home_catalog-filter').toggleClass('opened');
            $('input[name=advanced_search]').val($('input[name=advanced_search]').val() === "1" ? "0" : "1");
        });

        var ddData = [
        ];
        $.each($('#category_filter option'),function () {
            ddData.push(  {
                text: $(this).text(),
                value: $(this).val(),
            });
        })
        $('#category_filter').empty().ddslick({
            data: ddData,
            selectText: $('#category_filter').attr('placeholder'),
        });
    },
    contactForm: function () {
        $('.send_contact_form').on('click',function () {
            var form = $(this).closest('.contacts_block-content_block__contacts_wrapper-contact_form');
            console.log(form.find('#email').val());
            $.each(form.find('.form-group'),function(key,val){
                $(val).find('.form-control').removeClass('is-invalid');
                $(val).children('.invalid-feedback').remove();
            });
            var pageURL = $(location).attr("href");
            $.post('/feedback', {name: form.find('#name').val(), 'link_from' : pageURL, agent: form.find('input[name=agent]').val(),email: form.find('#email').val(), phone: form.find('#phone').val(), message: form.find('#message').val()}
            ).done(function (response) {
                form.find('#name, #email, #phone, #message').val('');
                $('#contact_form_popup').modal('hide');
                $('#contacts_block_modal').modal('show');
            }).fail(function (response) {
                let errors = response.responseJSON.errors;
                if(errors.name) {
                    form.find('#name').addClass('is-invalid');
                    form.find('#name').closest('.form-group').append('<div class="invalid-feedback"></div>');
                    form.find('#name').closest('.form-group').children('.invalid-feedback').text(errors.name);
                }
                if(errors.email) {
                    form.find('#email').addClass('is-invalid');
                    form.find('#email').closest('.form-group').append('<div class="invalid-feedback"></div>');
                    form.find('#email').closest('.form-group').children('.invalid-feedback').text(errors.email);
                }
                if(errors.phone) {
                    form.find('#phone').addClass('is-invalid');
                    form.find('#phone').closest('.form-group').append('<div class="invalid-feedback"></div>');
                    form.find('#phone').closest('.form-group').children('.invalid-feedback').text(errors.phone);
                }
                if(errors.message) {
                    form.find('#message').addClass('is-invalid');
                    form.find('#message').closest('.form-group').append('<div class="invalid-feedback"></div>');
                    form.find('#message').closest('.form-group').children('.invalid-feedback').text(errors.message);
                }
            });

        });
    },
    reviewForm: function(){
        $('#send_review_form').on('click',function(){

            $.each($('.form-group'),function(key,val){
                $(val).find('.form-control').removeClass('is-invalid');
                $(val).children('.invalid-feedback').remove();
            });

            $.post('/review', {nameReview: $('#nameReview').val(), emailReview: $('#emailReview').val(), review: $('#review').val()}
            ).done(function (response) {
                $('#happyClientReview').hide();
            }).fail(function (response) {
                let errors = response.responseJSON.errors;
                console.log(errors);
                if(errors.nameReview) {
                    $('#nameReview').addClass('is-invalid');
                    $('#nameReview').closest('.form-group').append('<div class="invalid-feedback"></div>');
                    $('#nameReview').closest('.form-group').children('.invalid-feedback').text(errors.name);
                }
                if(errors.emailReview) {
                    $('#emailReview').addClass('is-invalid');
                    $('#emailReview').closest('.form-group').append('<div class="invalid-feedback"></div>');
                    $('#emailReview').closest('.form-group').children('.invalid-feedback').text(errors.email);
                }
                if(errors.review) {
                    $('#review').addClass('is-invalid');
                    $('#review').closest('.form-group').append('<div class="invalid-feedback"></div>');
                    $('#review').closest('.form-group').children('.invalid-feedback').text(errors.message);
                }
            });
        });
    },
    initClickMore: function () {
        $('.agent_popup_button').on('click', function () {
            var agent_name = $(this).data('agent-name');
            $('#contact_form_popup').find('input[name=agent]').val(agent_name);
        });
        $('.product_card_asside_agents_item_more').on('click',function () {
            $(this).closest('.product_card_asside_agents_item').find('.product_card_asside_agents_item_phones').show();
            $(this).hide();
        });
    },
    changeProductView: function () {
        $('body').on('click', '[data-change-product-view]',function () {
            var type = $(this).data('change-product-view');
            $(this).parent().find('button.active').removeClass('active');
            $(this).addClass('active');
            $('.catalog_page-content__products-products').attr('data-view',type);

            $.post('/catalog/setproductview', {product_view_type: type}
            ).done(function (response) {
                console.log(response);
            }).fail(function (response) {
                console.log(response);
            });
        });
    },
    initContactBlockMap: function () {

        if($('#contact_block_map').length > 0){
            var coords_latitude = $('.contacts-block-coords-latitude').data('contacts-block-coords');
            var coords_longitude = $('.contacts-block-coords-longitude').data('contacts-block-coords');
            var title = $('.contacts-block-address').data('contacts-block-address');

            var mymap = L.map('contact_block_map').setView([coords_latitude, coords_longitude], 18);
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(mymap);

            var marker = L.marker([coords_latitude, coords_longitude]).addTo(mymap);
            marker.bindPopup(title).openPopup();
        }
    },
    initProductPageMap: function () {

        if($('#product_card_map').length > 0){
            var coordsArray = $('.product_cart_coords').data('coords');

            if(coordsArray){
                var coords = coordsArray.split(" ");
                var title = $('.product_cart_title').data('title');
                var address = $('.product_cart_address').data('address');

                var product_map = L.map('product_card_map').setView([coords[0], coords[1]], 18);
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(product_map);

                var marker = L.marker([coords[0], coords[1]]).addTo(product_map);
                marker.bindPopup(title + '<br />' + address).openPopup();
            }
        }
    },
    changeCategorySort: function () {
        var self = this;
        let crutch = 0;
      $('select[name=sort]').ddslick({
            onSelected: function(data){
                    if(crutch > 0){
                        if(data.selectedData.value != "") {
                            if($('#filter_form').find('input[name=sort]').length === 0){
                                $('#filter_form').prepend('<input type="hidden" name="sort" />');
                            }
                            $('#filter_form').find('input[name=sort]').val(data.selectedData.value);
                        }else{
                            $('#filter_form').find('input[name=sort]').remove();
                        }
                        self.refreshCatalogContent();
                    }else{
                        crutch++;
                    }

            }
        });
    },
    openAttribtesInCatalogFilter: function(){
        $('.catalog_page-content__filter-content-attributes__header').on('click', function() {
           $('.catalog_page-content__filter-content-attributes__content').slideToggle('slow');
            $('.catalog_page-content__filter-content-attributes__content').toggleClass('is-closed');
            $(this).toggleClass('is-closed');
        });
    },
    initHeader: function () {
        if(!$('#app').hasClass('p-85')){
            var scroll = $(window).scrollTop();

            if (scroll >= 5){
                $('.header').removeClass('transparent_header');
            }else{
                $('.header').addClass('transparent_header');
            }
            $(window).scroll(function(){
                var scroll = $(window).scrollTop();

                if (scroll >= 5){
                    $('.header').removeClass('transparent_header');
                }else{
                    if($('#menu_toggle_button').hasClass('collapsed')){
                        $('.header').addClass('transparent_header');
                    }
                }
            });
            $('#menu_toggle_button').on('click',function () {
                var scroll = $(window).scrollTop();
                var header =  $('.header');
                if (scroll <= 5){
                    if(header.hasClass('transparent_header')){
                        header.removeClass('transparent_header');
                    }else{
                        header.addClass('transparent_header');
                    }
                }

            })
        }},

    /**
     * ajax refresh realestates in catalog
     */
    refreshCatalogContent: function () {
        let self = this;
        $('.catalog_page-content__loader_wrapper').addClass('show');
        var page = 1;
        if($(".page-item.active").length > 0){
            page = $(".page-item.active").find('a').text();
        }
        $.post( '/catalog/getproducts',$('#filter_form').serialize()+'&page='+page, function( data ) {
            $('.catalog_page-content__products').html(data);
            self.changeCategorySort();
            $('.catalog_page-content__loader_wrapper').removeClass('show');
            var param = '?' + $('#filter_form input:not([name=country])').serialize() + '&page=' + page;
            var country_str = '';
            if($('#filter_form input[name=country]').val() > 0){
                country_str ='/' +  $('a.country_link[data-value="'+parseInt($('#filter_form input[name=country]').val())+'"]').attr('data-seo-url');
            }
            window.history.pushState({href: "/catalog"}, 'title 2', "/catalog"+country_str + param);
        },'html');
    },

    /**
     * Filter in catalog page
     */
    initCatalogMenu: function () {

        let self = this;

        //pagination

        $('body').on('click','ul.pagination a',function (e) {
            e.stopPropagation();
            e.preventDefault();
            $('ul.pagination').find('.active').removeClass('active');
            $(this).parent().addClass('active');
            self.refreshCatalogContent();
            $('html,body').animate({ scrollTop: 0 }, 'slow');
        })

        //close dropdown
        $('body').on('click', function (e) {
            if($('.catalog_page-filter__menu-item__dropdown.open').length > 0){
                if($(event.target).closest('.catalog_page-filter__menu').length === 0){
                  $('.catalog_page-filter__menu-item__dropdown').removeClass('open');
                }
            }
        });

        //open dropdown
      $('.catalog_page-filter__menu-item__toggler').on('click',function () {
          if(!$(this).is($('.catalog_page-filter__menu-item__dropdown.open'))){
              $('.catalog_page-filter__menu-item__dropdown').removeClass('open');
              $('.catalog_page-filter__menu-item__toggler').removeClass('up').addClass('down');
          }
         $(this).parent().find('.catalog_page-filter__menu-item__dropdown').addClass('open');
          $(this).removeClass('down').addClass('up');
      });

      $('.custom_checkbox').on('change','input', function(){
            self.refreshCatalogContent();
      });

      //mobile hide filter
        $('.open_mobile_filter').on('click',function () {
            $('.catalog_page-filter_container').slideToggle();
        })

      //set value and text key
        $('body').on('click','.catalog_page-filter__menu-item__dropdown-item',function (e) {
            e.stopPropagation();
            e.preventDefault();
            if(parseInt($(this).attr('data-disabled')) === 1){
                return;
            }
            $(this).parent().parent().find('input[type=hidden]').val($(this).data('value'));
            $(this).parent().parent().find('.catalog_page-filter__menu-item__toggler').html($(this).html());
            $(this).parent().removeClass('open');

            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');

            var name= $(this).parent().parent().find('input[type=hidden]').attr('name');
            switch(name){
                case "country":
                    $('.catalog_page-filter__menu-item input[name=city]').val(0);
                    $.post('/getcities', {country_id: $(this).data('value')}
                    ).done(function (response) {
                        var city_container = $('.catalog_page-filter__menu-item input[name=city]').parent();
                        city_container.find('.catalog_page-filter__menu-item__dropdown').empty();
                        $.each(response, function(index, value) {
                            city_container.find('.catalog_page-filter__menu-item__dropdown').append('<a href="#" class="catalog_page-filter__menu-item__dropdown-item" data-value="'+index+'">'+value+'</a>');
                        });
                        city_container.find('.catalog_page-filter__menu-item__toggler').text('City');

                    }).fail(function (response) {
                        console.log(response);
                    });

                    //set needed currency to price filter
                    //перезагшружает контент 2 раза
                    //$('#slider-price').get(0).noUiSlider.updateOptions({range: {'min': parseInt($('input[name="price[min]"]').val()),'max':parseInt($('input[name="price[max]"]').val())}});
                    function formatNumber(num) {
                        return parseInt(num).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                    }
                    $('.home_catalog-filter__advanced-slider__min').text(formatNumber(parseInt($('input[name="price[min]"]').val())) + $(this).data('currency'));
                    $('.home_catalog-filter__advanced-slider__max').text(formatNumber(parseInt($('input[name="price[max]"]').val())) + $(this).data('currency'));
                    break;
            }

            self.refreshCatalogContent();
        })
    },
    init: function () {
        this.initHeader();
        this.initLangSelect();
        this.initBanner();
        this.initTypeSelect();
        this.initOwl();
        this.mainFilter();
        this.contactForm();
        this.reviewForm();
        this.initClickMore();
        this.changeProductView();
        this.initContactBlockMap();
        this.initProductPageMap();

        this.changeCategorySort();

        this.openAttribtesInCatalogFilter();

        this.initCatalogMenu();
    }
};

$(function () {
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

    Main.init();
    var ddData = [
    ];
    $.each($('#country_filter option'),function () {
        ddData.push(  {
            text: $(this).text(),
            value: $(this).val(),
        });
    })
    $('#country_filter').empty().ddslick({
       // defaultSelectedIndex: $('#countryId').val(),
        data: ddData,
        selectText: $('#country_filter').attr('placeholder'),
        onSelected: function(data){
            if(data.selectedData.value != '' && data.selectedData.value != 0) {
                $.post('/getcities', {country_id: data.selectedData.value}
                ).done(function (response) {
                    $('#city_filter-dd-placeholder').ddslick('destroy');
                    $('#city_filter option').remove();

                    var city_Data = [
                    ];
                    $.each(response, function(index, value) {
                        city_Data.push(  {
                            text: value,
                            value: index,
                        });
                    });

                    $('#city_filter').ddslick({
                        data:city_Data,
                        selectText : $('#city_filter').attr('placeholder')
                    });
                }).fail(function (response) {
                    console.log(response);
                });
            }else{
                $('#city_filter-dd-placeholder').ddslick('destroy');
                $('#city_filter option').slice(1).remove();
                $('#city_filter').ddslick();
            }
        }
    });
    $('#transactions_filter').ddslick({
        defaultSelectedIndex: $('#transactions_filter').val(),
        onSelected: function(data){
            $.post('/getcategories', {type_id: data.selectedData.value}
            ).done(function (response) {

                $('#category_filter-dd-placeholder').ddslick('destroy');
                $('#category_filter option').remove();
                var cat_Data = [
                ];
                $.each(response, function(index, value) {
                    cat_Data.push(  {
                        text: value,
                        value: index,
                    });
                });

                $('#category_filter').ddslick({
                    data:cat_Data,
                    selectText : $('#category_filter').attr('placeholder')
                });

            }).fail(function (response) {
                console.log(response);
            });
        }
    });

    $('#catalog-filter-type').on('change', function (e) {

        $.post('/getcategories', {type_id: $(this).val()}
        ).done(function (response) {
            var categories = '';
            $.each(response, function(index, value) {
                categories += '<label class="custom_checkbox text-uppercase"><input type="checkbox" name="category[]" value="'+index+'"> <span class="checkmark"></span>'+value+'\n' +
                '</label>';
            });
            $('.catalog_page-content__filter-content__checkboxes').empty().append(categories);
        }).fail(function (response) {
            console.log(response);
        });
    })

    if($('#countryId').length){
        $.post('/getcities', {country_id: $('#countryId').val()}
        ).done(function (response) {
            $.each(response, function(index, value) {
                $('#catalog-filter-city').append('<option value="'+index+'">'+value+'</option>');
            });

            if($('#cityId').length && $('#cityId').val() != 0){
                $("#catalog-filter-city").val($('#cityId').val());
            }
        }).fail(function (response) {
            console.log(response);
        });
    }

    $('#catalog-filter-country').on('change', function () {
        if($('#catalog-filter-country option:selected').val() == 0){
            $('#catalog-filter-city option').slice(1).remove();
            $('#catalog-filter-city').hide();
        }else {
            $('#catalog-filter-city option').slice(1).remove();
            $('#catalog-filter-city').show();

            $.post('/getcities', {country_id: $(this).val()}
            ).done(function (response) {
                $.each(response, function(index, value) {
                    $('#catalog-filter-city').append('<option value="'+index+'">'+value+'</option>');
                });
            }).fail(function (response) {
                console.log(response);
            });
        }
    });

    if($('#catalog-filter-country option:selected').val() == 0){
        $('#catalog-filter-city').hide();
    }else {
        $('#catalog-filter-city').show();
    }

    // Sent viewed for RealEstate
    if ($('#realEstateId').length && $('#realEstateViewed').length) {
        $.post('/realestate/viewed',{realEstateId : $('#realEstateId').val(),viewed : $('#realEstateViewed').val()}
        ).done(function(response) {
            console.log(response);
        }).fail(function(error) {
            console.log(error);
        });
    }

    //show filter if window resized
    $(window).resize(function () {
       if($(this).width() > 991){
            $('.custom-toogle-wrapper-content').show();
       }else{
           //$('.custom-toogle-wrapper-content').hide();
       }
    });
    $(window).resize();
    $('.custom-toogle-wrapper-header').click(function() {
        /*var content  = $(this).closest('.custom-toogle-wrapper').find('.custom-toogle-wrapper-content');
        if (content.is(":visible")){
            content.closest('.custom-toogle-wrapper').find('.custom-toogle-wrapper-header').removeClass("is-closed");
            content.removeClass("is-closed");
        }else{
            content.closest('.custom-toogle-wrapper').find('.custom-toogle-wrapper-header').addClass("is-closed");
            content.addClass("is-closed");
        }
        content.slideToggle();
        $.post('/catalog/setfiltertoggle', {filter_toggle: content.is(":visible") ? 0 : 1}
        ).done(function (response) {
            console.log(response);
        }).fail(function (response) {
            console.log(response);
        });*/
        $(this).closest('.custom-toogle-wrapper').find('.custom-toogle-wrapper-content').slideToggle('slow', function() {
            if ($(this).is(":visible")){
                $(this).closest('.custom-toogle-wrapper').find('.custom-toogle-wrapper-header').removeClass("is-closed");
                $(this).removeClass("is-closed");
            }else{
                $(this).closest('.custom-toogle-wrapper').find('.custom-toogle-wrapper-header').addClass("is-closed");
                $(this).addClass("is-closed");
            }
            $.post('/catalog/setfiltertoggle', {filter_toggle: $(this).is(":visible") ? 0 : 1}
            ).done(function (response) {
                console.log(response);
            }).fail(function (response) {
                console.log(response);
            });
        });

    });

    $(window).on('load resize', function () {
        if ($(window).width() < 992) {

            $('.contacts_block-content_block').append(function () {
                return $(this).find('.container #contact_map');
            });

        } else {

            $('.row > div:last-child').append(function () {
                return $(this).closest('.contacts_block-content_block').find('#contact_map');
            });

        }
    });
});
