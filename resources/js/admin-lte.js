try {
    window.$ = window.jQuery = require('jquery');
    require('admin-lte');
    require('bootstrap-sass');
    require('bootstrap-3-typeahead');
    require('select2');
    require('chartjs-color');
    require('dropzone');
} catch (e) {}

$(function () {
    let Chart = require('chart.js/dist/Chart.js');
    window.Dropzone = require('dropzone');

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

    $('.tab-switch').change(function () {
        if($(this).prop('checked')) {
            $('.'+$(this).data('controltab'))
                .removeClass('inactive-tab').find('input').prop('readonly', false);

        } else {
            $('.'+$(this).data('controltab'))
                .addClass('inactive-tab').find('input').prop('readonly', true);
        }
    });

    // Image Manager
    $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
        var $element = $(this);
        var $popover = $element.data('bs.popover'); // element has bs popover?

        e.preventDefault();

        // destroy all image popovers
        $('a[data-toggle="image"]').popover('destroy');

        // remove flickering (do not re-add popover when clicking for removal)
        if ($popover) {
            return;
        }

        $element.popover({
            html: true,
            placement: 'right',
            trigger: 'manual',
            content: function() {
                return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
            }
        });

        $element.popover('show');

        $('#button-image').on('click', function() {
            var $button = $(this);
            var $icon   = $button.find('> i');

            $('#modal-image').remove();

            $.ajax({
                url: '/admin/filemanager/show?target=' + $element.parent().find('input').attr('id') + '&thumb=' + $element.attr('id'),
                dataType: 'html',
                beforeSend: function() {
                    $button.prop('disabled', true);
                    if ($icon.length) {
                        $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                    }
                },
                complete: function() {
                    $button.prop('disabled', false);

                    if ($icon.length) {
                        $icon.attr('class', 'fa fa-pencil');
                    }
                },
                success: function(html) {
                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                    $('#modal-image').modal('show');
                }
            });

            $element.popover('destroy');
        });

        $('#button-clear').on('click', function() {
            $element.find('img').attr('src', $element.find('img').attr('data-placeholder'));

            $element.parent().find('input').val('');

            $element.popover('destroy');
        });
    });

    $( ".nav-tabs-general .nav-ul-general li" ).each( function( index, element ){

        var tab_id = $(element).find('a.general-tabs').attr('href');

        if($(tab_id).find('.help-block').length > 0){
            $(element).addClass('error-tab-header');
            $(element).find('.error-tab-header-label').show();
        }
    });

    // Dashboard
    let realEstates = $('#RealEstates');
    if(realEstates.length == 1) {
        realEstates = document.getElementById("RealEstates").getContext('2d');
        $.get('/admin/chart', function (response) {
            options = {};

            let realEstateChart = new Chart(realEstates, {
                type: 'pie',
                data: response,
                options: options
            });
        });
    }

    // realEstate rent period script
    $('.realEstateType').on('change',function () {
        if (this.value==2){
            $('.rentPeriod').show();
        } else {
            $('.rentPeriod').hide();
        }
    });

    if ($('.realEstateType').val() ==2){
        $('.rentPeriod').show();
    }
    else {
        $('.rentPeriod').hide();
    }

    if($('#multipleAddImg').length > 0){
        let myDropzone = new Dropzone("#multipleAddImg",{
            url:'/admin/upload',
            addRemoveLinks: true,
        });

        $('#multipleAddImg').addClass('dropzone');
        myDropzone.on("sending", function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
        });

        myDropzone.on('success', function(file, response){
            let key = $('#image_table tr').last().data('key');
            let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            let text = "";

            for (var i = 0; i < 10; i++){
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }

            key++;

            response = response.replace(/replacementKey/g, key);
            response = response.replace(/replacementRandomStr/g, text);
            $('#image_table').append(response);
            myDropzone.removeFile(file);
        });

    }



});
