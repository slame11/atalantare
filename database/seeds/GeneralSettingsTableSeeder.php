<?php

use Illuminate\Database\Seeder;

class GeneralSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\GeneralSetting::create(
            [
                'key' => "general_address",
                'value' => "256, 1st AVE, Manchester 125 , Noth England",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_phone_1",
                'value' => "+011 222 3333",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_phone_2",
                'value' => "+044 555 666",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_email",
                'value' => "info@example.com",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_site_url",
                'value' => "www.example.com",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_map_coords_latitude",
                'value' => "51.505",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_map_coords_longitude",
                'value' => "-0.09",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "home_filter_config",
                'value' => '{"checkboxes":{"1":"1","2":"0","5":"1","9":"1","12":"0"},"slider":{"13":{"min":"2","max":"4"},"14":{"min":"3","max":"6"}},"number":{"15":"2","18":"1"}}',
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "top_menu_items",
                'value' => '[{"locales":{"en":"Home","ru":"\u0413\u043b\u0430\u0432\u043d\u0430\u044f","ch":"Home","es":"Home"},"url":"\/"},{"locales":{"en":"About us","ru":"\u041e \u043d\u0430\u0441","ch":"About us","es":"About us"},"url":"\/about_us"},{"locales":{"en":"Contacts","ru":"\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b","ch":"Contacts","es":"Contacts"},"url":"\/contact"},{"locales":{"en":"Catalog","ru":"Каталог","ch":"Catalog","es":"Catalog"},"url":"/catalog"}]',
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "home_locales",
                'value' => '{"en":{"service":{"title":"Service"},"why_us":{"title":"Why us","subtitle":"DOM-INNO is the best theme for elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed                                 do eiusmod tempor incididunt ut labore et lorna aliquatd minim veniam, quis nostrud exercitation oris","description":"<div class=\"row\">\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_1.png\" \/>\r\n<p>Fully Furnished<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Lorem is a dummy text do eiud tempor dolor sit amet dum<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_2.png\" \/>\r\n<p>Royal Touch Paint\u0435<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Lorem is a dummy text do eiud tempor dolor sit amet dum<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_3.png\" \/>\r\n<p>Latest Interior Design<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Lorem is a dummy text do eiud tempor dolor sit amet dum Lorem is a dummy text do eiud tempor dolor sit amet dum<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_4.png\" \/>\r\n<p>Non Stop Security<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Lorem is a dummy text do eiud tempor dolor sit amet dum<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_5.png\" \/>\r\n<p>Living Inside a Nature<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Lorem is a dummy text do eiud tempor dolor sit amet dum<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_6.png\" \/>\r\n<p>Luxurious Fittings<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Lorem is a dummy text do eiud tempor dolor sit amet dum Lorem is a dummy text do eiud tempor dolor sit amet dum<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n<\/div>"},"reviews":{"title":"Reviews"},"agents":{"title":"Agents","subtitle":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."},"blog":{"title":"Blog"},"contact_form":{"title":"Ask what you want","subtitle":"We will answer you soon!"},"footer":{"seo_title":"OUR COMPANY","seo_description":"<p>Lorem must explain to you how all this mistaolt cing pleasure and praising ain wasnad I will give you a complete pain was prexplain to you lorem<\/p>","blog_title":"POPULAR POST","menu_title":"QUICK LINK","contact_title":"CONTACT"}},"ru":{"service":{"title":"\u0421\u0435\u0440\u0432\u0438\u0441"},"why_us":{"title":"\u041f\u043e\u0447\u0435\u043c\u0443 \u043c\u044b","subtitle":"Просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст","description":"<div class=\"row\">\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_1.png\" \/>\r\n<p>Полностью меблирована<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_2.png\" \/>\r\n<p>Royal Touch Paint\u0435<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_3.png\" \/>\r\n<p>Последний дизайн интерьера<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_4.png\" \/>\r\n<p>Безостановочная безопасность<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_5.png\" \/>\r\n<p>Жизнь внутри природы<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n\r\n<div class=\"col-xl-6 col-lg-4 col-md-4 col-sm-6 col-12\">\r\n<div class=\"section_why_us_reason\">\r\n<div class=\"section_why_us_reason_title\"><img alt=\"\" src=\"\/storage\/images\/why_us\/why_us_6.png\" \/>\r\n<p>Роскошная фурнитура<\/p>\r\n<\/div>\r\n\r\n<div class=\"section_why_us_reason_desc\">\r\n<p>Просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст просто текст<\/p>\r\n<\/div>\r\n<\/div>\r\n<\/div>\r\n<\/div>"},"reviews":{"title":"\u041e\u0442\u0437\u044b\u0432\u044b"},"agents":{"title":"\u0410\u0433\u0435\u043d\u0442\u044b","subtitle":"Лучшие риэлторы Лучшие риэлторы Лучшие риэлторы Лучшие риэлторы Лучшие риэлторы Лучшие риэлторы Лучшие риэлторыЛучшие риэлторыЛучшие риэлторыЛучшие риэлторыЛучшие риэлторыЛучшие риэлторы Лучшие риэлторыЛучшие риэлторы Лучшие риэлторыЛучшие риэлторы."},"blog":{"title":"\u0411\u043b\u043e\u0433"},"contact_form":{"title":"\u041d\u0430\u043f\u0438\u0448\u0438\u0442\u0435 \u043d\u0430\u043c","subtitle":null},"footer":{"seo_title":" Наша компания ","seo_description":"Просто текст просто текст просто текст просто текст просто текст просто текст","blog_title":" Популярные статьи ","menu_title":" Ссылки ","contact_title":" Контакты "}},"ch":{"service":{"title":null},"why_us":{"title":null,"subtitle":null,"description":null},"reviews":{"title":null},"agents":{"title":null,"subtitle":null},"blog":{"title":null},"contact_form":{"title":null,"subtitle":null},"footer":{"seo_title":"Наша компания","seo_description":"Просто текст просто текст просто текст просто текст просто текст просто текст","blog_title":"Популярные статьи","menu_title":"Быстрие ссылки","contact_title":"Контакты"}},"es":{"service":{"title":null},"why_us":{"title":null,"subtitle":null,"description":null},"reviews":{"title":null},"agents":{"title":null,"subtitle":null},"blog":{"title":null},"contact_form":{"title":null,"subtitle":null},"footer":{"seo_title":null,"seo_description":null,"blog_title":null,"menu_title":null,"contact_title":null}}}',
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "top_logo",
                'value' => 'catalog/logo.png',
            ]
        );
    }
}
