<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Review::create(
            [
                'full_name' => 'Alex',
                'email' => 'alex@example.com',
                'published' => 1,
                'available_translation' => 'en',
                'review' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. '
            ]
        );

        \App\Models\Review::create(
            [
                'full_name' => 'Nikon',
                'email' => 'alex@example.com',
                'published' => 1,
                'available_translation' => 'en',
                'review' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. '
            ]
        );

        \App\Models\Review::create(
            [
                'full_name' => 'Smiraldo',
                'email' => 'alex@example.com',
                'published' => 1,
                'available_translation' => 'en',
                'review' => 'Lorem ipsum dolor sit ametr sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. '
            ]
        );

        \App\Models\Review::create(
            [
                'full_name' => 'Komkovaga',
                'email' => 'alex@example.com',
                'published' => 1,
                'available_translation' => 'en',
                'review' => 'Lorem ipsum dolor sittrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. '
            ]
        );




        \App\Models\ReviewTranslation::create(
            [
                'review_id' => 1,
                'locale' => 'ru',
                'review' => 'Добрый день! Благодаря вашему агентству и специалисту по недвижимости Яне Решитько мы приобрели ту недвижимость, о которой мечтали. За короткое время Яне удалось понять наши пожелания, решить сопутствующие проблемы и найти именно то, что мы хотели. Яна очень грамотный, опытный и ответственный специалист.',
            ]
        );
        \App\Models\ReviewTranslation::create(
            [
                'review_id' => 2,
                'locale' => 'ru',
                'review' => 'Наверное, нет таких слов, какими можно выразить мою благодарность Катерине Голод! Невероятный профессионал, но самое главное - необыкновенной красоты человек: внимательная к деталям, порядочная. Это как раз тот самый случай, когда жалеешь, что на данный момент, не нужно ничего продавать/покупать/сдавать в аренду)',
            ]
        );
        \App\Models\ReviewTranslation::create(
            [
                'review_id' => 3,
                'locale' => 'ru',
                'review' => 'Хочу сказать огромное спасибо за работу Георгию Иконникову. Благодаря работе этого человека у нас теперь есть долгожданная квартира. Настолько внимательного профессионала я еще не встречала. Георгий моментально подобрал самый подходящий вариант и опекался как родной человек, сопровождая не простую сделку с момента просмотра и до вручения ключей.',
            ]
        );
        \App\Models\ReviewTranslation::create(
            [
                'review_id' => 4,
                'locale' => 'ru',
                'review' => 'Хочу поблагодарить Компанию «Благовест», а в частности Касьяна Дениса и Подольскую Татьяну Петровну за профессиональную работу в поиске квартиры и проведении сделки. Все прошло гладко, быстро и безопасно. ',
            ]
        );


        DB::table('reviews')
            ->update(['available_translation'  => 'en,ru']);
    }
}
