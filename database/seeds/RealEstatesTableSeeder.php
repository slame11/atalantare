<?php

use Illuminate\Database\Seeder;

class RealEstatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $realestate = \App\Models\RealEstate::create(
        [
            'country_id' => 1,
            'city_id' => 3 ,
            'real_estate_type_id' => 1,
            'show_on_home_slider' => 1,
            'show_on_home_recommended' => 1,
            'description' =>'Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incidint ut labore lore gna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm emod tempor nt ut labore lore magna iqua. Ut enim ad minim veniamco laboris nisi ut aliqu

Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore lore gna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm',
            'title' =>'Real Estate 1',
            'keyword' => 'Real Estate 1',
            'media_description' => 'Real Estate 1',
            'address' =>'Orgeval, France',
            'category_id' => 1,
            'seo_url' => 'realestate1',
            'map_point' => '50.444761 30.520970',
            'price' => '500000',
            'viewed' => 1,
        ]
    );

        $realestate->agents()->sync([1, 2, 3]);

        $realestate = \App\Models\RealEstate::create(
            [
                'country_id' => 2,
                'city_id' => 1 ,
                'real_estate_type_id' => 1,
                'show_on_home_slider' => 1,
                'show_on_home_recommended' => 1,
                'description' =>'Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incidint ut labore lore gna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm emod tempor nt ut labore lore magna iqua. Ut enim ad minim veniamco laboris nisi ut aliqu

Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore lore gna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm',
                'title' =>'Real Estate 2',
                'keyword' => 'Real Estate 2',
                'media_description' => 'Real Estate 2',
                'address' =>'Moscow, Russia',
                'category_id' => 2,
                'seo_url' => 'realestate2',
                'map_point' => '50.444761 30.520970',
                'price' => '400000',
                'viewed' => 1,
            ]
        );
        $realestate->agents()->sync([1, 2]);

        $realestate = \App\Models\RealEstate::create(
            [
                'country_id' => 3,
                'city_id' => 1 ,
                'real_estate_type_id' => 1,
                'show_on_home_slider' => 1,
                'show_on_home_recommended' => 1,
                'description' =>'Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incidint ut labore lore gna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm emod tempor nt ut labore lore magna iqua. Ut enim ad minim veniamco laboris nisi ut aliqu

Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore lore gna iqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacm',
                'title' =>'Real Estate 3',
                'keyword' => 'Real Estate 3',
                'media_description' => 'Real Estate 3',
                'address' =>'Shanghai, China',
                'category_id' => 1,
                'seo_url' => 'realestate3',
                'map_point' => '50.444761 30.520970',
                'price' => '30000',
                'viewed' => 1,
            ]
        );
        $realestate->agents()->sync([1, 2, 3, 4]);

        \App\Models\RealEstate::create(
            [
                'map_point' => null,
                'country_id' => 4,
                'city_id' => 1 ,
                'real_estate_type_id' => 1,
                'show_on_home_slider' => 0,
                'show_on_home_recommended' => 1,
                'title' =>'Real Estate 4',
                'keyword' => 'Real Estate 4',
                'media_description' => 'Real Estate 4',
                'address' =>'Toronto, Canada',
                'description' =>'Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incidint ut',
                'category_id' => 2,
                'viewed' => 1,
                'seo_url' => 'realestate4',
            ]
        );
        \App\Models\RealEstate::create(
            [
                'map_point' => null,
                'country_id' => 1,
                'city_id' => 1 ,
                'real_estate_type_id' => 1,
                'show_on_home_slider' => 0,
                'title' =>'Real Estate 5',
                'keyword' => 'Real Estate 5',
                'media_description' => 'Real Estate 5',
                'address' =>'Paris, France',
                'show_on_home_recommended' => 1,
                'description' => 'Dom-Inno is the Best should be the consectetur adipiscing elit, sed do eiusmod tempor incidint ut',
                'category_id' => 3,
                'viewed' => 1,
                'seo_url' => 'realestate5',
            ]
        );


        \App\Models\RealEstateTranslation::create(
            [
                'real_estate_id' => 1,
                'locale' => 'ru' ,
                'description' => 'Дом-Инно является Лучшим Должностным лицом, которое делает elit, sed do eiusmod tempor incidint ut',
                'title' => 'Недвижимость 1',
                'keyword' => 'Недвижимость 1',
                'media_description' =>'Недвижимость 1',
                'address' =>'Скоттсдейл, AZ, Соединенные Штаты',
            ]
        );

        \App\Models\RealEstateTranslation::create(
            [
                'real_estate_id' => 2,
                'locale' => 'ru' ,
                'description' => 'Дом-Инно является Лучшим Должностным лицом, которое делает elit, sed do eiusmod tempor incidint ut',
                'title' => 'Недвижимость 2',
                'keyword' => 'Недвижимость 2',
                'media_description' =>'Недвижимость 2',
                'address' =>'Скоттсдейл, AZ, Соединенные Штаты',
            ]
        );

        \App\Models\RealEstateTranslation::create(
            [
                'real_estate_id' => 3,
                'locale' => 'ru' ,
                'description' => 'Дом-Инно является Лучшим Должностным лицом, которое делает elit, sed do eiusmod tempor incidint ut',
                'title' => 'Недвижимость 3',
                'keyword' => 'Недвижимость 3',
                'media_description' =>'Недвижимость 3',
                'address' =>'Скоттсдейл, AZ, Соединенные Штаты',
            ]
        );

        \App\Models\RealEstateTranslation::create(
            [
                'real_estate_id' => 4,
                'locale' => 'ru' ,
                'description' => 'Дом-Инно является Лучшим Должностным лицом, которое делает elit, sed do eiusmod tempor incidint ut',
                'title' => 'Недвижимость 4',
                'keyword' => 'Недвижимость 4',
                'media_description' =>'Недвижимость 4',
                'address' =>'Скоттсдейл, AZ, Соединенные Штаты',
            ]
        );

        \App\Models\RealEstateTranslation::create(
            [
                'real_estate_id' => 5,
                'locale' => 'ru' ,
                'description' => 'Дом-Инно является Лучшим Должностным лицом, которое делает elit, sed do eiusmod tempor incidint ut',
                'title' => 'Недвижимость 5',
                'keyword' => 'Недвижимость 5',
                'media_description' =>'Недвижимость 5',
                'address' =>'Скоттсдейл, AZ, Соединенные Штаты',
            ]
        );

        DB::table('real_estates')
            ->update(['available_translation'  => 'en,ru']);
    }
}
