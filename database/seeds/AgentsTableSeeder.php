<?php

use Illuminate\Database\Seeder;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Agent::create(
            [
                'show_on_main' => 1,
                'photo' => 'catalog/agents/agent_1.png',
                'name' => 'Agent 1',
                'email' => 'admin@admin.com',
                'address' => 'test addres 1',
                'phone' => '34534534',
                'mobile' => '4334345',
                'organization' => 'Test organization 1',
            ]
        );

        \App\Models\Agent::create(
            [
                'show_on_main' => 1,
                'photo' => 'catalog/agents/agent_2.png',
                'name' => 'Agent 2',
                'email' => 'admin@admin.com',
                'address' => 'test addres 2',
                'phone' => '34534534',
                'mobile' => '4334345',
                'organization' => 'Test organization 2',
            ]
        );

        \App\Models\Agent::create(
            [
                'show_on_main' => 1,
                'photo' => 'catalog/agents/agent_3.png',
                'name' => 'Agent 3',
                'email' => 'admin@admin.com',
                'address' => 'test addres 3',
                'phone' => '34534534',
                'mobile' => '4334345',
                'organization' => 'Test organization 3',
            ]
        );

        \App\Models\Agent::create(
            [
                'show_on_main' => 1,
                'photo' => 'catalog/agents/agent_4.png',
                'name' => 'Agent 4',
                'email' => 'admin@admin.com',
                'address' => 'test addres 4',
                'phone' => '34534534',
                'mobile' => '4334345',
                'organization' => 'Test organization 4',
            ]
        );
    }
}
