<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'title' => 'Home',
                'seo_url' => '/',
                'available_translation' => 'ru,en,es,ch',
                'static' => 1,
            ],
            [
                'title' => 'About_us',
                'seo_url' => 'about_us',
                'available_translation' => 'ru,en,es,ch',
                'static' => 1,
            ],
            [
                'title' => 'Contacts',
                'seo_url' => 'contacts',
                'available_translation' => 'ru,en,es,ch',
                'static' => 1,
            ],
        ];

        foreach ($pages as $data) {
            $page = \App\Models\Page::create($data);
            DB::table('pages')
                ->where('id', $page->id)
                ->update(['available_translation'  => $data['available_translation']]);
        }
    }
}
