<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Gas station',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Restaurant',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Flat',
                
            ]
        );

        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 3,
                'locale' => 'ru',
                'name' => 'Квартира',
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Office Center',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Villas',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Chalet',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'House',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Penthouse',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Castle',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Hotels',
                
            ]
        );
        \App\Models\Category::create(
            [
                'available_translation'=> 1,
                'name' => 'Supermarket',
                
            ]
        );



        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 1,
                'locale' => 'ru',
                'name' => 'АЗС',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 2,
                'locale' => 'ru',
                'name' => 'Ресторан',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 4,
                'locale' => 'ru',
                'name' => 'Офис центр',
            ]
        );

        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 5,
                'locale' => 'ru',
                'name' => 'Виллы',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 6,
                'locale' => 'ru',
                'name' => 'Шале',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 7,
                'locale' => 'ru',
                'name' => 'Дом',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 8,
                'locale' => 'ru',
                'name' => 'Пентхаус',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 9,
                'locale' => 'ru',
                'name' => 'Замок',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 10,
                'locale' => 'ru',
                'name' => 'Отель',
            ]
        );
        \App\Models\CategoryTranslation::create(
            [
                'category_id'=> 11,
                'locale' => 'ru',
                'name' => 'Супермаркет',
            ]
        );


        DB::table('categories')
            ->update(['available_translation'  => 'en,ru']);
    }
}
