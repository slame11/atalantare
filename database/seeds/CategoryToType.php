<?php

use Illuminate\Database\Seeder;
use \App\Models\CategoryToType as MainClass;

/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 05.02.2019
 * Time: 1:20
 */

class CategoryToType extends Seeder
{

    public function run()
    {
        MainClass::create([
            'category_id' => 1,
            'type_id' => 1
        ]);
        MainClass::create([
            'category_id' => 1,
            'type_id' => 2
        ]);
        MainClass::create([
            'category_id' => 2,
            'type_id' => 3
        ]);
        MainClass::create([
            'category_id' => 3,
            'type_id' => 2
        ]);
        MainClass::create([
            'category_id' => 5,
            'type_id' => 1
        ]);
        MainClass::create([
            'category_id' => 8,
            'type_id' => 1
        ]);
    }
}