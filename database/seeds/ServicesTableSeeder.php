<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Service::create(
            [
                'image' => 'catalog/services/service_1.png',
                'status' => 1,
                'name' => 'Service 1',
                'available_translation' => 'dadadad',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, voluptatum.'
            ]
        );

        \App\Models\Service::create(
            [
                'image' => 'catalog/services/service_2.png',
                'status' => 1,
                'name' => 'Service 2',
                'available_translation' => '1',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, voluptatum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, voluptatum.'
            ]
        );

        \App\Models\Service::create(
            [
                'image' => 'catalog/services/service_3.png',
                'status' => 1,
                'name' => 'Service 3',
                'available_translation' => '1',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, voluptatum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, voluptatum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, voluptatum.'
            ]
        );

        \App\Models\Service::create(
            [
                'image' => 'catalog/services/service_4.png',
                'status' => 1,
                'name' => 'Service 4',
                'available_translation' => '1',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, voluptatum.'
            ]
        );



        \App\Models\ServiceTranslation::create(
            [
                'service_id' => 1,
                'locale' => 'ru',
                'name' => 'Сервис 1',
                'text' => 'Lorem Ipsum Dolor Sit Amet, Заклинатель, жировой элит. Qui, Vylptatum.'
            ]
        );
        \App\Models\ServiceTranslation::create(
            [
                'service_id' => 2,
                'locale' => 'ru',
                'name' => 'Сервис 2',
                'text' => 'Lorem Ipsum Dolor Sit Amet, Заклинатель, жировой элит. Qui, Vylptatum.'
            ]
        );
        \App\Models\ServiceTranslation::create(
            [
                'service_id' => 3,
                'locale' => 'ru',
                'name' => 'Сервис 3',
                'text' => 'Lorem Ipsum Dolor Sit Amet, Заклинатель, жировой элит. Qui, Vylptatum.'
            ]
        );
        \App\Models\ServiceTranslation::create(
            [
                'service_id' => 4,
                'locale' => 'ru',
                'name' => 'Сервис 4',
                'text' => 'Lorem Ipsum Dolor Sit Amet, Заклинатель, жировой элит. Qui, Vylptatum.'
            ]
        );


        DB::table('services')
            ->update(['available_translation'  => 'en,ru']);

    }
}
