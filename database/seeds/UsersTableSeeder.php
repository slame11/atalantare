php artisan migrate:refresh --seed<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Administrator',
                'email' => 'admin@admin.com',
                'password' => '111111',
                'is_active' => '1',
                'roles'      => [
                    'Admin',
                ],
            ],
        ];

        foreach ($users as $data) {
            $user = \App\Models\User::create(
                [
                    'email'      => $data['email'],
                    'name'       => $data['name'],
                    'password'   => $data['password'],
                    'is_active'   => $data['is_active'],
                ]
            );
            foreach ($data['roles'] as $r) {
                $role = \Spatie\Permission\Models\Role::firstOrCreate(['name' => $r]);
                $user->assignRole($role);
            }
        }
    }
}
