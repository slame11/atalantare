<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Attribute::create(
            [
                'name' => 'pool',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
                'show_on_main' => 1
            ]
        );

        \App\Models\Attribute::create(
            [
                'name' => 'sauna',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
                'show_on_main' => 1
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'jacuzzi',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'ski-in-ski-out',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'first line',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
                'show_on_main' => 1
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'sea view',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'air condition',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'wi-fi',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'terrace balcony',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
                'show_on_main' => 1
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'fireplace',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'garden',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'barbecue',
                'attribute_type_id'=> '1',
                'val_min' => null,
                'val_max' => null,
                'show_on_main' => 1
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'distance to see',
                'attribute_type_id'=> '2',
                'val_min' => '2',
                'val_max' => '5',
                'show_on_main' => 1
            ]
        );

        \App\Models\Attribute::create(
            [
                'name' => 'distance to city center',
                'attribute_type_id'=> '2',
                'val_min' => '1',
                'val_max' => '35',
                'show_on_main' => 1
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'room count',
                'attribute_type_id'=> '3',
                'val_min' => '0',
                'val_max' => '24',
                'show_on_main' => 1
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'Sq Ft',
                'attribute_type_id'=> '3',
                'val_min' => '0',
                'val_max' => '11',
                'path' => 'catalog/realEstateIcons/attr_1.png',
                'show_on_main' => 0
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'Bedrooms',
                'attribute_type_id'=> '3',
                'val_min' => '0',
                'val_max' => '12',
                'path' => 'catalog/realEstateIcons/attr_2.png'
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'Parking',
                'attribute_type_id'=> '3',
                'val_min' => '0',
                'val_max' => '6',
                'path' => 'catalog/realEstateIcons/attr_4.png',
                'show_on_main' => 1
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'Showers',
                'attribute_type_id'=> '3',
                'val_min' => '0',
                'val_max' => '4',
                'path' => 'catalog/realEstateIcons/attr_3.png',
                'show_on_main' => 0
            ]
        );
        \App\Models\Attribute::create(
            [
                'name' => 'Kitchen',
                'attribute_type_id'=> '3',
                'val_min' => '0',
                'val_max' => '4',
                'path' => 'catalog/realEstateIcons/attr_4.png',
                'show_on_main' => 0
            ]
        );


        \App\Models\Attribute::create(
            [
                'name' => 'Toilet',
                'attribute_type_id'=> '3',
                'val_min' => '0',
                'val_max' => '4',
                'path' => 'catalog/realEstateIcons/attr_3.png',
                'show_on_main' => 0
            ]
        );

        \App\Models\Attribute::create(
            [
                'name' => 'Living space',
                'attribute_type_id'=> '2',
                'val_min' => '0',
                'val_max' => '899',
                'show_on_main' => 0
            ]
        );

        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 1,
                'locale'=> 'ru',
                'name' => 'бассейн'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 2,
                'locale'=> 'ru',
                'name' => 'сауна'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 3,
                'locale'=> 'ru',
                'name' => 'джакузи'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 4,
                'locale'=> 'ru',
                'name' => 'лыжа'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 5,
                'locale'=> 'ru',
                'name' => 'первая строка'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 6,
                'locale'=> 'ru',
                'name' => 'вид на море'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 7,
                'locale'=> 'ru',
                'name' => 'кондиционер'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 8,
                'locale'=> 'ru',
                'name' => 'Wi-Fi'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 9,
                'locale'=> 'ru',
                'name' => 'терраса балкон'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 10,
                'locale'=> 'ru',
                'name' => 'камин'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 11,
                'locale'=> 'ru',
                'name' => 'сад'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 12,
                'locale'=> 'ru',
                'name' => 'барбекю'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 13,
                'locale'=> 'ru',
                'name' => 'расстояние до моря'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 14,
                'locale'=> 'ru',
                'name' => 'расстояние до центра города'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 15,
                'locale'=> 'ru',
                'name' => 'количество комнат'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 16,
                'locale'=> 'ru',
                'name' => 'м2'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 17,
                'locale'=> 'ru',
                'name' => 'спальни'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 18,
                'locale'=> 'ru',
                'name' => 'Стоянка'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 19,
                'locale'=> 'ru',
                'name' => 'душ'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 20,
                'locale'=> 'ru',
                'name' => 'Кухня'
            ]
        );
        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 21,
                'locale'=> 'ru',
                'name' => 'Туалет'
            ]
        );

        \App\Models\AttributeTranslation::create(
            [
                'attribute_id' => 22,
                'locale'=> 'ru',
                'name' => 'Квадратура'
            ]
        );
        DB::table('attributes')
            ->update(['available_translation'  => 'en,ru']);
    }
}
