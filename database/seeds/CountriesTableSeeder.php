<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Country::create(
            [
                'name' => "France",
                'symbols' => 'FR',
                'seo_url' => 'france',
                'map_point' => null,
                'available_translation'=> null,
            ]
        );



        \App\Models\CountryTranslation::create(
            [
                'country_id' => 1,
                'locale' => 'ru',
                'name' => 'Франция'
            ]
        );


        \App\Models\Country::create(
            [
                'name' => "Russia",
                'symbols' => 'RU',
                'seo_url' => 'russia',
                'map_point' => null,
                'available_translation'=> null,
            ]
        );

        \App\Models\Country::create(
            [
                'name' => "China",
                'symbols' => 'CH',
                'seo_url' => 'china',
                'map_point' => null,
                'available_translation'=> null,
            ]
        );

        \App\Models\Country::create(
            [
                'name' => "Canada",
                'symbols' => 'CA',
                'map_point' => null,
                'seo_url' => 'canada',
                'available_translation'=> null,
            ]
        );


        \App\Models\CountryTranslation::create(
            [
                'country_id' => 2,
                'locale' => 'ru',
                'name' => 'Россия'
            ]
        );

        \App\Models\CountryTranslation::create(
            [
                'country_id' => 3,
                'locale' => 'ru',
                'name' => 'Китай'
            ]
        );

        \App\Models\CountryTranslation::create(
            [
                'country_id' => 4,
                'locale' => 'ru',
                'name' => 'Канада'
            ]
        );


        DB::table('countries')
            ->update(['available_translation'  => 'en,ru']);
    }
}
