<?php

use Illuminate\Database\Seeder;

class RealEstateAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 1,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 2,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 3,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 4,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 6,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 9,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 10,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 15,
                'value' => 480
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 16,
                'value' => 3
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 19,
                'value' => 2
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 17,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 1,
                'attribute_id' => 18,
                'value' => 2
            ]
        );

        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 1,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 2,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 3,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 4,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 6,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 9,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 10,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 15,
                'value' => 480
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 16,
                'value' => 3
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 19,
                'value' => 2
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 17,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 2,
                'attribute_id' => 18,
                'value' => 2
            ]
        );

        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 1,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 2,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 3,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 4,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 6,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 9,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 10,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 15,
                'value' => 480
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 16,
                'value' => 3
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 19,
                'value' => 2
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 17,
                'value' => 1
            ]
        );
        \App\Models\RealEstateAttribute::create(
            [
                'real_estate_id' => 3,
                'attribute_id' => 18,
                'value' => 2
            ]
        );

    }
}
