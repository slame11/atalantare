<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleTableSeeder::class,
            UsersTableSeeder::class,
            CountriesTableSeeder::class,
            CitiesTableSeeder::class,
            AgentsTableSeeder::class,
            RealEstatesTableSeeder::class,
            RealEstateImagesTableSeeder::class,
            AttributesTableSeeder::class,
            CategoriesTableSeeder::class,
            RealEstateAttributesTableSeeder::class,
            PagesTableSeeder::class,
            ArticlesTableSeeder::class,
            ServicesTableSeeder::class,
            ReviewsTableSeeder::class,
            CategoryToType::class,
            GeneralSettingsTableSeeder::class
        ]);
    }
}
