<?php

use Illuminate\Database\Seeder;

class RealEstateImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 1,
                'sort' => 2 ,
                'path' => 'catalog/banner_2.jpg'
            ]
        );
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 1,
                'sort' => 3 ,
                'path' => 'catalog/banner_3.jpg'
            ]
        );

        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 2,
                'sort' => 1 ,
                'path' => 'catalog/banner.jpg'
            ]
        );
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 2,
                'sort' => 2 ,
                'path' => 'catalog/banner_2.jpg'
            ]
        );
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 2,
                'sort' => 3 ,
                'path' => 'catalog/banner_3.jpg'
            ]
        );

        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 3,
                'sort' => 2 ,
                'path' => 'catalog/banner_2.jpg'
            ]
        );
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 3,
                'sort' => 3 ,
                'path' => 'catalog/banner_3.jpg'
            ]
        );
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 4,
                'sort' => 3 ,
                'path' => 'catalog/banner_3.jpg'
            ]
        );
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 5,
                'sort' => 3 ,
                'path' => 'catalog/banner_2.jpg'
            ]
        );
        \App\Models\RealEstateImages::create(
            [
                'real_estate_id' => 6,
                'sort' => 3 ,
                'path' => 'catalog/banner_1.jpg'
            ]
        );
    }
}
