<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \App\Models\Article::create(
            [
                'image' => 'catalog/banner.jpg',
                'slug' => 'blog-1',
                'image_show' => 1,
                'published' => 1,
                'created_by' => 1,
                'name' => 'Special Unveiling of 568 N. Tigertail Road',
                'description_short' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id&nbsp;</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id&nbsp;</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id&nbsp;</p>',
                'meta_title' => 'Blog 1',
                'meta_description' => 'Blog 1',
                'meta_keyword' => 'Blog 1',
            ]
        );

        \App\Models\Article::create(
            [
                'image' => 'catalog/grid-blog-1-571x353.png',
                'slug' => 'blog-2',
                'image_show' => 1,
                'published' => 1,
                'created_by' => 1,
                'name' => 'Special Unveiling of 568 N. Tigertail Road',
                'description_short' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id&nbsp;</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id&nbsp;</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id&nbsp;</p>',
                'meta_title' => 'Blog 2',
                'meta_description' => 'Blog 2',
                'meta_keyword' => 'Blog 2',
            ]
        );




        \App\Models\ArticleTranslation::create(

            [
                'article_id' => 1,
                'locale' => 'ru',
                'name' => 'Специальное открытие 568 N. Tigertail Road',
                'description_short' => 'Просто текст протсо текст просто текст просто текст  просто текст  просто текст ',
                'description' => '<p>Просто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текст</p>',
                'meta_title' => 'Блог 1',
                'meta_description' => 'Блог 1',
                'meta_keyword' => 'Блог 1',
            ]
        );
        \App\Models\ArticleTranslation::create(

            [
                'article_id' => 2,
                'locale' => 'ru',
                'name' => 'Специальное открытие 568 N. Tigertail Road',
                'description_short' => 'Просто текст протсо текст просто текст просто текст  просто текст  просто текст ',
                'description' => '<p>Просто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текстПросто текст протсо текст просто текст просто текст  просто текст  просто текст</p>',
                'meta_title' => 'Блог 2',
                'meta_description' => 'Блог 2',
                'meta_keyword' => 'Блог 2',
            ]
        );


        DB::table('articles')
            ->update(['available_translation'  => 'en,ru']);
    }
}
