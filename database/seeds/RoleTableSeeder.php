<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'        => 'Admin',
                'permissions' => [
                    'admin',
                    'permissions',
                    'roles',
                    'users',
                    'countries',
                    'cities',
                    'articles',
                    'attributes',
                    'attributeTypes',
                    'realEstates',
                    'fileManager',
                    'categories',
                    'attributeTypes',
                    'agents',
                    'reviews',
                    'services',
                    'import_glamourapartments'
                ],
            ],
            [
                'name'        => 'User',
                'permissions' => [
                ],
            ],
        ];

        foreach ($roles as $data) {
            $role        = new \App\Models\Role();
            $role->name  = $data['name'];
            $role->save();

            foreach ($data['permissions'] as $p) {
                $permission = \Spatie\Permission\Models\Permission::firstOrCreate(
                    ['name' => $p]
                );
                $role->givePermissionTo($permission);
            }
        }
    }
}
