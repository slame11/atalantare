<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Paris',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );



        \App\Models\CityTranslation::create(
            [
                'city_id' => 1,
                'locale' => 'ru',
                'name' => 'Париж',
            ]
        );

        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Bougival',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );

        \App\Models\CityTranslation::create(
            [
                'city_id' => 2,
                'locale' => 'ru',
                'name' => 'Бугивал',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Orgeval',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );

        \App\Models\CityTranslation::create(
            [
                'city_id' => 3,
                'locale' => 'ru',
                'name' => 'Орегвал',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Deauville',
                'map_point' => null,
                'zip_code'=> 73034,
            ]
        );


        \App\Models\CityTranslation::create(
            [
                'city_id' => 4,
                'locale' => 'ru',
                'name' => 'Дэвиль',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Brignac',
                'map_point' => null,
                'zip_code'=> 73000,
            ]
        );

        \App\Models\CityTranslation::create(
            [
                'city_id' => 5,
                'locale' => 'ru',
                'name' => 'Бригнак',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Cannes',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );

        \App\Models\CityTranslation::create(
            [
                'city_id' => 6,
                'locale' => 'ru',
                'name' => 'Канны',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Thiais',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );

        \App\Models\CityTranslation::create(
            [
                'city_id' => 7,
                'locale' => 'ru',
                'name' => 'Таис',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Brétigny Sur Orge',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );

        \App\Models\CityTranslation::create(
            [
                'city_id' => 8,
                'locale' => 'ru',
                'name' => 'Бретиньи-сюр-Орж',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Saint-Étienne-la-Thillaye',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );
        \App\Models\CityTranslation::create(
            [
                'city_id' => 9,
                'locale' => 'ru',
                'name' => 'Saint-Étienne-la-Thillaye',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Levallois Perret',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );
        \App\Models\CityTranslation::create(
            [
                'city_id' => 10,
                'locale' => 'ru',
                'name' => 'Levallois Perret',
            ]
        );
        \App\Models\City::create(
        [
            'country_id' => 1,
            'name' => 'Saint-Tropez',
            'map_point' => null,
            'zip_code'=> null,
        ]
        );
        \App\Models\CityTranslation::create(
            [
                'city_id' => 11,
                'locale' => 'ru',
                'name' => 'Сантропе',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Nice',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );
        \App\Models\CityTranslation::create(
            [
                'city_id' => 12,
                'locale' => 'ru',
                'name' => 'Nice',
            ]
        );
        \App\Models\City::create(
            [
                'country_id' => 1,
                'name' => 'Melun',
                'map_point' => null,
                'zip_code'=> null,
            ]
        );
        \App\Models\CityTranslation::create(
            [
                'city_id' => 13,
                'locale' => 'ru',
                'name' => 'Melun',
            ]
        );

        DB::table('cities')
            ->update(['available_translation'  => 'en,ru']);
    }
}
