<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagesTable extends Migration
{

    public $timestamps = true;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seo_url', 255);
            $table->boolean('static')->default(1);
            $table->string('available_translation', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('page_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('page_id');
            $table->string('locale')->index();

            $table->string( 'title', 255 )->nullable();
            $table->text('description')->nullable();

            $table->string( 'meta_title', 255 )->nullable();
            $table->text( 'meta_keyword' )->nullable();
            $table->text( 'meta_description' )->nullable();

            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_translations');
        Schema::dropIfExists('pages');
    }
}
