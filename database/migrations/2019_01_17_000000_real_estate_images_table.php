<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RealEstateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estate_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('real_estate_id');
            $table->unsignedInteger('sort');
            $table->string('path');

        });

        Schema::create('real_estate_images_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('real_estate_images_id');
            $table->string('locale')->index();

            $table->string( 'title', 255 )->nullable();

            $table->foreign('real_estate_images_id')->references('id')->on('real_estate_images')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_estate_images_translations');
        Schema::dropIfExists('real_estate_images');
    }
}
