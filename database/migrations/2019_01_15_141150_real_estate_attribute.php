<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RealEstateAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estate_attribute', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('real_estate_id');
            $table->unsignedInteger('attribute_id');
            $table->integer('value');

            $table->foreign('real_estate_id')->references('id')->on('real_estates')->onDelete('cascade');
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_estate_attribute');
    }
}
