<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('real_estate_type_id');
            $table->unsignedInteger ('rent_period')->nullable();
            $table->unsignedInteger('import_id')->default(0);
            $table->unsignedInteger('import_object_id')->default(0);
            $table->string( 'map_point' )->nullable();
            $table->boolean( 'show_on_home_slider' )->default(0);
            $table->boolean( 'show_on_home_recommended' )->default(0);
            $table->unsignedInteger('category_id');
            $table->string('seo_url', 255)->nullable();
            $table->unsignedInteger('viewed')->default(0);
            $table->string('available_translation', 255)->nullable();


            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->timestamps();
        });

        Schema::create('real_estate_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('real_estate_id');
            $table->string('locale')->index();

            $table->text('description')->nullable();

            $table->string( 'title', 255 )->nullable();
            $table->string( 'keyword', 255 )->nullable();
            $table->text( 'media_description' )->nullable();

            $table->foreign('real_estate_id')->references('id')->on('real_estates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_estate_translations');
        Schema::dropIfExists('real_estates');
    }
}
