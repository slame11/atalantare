<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Use get ?lang=locale
Route::middleware(['locale'])->group(function () {
    Route::get('/', 'MainController@index')->name('home');
    Route::get('/articles', 'ArticleController@index')->name('articles');
    Route::get('/article','ArticleController@show')->name('article');
    Route::get('/contact', 'ContactController@index')->name('contacts');
    Route::get('/catalog/','CatalogController@index')->name('catalog');
    Route::get('/catalog/{country}','CatalogController@index')->name('catalog-country');
    Route::get('/realestate/{name}', 'RealEstateController@index')->name('realestate');
});

Route::post('/getcities','MainController@getCitiesByCountryId')->name('getcities');
Route::post('/getcategories','MainController@getCategoriesByTypeId')->name('getcategories');
Route::post('/feedback', 'MainController@feedback')->name('feedback');
Route::post('/review', 'ReviewController@review')->name('review');
Route::get('/setlang/{locale}', 'MainController@setLang')->name('setlang');
Route::get('/setcurrency/{currency}', 'MainController@setCurrency')->name('setcurrency');
Route::post('/realestate/viewed', 'RealEstateController@viewed')->name('realestateviewed');
Route::post('/catalog/setproductview','CatalogController@setProductView')->name('setproductview');
Route::post('/catalog/setfiltertoggle','CatalogController@setFilterToggle')->name('setfiltertoggle');
Route::post('/catalog/getproducts', 'CatalogController@getProducts')->name('getproducts');

Auth::routes();

// admin-lte
Route::prefix( '/admin' )->group( function () {

    Route::get('/admin-lte/{locale}', 'Admin\AdminController@setLang')->name('lang');
    Route::get( '/', 'Admin\AdminController@index' )->name('dashboard');
    Route::get('/chart', 'Admin\AdminController@getChartData' );

    Route::resource('/users','Admin\UserController');
    Route::resource('/countries','Admin\CountryController');
    Route::resource('/cities','Admin\CityController');
    Route::resource('/generalSettings','Admin\GeneralSettingController');
    Route::resource('/realEstates','Admin\RealEstateController');
    Route::post('/upload','Admin\RealEstateController@multipleUpload')->name('multipleUpload');
    Route::resource('/articles','Admin\ArticleController');
    Route::resource('/attributes','Admin\AttributeController');
    Route::resource('/attributeTypes','Admin\AttributeTypeController');
    Route::resource('/feedbacks','Admin\FeedbackController');
    Route::resource('/services','Admin\ServiceController');
    Route::resource('/categories','Admin\CategoryController');
    Route::resource('/agents','Admin\AgentController');
    Route::resource('/pages','Admin\PageController');
    Route::resource('/ga','Admin\GoogleAnalyticsController');
    Route::resource('/reviews','Admin\ReviewController');

    Route::get( '/import/glamourapartments', 'Admin\ImportGlamourapartmentsController@index')->name('import.glamourapartments');
    Route::get('/filemanager','Admin\FilemanagerController@index')->name('filemanager');
    Route::get('/filemanager/show','Admin\FilemanagerController@show')->name('show-filemanager');

    Route::post('/feedbacks/sendreaction','Admin\FeedbackController@sendReaction')->name('send_feedback_reaction');

    Route::post('/import/import_glamourapartments','Admin\ImportGlamourapartmentsController@import_glamourapartments')->name('import_glamourapartments');
    Route::post('/filemanager/upload','Admin\FilemanagerController@upload')->name('upload-items');
    Route::post('/filemanager/new','Admin\FilemanagerController@folder')->name('new-folder');
    Route::post('/filemanager/delete','Admin\FilemanagerController@delete')->name('delete-items');

    Route::get('/getAttributesByName','Admin\AttributeController@getAttributesByName')->name('getAttributesByName');
    Route::post('/getAttribute','Admin\AttributeController@getAttribute')->name('getAttribute');

    // Avatar
    Route::post('uploadAvatar', 'Admin\UserAvatarController@uploadAvatar')->name('uploadAvatar');
    Route::post('removeAvatar', 'Admin\UserAvatarController@removeAvatar')->name('removeAvatar');

    Route::get( '/profile', 'Admin\ProfileController@profileEdit' )->name('profile.edit');
    Route::put( '/profile', 'Admin\ProfileController@profileSave')->name('profile.save');



//    Route::get('/impersonate/{user_id}', 'Admin\ImpersonateController@impersonate')->name('impersonate');
//    Route::get('/impersonate.leave', 'Admin\DashboardController@impersonate_leave')->name('impersonate.leave');

//    Route::get( '/', 'Admin\DashboardController@index' )->name('dashboard');

});

Route::get('clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

Route::get('{name}', 'PageController@index')->name('page');
